subroutine xmilar(ndim, ndime, elrefp, geom,  pinref,&
                  ia, ib, ip, ksia, ksib, milara, milarb)
!
    implicit none
!
#include "asterfort/assert.h"
#include "asterfort/xelrex.h"
#include "asterfort/reerel.h"
    integer :: ndim, ndime, ia, ib, ip
    real(kind=8) :: milara(3), milarb(3), pinref(*), geom(*)
    real(kind=8) :: ksia(ndime), ksib(ndime)
    character(len=8) :: elrefp
!
! ======================================================================
! COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
! THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
! IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
! THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
! (AT YOUR OPTION) ANY LATER VERSION.
!
! THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
! WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
! MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
! GENERAL PUBLIC LICENSE FOR MORE DETAILS.
!
! YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
! ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
!   1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
! ======================================================================
!                      TROUVER LES PTS MILIEUX ENTRE LES EXTREMITES DE
!                      L'ARETE ET LE POINT D'INTERSECTION
!
!     ENTREE
!       NDIM    : DIMENSION TOPOLOGIQUE DU MAILLAGE
!       PINTER  : COORDONNÉES DES POINTS D'INTERSECTION
!       TABAR   : COORDONNEES DES 3 NOEUDS DE L'ARETE
!       AREINT  : POSITION DU PT INTER DE L'ARETE DANS LA LISTE
!
!     SORTIE
!       MILARA  : COOR DU PT MILIEU ENTRE 1ER PT DE COORSG ET PT INTER
!       MILARB  : COOR DU PT MILIEU ENTRE 2EM PT DE COORSG ET PT INTER
!     ----------------------------------------------------------------
!
    integer :: nno, j
    real(kind=8) :: x(81)
!
! --------------------------------------------------------------------
!
    call xelrex(elrefp, nno, x)
!
    do j = 1,ndime
      ksia(j)=(pinref(ndime*(ip-1)+j)+x(ndime*(ia-1)+j))/2.d0
      ksib(j)=(pinref(ndime*(ip-1)+j)+x(ndime*(ib-1)+j))/2.d0
    enddo
!
    call reerel(elrefp, nno, ndim, geom, ksia,&
                milara)
    call reerel(elrefp, nno, ndim, geom, ksib,&
                milarb)
!
end subroutine
