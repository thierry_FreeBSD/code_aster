!
! COPYRIGHT (C) 1991 - 2013  EDF R&D                WWW.CODE-ASTER.ORG
!
! THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
! IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
! THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
! (AT YOUR OPTION) ANY LATER VERSION.
!
! THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
! WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
! MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
! GENERAL PUBLIC LICENSE FOR MORE DETAILS.
!
! YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
! ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
! 1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
!
interface
    subroutine cfacat(ndim, indic, nbliac, ajliai, spliai,&
                      llf, llf1, llf2, indfac, nesmax,&
                      defico, resoco, solveu, lmat, nbliai,&
                      xjvmax)
        integer :: ndim
        integer :: indic
        integer :: nbliac
        integer :: ajliai
        integer :: spliai
        integer :: llf
        integer :: llf1
        integer :: llf2
        integer :: indfac
        integer :: nesmax
        character(len=24) :: defico
        character(len=24) :: resoco
        character(len=19) :: solveu
        integer :: lmat
        integer :: nbliai
        real(kind=8) :: xjvmax
    end subroutine cfacat
end interface
