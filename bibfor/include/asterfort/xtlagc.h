! COPYRIGHT (C) 1991 - 2013  EDF R&D                WWW.CODE-ASTER.ORG
!
! THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
! IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
! THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
! (AT YOUR OPTION) ANY LATER VERSION.
!
! THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
! WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
! MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
! GENERAL PUBLIC LICENSE FOR MORE DETAILS.
!
! YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
! ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
! 1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
!
interface
    subroutine xtlagc(typmai, ndim, nnc, jnn, nddls,&
                      nface, cface, jdepde, jpcai, ffc,&
                      nconta, nfhe, lmulti, heavno, dlagrc)
        character(len=8) :: typmai
        integer :: ndim
        integer :: nnc
        integer :: jnn(3)
        integer :: nddls
        integer :: nface
        integer :: cface(5, 3)
        integer :: jdepde
        integer :: jpcai
        real(kind=8) :: ffc(9)
        integer :: nconta
        integer :: nfhe
        logical :: lmulti
        integer :: heavno(8)
        real(kind=8) :: dlagrc
    end subroutine xtlagc
end interface
