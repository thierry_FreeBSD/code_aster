!
! COPYRIGHT (C) 1991 - 2013  EDF R&D                WWW.CODE-ASTER.ORG
!
! THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
! IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
! THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
! (AT YOUR OPTION) ANY LATER VERSION.
!
! THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
! WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
! MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
! GENERAL PUBLIC LICENSE FOR MORE DETAILS.
!
! YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
! ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
! 1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
!
interface
    subroutine aceinc(noma, nomo, nbmcf, mclf, ntyele,&
                      nbocc, ivr, nbepo, nbedi, nbeco,&
                      nbeca, nbeba, nbema, nbegb, nbemb,&
                      nbtel, locaco, locagb, locamb, jdlm,&
                      jdln, lmax, ier)
        character(len=8) :: noma
        character(len=8) :: nomo
        integer :: nbmcf
        character(len=16) :: mclf(*)
        integer :: ntyele(*)
        integer :: nbocc(*)
        integer :: ivr(*)
        integer :: nbepo
        integer :: nbedi
        integer :: nbeco
        integer :: nbeca
        integer :: nbeba
        integer :: nbema
        integer :: nbegb
        integer :: nbemb
        integer :: nbtel
        logical :: locaco
        logical :: locagb
        logical :: locamb
        integer :: jdlm
        integer :: jdln
        integer :: lmax
        integer :: ier
    end subroutine aceinc
end interface
