!
! COPYRIGHT (C) 1991 - 2013  EDF R&D                WWW.CODE-ASTER.ORG
!
! THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
! IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
! THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
! (AT YOUR OPTION) ANY LATER VERSION.
!
! THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
! WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
! MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
! GENERAL PUBLIC LICENSE FOR MORE DETAILS.
!
! YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
! ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
! 1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
!
interface
    subroutine nmfgas(fami, npg, icodma, pgl, nno,&
                      nc, ugl, effnom, pm, crit,&
                      tmoins, tplus, xlong0, a, coeffl,&
                      irram, irrap, kls, flc, effnoc,&
                      pp)
        character(len=*) :: fami
        integer :: npg
        integer :: icodma
        real(kind=8) :: pgl(3, 3)
        integer :: nno
        integer :: nc
        real(kind=8) :: ugl(12)
        real(kind=8) :: effnom
        real(kind=8) :: pm(3)
        real(kind=8) :: crit(*)
        real(kind=8) :: tmoins
        real(kind=8) :: tplus
        real(kind=8) :: xlong0
        real(kind=8) :: a
        real(kind=8) :: coeffl(7)
        real(kind=8) :: irram
        real(kind=8) :: irrap
        real(kind=8) :: kls(78)
        real(kind=8) :: flc
        real(kind=8) :: effnoc
        real(kind=8) :: pp(3)
    end subroutine nmfgas
end interface
