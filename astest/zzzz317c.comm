# TITRE GENERATION DE SIGNAUX SISMIQUES OPTION DSP: CAS SEPARABLE ET DONNE ACCE_MAXI
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY  
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY  
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR     
# (AT YOUR OPTION) ANY LATER VERSION.
#                                                                       
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT   
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF            
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              
#                                                                       
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,         
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        
# ======================================================================
# person_in_charge: irmela.zentner at edf.fr 
#
DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET',VISU_EFICAS='NON'), PAR_LOT='NON')
#
from math import pi,sqrt,log,exp, ceil
import numpy as NP
###############################################
DT=0.01
NBPO=2**10
frr=2.0
amor=0.6
wg=frr*2.*pi
ecart=5.
####################################################
#
####################################################
# SIMULATION POUR DSP SEPARABLE ET DONNE ACCE_MAXI
####################################################
#
####################################################
TSM=8.
t_ini=0.5
ecart=1.0
pga=0.2
#
#Attention: ces valeurs sont les moyennes utilises pour la simulation
# - on peut faire des tests de non regression pour une realisation
# - on teste les moyennes pour le cas TYPE=   "CONST"
#
##############################
# TYPE "JH"
##############################
# OPTION ACCE_MAXI
#####
ACCE1=GENE_ACCE_SEISME( INFO =2,  INIT_ALEA=1000000,TITRE='DSP1',
              PAS_INST       =DT,NB_POIN=2**10,PESANTEUR =9.81, DUREE_PHASE_FORTE   =10.1,
              DSP        = _F(AMOR_REDUIT  =amor,FREQ_FOND =frr,),
              MODULATION      = _F(TYPE=   "JENNINGS_HOUSNER",   ACCE_MAX=pga),),

FONCT1=RECU_FONCTION( TABLE=ACCE1,
                              FILTRE=_F(  NOM_PARA = 'NUME_ORDRE',
                                       VALE_I = 1),
                              NOM_PARA_TABL='FONCTION',)

RMS1=INFO_FONCTION( ECART_TYPE=_F(  FONCTION = FONCT1))
N1=INFO_FONCTION(  NOCI_SEISME=_F(FONCTION = FONCT1,  OPTION = 'MAXI') )

IMPR_TABLE (TABLE=N1,  UNITE=8,);
#
TEST_TABLE(
           VALE_CALC=2.5626567182842,
           NOM_PARA='ACCE_MAX',
           TABLE=N1,
           )

TEST_TABLE(
           VALE_CALC=0.62762979215026,
#           VALE_REFE=0.603,PRECISION=0.1,REFERENCE='ANALYTIQUE',
           NOM_PARA='ECART_TYPE',
           TABLE=RMS1,)

##############################
# TYPE "GAMMA"
##############################
#  ACCE_MAXI
#####
ACCE3=GENE_ACCE_SEISME( INFO =2,INIT_ALEA=1000000,TITRE='DSP3',
              PAS_INST       =DT,PESANTEUR =9.81,DUREE_PHASE_FORTE   =10.1,
              DSP        = _F(AMOR_REDUIT  =amor,FREQ_FOND =frr,),
              MODULATION      = _F(TYPE=   "GAMMA",  INST_INI= t_ini,  ACCE_MAX=pga), )

FONCT3=RECU_FONCTION(TABLE=ACCE3,
                     FILTRE=_F(  NOM_PARA = 'NUME_ORDRE', VALE_I = 1),
                              NOM_PARA_TABL='FONCTION',)

N3=INFO_FONCTION(  NOCI_SEISME=_F(
                             FONCTION = FONCT3,OPTION = ('MAXI'), ))


RMS3=INFO_FONCTION( ECART_TYPE=_F(  FONCTION = FONCT3));
#

TEST_TABLE(
           VALE_CALC=1.9725903791494,
           NOM_PARA='ACCE_MAX',
           TABLE=N3,
           )

#TEST_TABLE( TABLE=RMS3,
#                NOM_PARA='ECART_TYPE', REFERENCE='AUTRE_ASTER', PRECISION=1.E-1,
#                    VALE=0.60178 )

##############################
# TYPE "CONSTANT"
##############################
# OPTION ACCE_MAXI
########
ACCE2b=GENE_ACCE_SEISME( INFO =2,INIT_ALEA=10000, TITRE='DSP2',
              PAS_INST       =DT,NB_POIN=2**10,PESANTEUR =9.81,DUREE_PHASE_FORTE   =TSM,
              DSP        = _F(AMOR_REDUIT  =amor,FREQ_FOND =frr,),
               MODULATION      = _F(TYPE="CONSTANT",  ACCE_MAX=pga),)

FONCT2b=RECU_FONCTION(    TABLE=ACCE2b,
                              FILTRE=_F(  NOM_PARA = 'NUME_ORDRE',
                                       VALE_I = 1),
                              NOM_PARA_TABL='FONCTION',)
RMS2b=INFO_FONCTION( ECART_TYPE=_F(  FONCTION = FONCT2b))
N2b=INFO_FONCTION(  NOCI_SEISME=_F(
                             FONCTION = FONCT2b, OPTION = ('MAXI'),
) )

IMPR_TABLE (TABLE=N2b,  UNITE=8,);


TEST_TABLE(
           VALE_CALC=2.0308833854998,
           NOM_PARA='ACCE_MAX',
           TABLE=N2b,
           )

TEST_TABLE(
           VALE_CALC=0.61988828780165,
           NOM_PARA='ECART_TYPE',
           TABLE=RMS2b,
           )

TEST_TABLE(
           VALE_CALC=0.61988828780165,
#           VALE_REFE=0.60177999999999998,PRECISION=0.10000000000000001,REFERENCE='AUTRE_ASTER',
           NOM_PARA='ECART_TYPE',
           TABLE=RMS2b,)

########################################################
# TEST DU MAX MEDIAN DES SIGNAUX AVEC OPTION PGA
########################################################
# CONSTANT
l_PGA2=[]
Nsim2=25
for iii in range(Nsim2):
   DETRUIRE(CONCEPT=_F(NOM=(ACCE2b, FONCT2b, N2b))) 
   ACCE2b=GENE_ACCE_SEISME( PAS_INST       =DT, NB_POIN=2**10,PESANTEUR =9.81, DUREE_PHASE_FORTE   =TSM,
              DSP        = _F(AMOR_REDUIT  =amor,FREQ_FOND =frr,),
               MODULATION      = _F(TYPE="CONSTANT",  ACCE_MAX=pga),)

   FONCT2b=RECU_FONCTION(    TABLE=ACCE2b,
                              FILTRE=_F(  NOM_PARA = 'NUME_ORDRE',
                                       VALE_I = 1),
                              NOM_PARA_TABL='FONCTION',)

   N2b=INFO_FONCTION(  NOCI_SEISME=_F(
                             FONCTION = FONCT2b, OPTION = ('MAXI')) )

   tab2b = N2b.EXTR_TABLE()
   l_PGA2.append(tab2b.ACCE_MAX[0])

PGA_MED2=NP.median(NP.array(l_PGA2))/9.81
print "median PGA", PGA_MED2
print 'mean PGA FIN', NP.mean(NP.array(l_PGA2))/9.81

########################################################
#JH
#
l_PGA1=[]
Nsim1=25
for iii in range(Nsim1): 
   print iii , ' JH'
   
   ACCE1b=GENE_ACCE_SEISME(PAS_INST       =DT,PESANTEUR =9.81, DUREE_PHASE_FORTE =TSM,
              DSP        = _F(AMOR_REDUIT  =amor,FREQ_FOND =frr,),
              MODULATION      = _F(TYPE=   "JENNINGS_HOUSNER", ACCE_MAX =pga), )

   FONCT1b=RECU_FONCTION(    TABLE=ACCE1b,
                              FILTRE=_F(  NOM_PARA = 'NUME_ORDRE',
                                       VALE_I = 1),
                              NOM_PARA_TABL='FONCTION',)

   N1b=INFO_FONCTION(  NOCI_SEISME=_F(
                             FONCTION = FONCT1, OPTION = ('MAXI')) )
   tab1 = N1b.EXTR_TABLE()
   l_PGA1.append(tab1.ACCE_MAX[0])
   DETRUIRE(CONCEPT=_F(NOM=(ACCE1b, FONCT1b, N1b)))
      
PGA_MED1=NP.median(NP.array(l_PGA1))/9.81
print "median PGA", PGA_MED1
print 'mean PGA FIN', NP.mean(NP.array(l_PGA1))/9.81



########################################################
#GAMMA
#
l_PGA3=[]
Nsim3=25
for iii in range(Nsim3):
   DETRUIRE(CONCEPT=_F(NOM=(ACCE3, FONCT3, N3)))
    
   ACCE3=GENE_ACCE_SEISME(PAS_INST       =DT,PESANTEUR =9.81,DUREE_PHASE_FORTE =TSM,
              DSP        = _F(AMOR_REDUIT  =amor,FREQ_FOND =frr,),
              MODULATION      = _F(TYPE=   "GAMMA",
                                    INST_INI= t_ini,  ACCE_MAX =pga), )

   FONCT3=RECU_FONCTION(    TABLE=ACCE3,
                              FILTRE=_F(  NOM_PARA = 'NUME_ORDRE',
                                       VALE_I = 1),
                              NOM_PARA_TABL='FONCTION',)

   N3=INFO_FONCTION(  NOCI_SEISME=_F(
                             FONCTION = FONCT3, OPTION = ('MAXI')) )

   tab3 = N3.EXTR_TABLE()
   l_PGA3.append(tab3.ACCE_MAX[0])

PGA_MED3=NP.median(NP.array(l_PGA3))/9.81
print "median PGA", PGA_MED3
print 'mean PGA FIN', NP.mean(NP.array(l_PGA3))/9.81


########################################################
## TEST TABLE

tabres=CREA_TABLE(LISTE = (
          _F(    PARA='GAMMA',      LISTE_R=(PGA_MED3,),),
          _F(    PARA='CONSTANT', LISTE_R=(PGA_MED2,),),
          _F(    PARA='JH',         LISTE_R=(PGA_MED1,),),))

TEST_TABLE(
           VALE_CALC=0.22434161546456,
           NOM_PARA='GAMMA',
           TABLE=tabres,
           )

TEST_TABLE(
           VALE_CALC=0.26122902327056,
           NOM_PARA='JH',
           TABLE=tabres,
           )

TEST_TABLE(
           VALE_CALC=0.19090118806923,
           NOM_PARA='CONSTANT',
           TABLE=tabres,
           )

FIN( )
