# TITRE TEST DE LIRE_RESU FORMAT MED DE  DEPL ET SIEF_ELNO
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# TEST ISSU DU TP2 THERMOPLASTICITE. PLAQUE TROUEE EN TRACTION (FORMA03)
# ======================================================================
# MODELISATION B: LECTURE DES CONTRAINTES  AU FORMAT MED
# EXEMPLE DE TRACE DE COURBES AVEC XMGRACE
#===========================================


DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET', VISU_EFICAS='OUI'), DEBUG=_F(SDVERI='OUI'))

# Renommage des groupes uniquement pour tester LIRE_MAILLAGE
MAIL = LIRE_MAILLAGE(
   FORMAT='MED',
   UNITE=81,
   RENOMME=(_F(NOM_MED='GM10',
               NOM='GRMA10',),),
)

MO=AFFE_MODELE(MAILLAGE=MAIL,
               AFFE=_F(TOUT='OUI',
                       PHENOMENE='MECANIQUE',
                       MODELISATION='C_PLAN',),);

#
# COURBE DE TRACTION
#

A5=DEFI_FONCTION(
                    NOM_PARA='EPSI',
                   VALE=(0.002,400.0,
                         0.003,500.0,
                         0.0045,550.0,
                         0.0065,580.0,
                         0.008,590.0,
                         0.01,600.0,
                         0.02,600.0,),
                    PROL_DROITE='CONSTANT',
                    PROL_GAUCHE='LINEAIRE',
                    );

MA=DEFI_MATERIAU(
 ELAS=_F(
  E  = 200000.,
  NU  = 0.3, ),
             TRACTION=_F(SIGM=A5,),
          )

CM=AFFE_MATERIAU(MAILLAGE=MAIL,
                 AFFE=_F(TOUT='OUI',
                         MATER=MA,),);

RESUNL=LIRE_RESU(TYPE_RESU='EVOL_NOLI',
              FORMAT='MED',
              MODELE=MO,
              FORMAT_MED=(_F( NOM_CHAM_MED='RESUPILODEPL____________________',
                              NOM_CHAM    ='DEPL' ),
                          _F( NOM_CHAM_MED='RESUPILOSIEF_ELNO_______________',
                              NOM_CHAM    ='SIEF_ELNO') ) ,
              TOUT_ORDRE='OUI',);


# CALCUL DES CHAMPS MOYENNES AUX NOEUDS

TDEP=1.4

RESUNL=CALC_CHAMP(  reuse=RESUNL,
                          CONTRAINTE='SIEF_NOEU',INST=TDEP,
                      RESULTAT=RESUNL
                        )

TEST_RESU(RESU=_F(GROUP_NO='PHAUTG',
                  INST=1.3999999999999999,
                  RESULTAT=RESUNL,
                  NOM_CHAM='DEPL',
                  NOM_CMP='DY',
                  VALE_CALC=1.3999999999999999,),
          )

TEST_RESU(RESU=_F(GROUP_NO='PHAUTG',
                  INST=1.3999999999999999,
                  RESULTAT=RESUNL,
                  NOM_CHAM='SIEF_NOEU',
                  NOM_CMP='SIYY',
                  VALE_CALC=541.56996562397001,),
          )

# SEGMENT VOISIN DE BD
LBAS=INTE_MAIL_2D(    MAILLAGE=MAIL,
                        DEFI_SEGMENT=_F(  ORIGINE = ( 10.,  0.1, ),
                                       EXTREMITE = ( 100.,  0.1, )))

SYYPRT=POST_RELEVE_T(
      ACTION=_F(  INTITULE = 'SYYBD',
              CHEMIN = LBAS, RESULTAT = RESUNL,
             NOM_CMP = 'SIYY', INST = TDEP,
            NOM_CHAM = 'SIEF_ELNO',
           OPERATION = 'EXTRACTION')
            )


SYYMLC10=MACR_LIGN_COUPE(RESULTAT=RESUNL,
                         MODELE=MO,
                         NOM_CHAM='SIEF_NOEU',
                         LIGN_COUPE=_F(NB_POINTS=10,
                                       COOR_ORIG=(10.0,0.1,0.,),
                                       COOR_EXTR=(100.0,0.1,0.,), ),);

SYYML100=MACR_LIGN_COUPE(RESULTAT=RESUNL,
                         MODELE=MO,
                         NOM_CHAM='SIEF_NOEU',
                         LIGN_COUPE=_F(NB_POINTS=100,
                                       COOR_ORIG=(10.0,0.1,0.,),
                                       COOR_EXTR=(100.0,0.1,0.,), ),);


FSYPRT   = RECU_FONCTION(TABLE=SYYPRT,  PARA_X='ABSC_CURV',PARA_Y='SIYY')
FSYMLC10 = RECU_FONCTION(TABLE=SYYMLC10,PARA_X='ABSC_CURV',PARA_Y='SIYY')
FSYML100 = RECU_FONCTION(TABLE=SYYML100,PARA_X='ABSC_CURV',PARA_Y='SIYY')


IMPR_FONCTION(
    UNITE     =  24,
    FORMAT    = 'XMGRACE',
    PILOTE    = 'POSTSCRIPT',
    LEGENDE_X = 'Abcisses curvilignes',
    LEGENDE_Y = 'Contraintes (MPa)',
    TITRE     = 'Plaque trouee',
    COURBE=(_F(FONCTION=FSYPRT),
            _F(FONCTION=FSYMLC10),
            _F(FONCTION=FSYML100),
           ))
FIN()
