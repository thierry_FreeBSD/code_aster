# TITRE TP ANALYSE MODALE TPAM2
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# person_in_charge: emmanuel.boyere at edf.fr
#-------------------------------------------------------------------
#                 CORRIGE TP ANALYSE MODAL N2
#                        MODES DE CORPS RIGIDE
#
# OBJECTIF POUR LA BASE DE CAS-TESTS ASTER: MODAL GENERALISE REEL.
# INTERCOMPARAISON DES METHODES DE SOUS-ESPACES (SORENSEN, LANCZOS,
# ET QZ).
#-------------------------------------------------------------------

#-------------------------------------------------------------------
#                       CALCUL ASTER
#-------------------------------------------------------------------
DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='OUI'))

# LECTURE MAILLAGE ET AFFECTATION MATERIAU/MODELE
ACIER=DEFI_MATERIAU(ELAS=_F(E=1.E8,
                            NU=0.3,
                            RHO=1.E4,),);
MAYA=LIRE_MAILLAGE(FORMAT='MED',);
MATER=AFFE_MATERIAU(MAILLAGE=MAYA,
                    AFFE=_F(TOUT='OUI',
                            MATER=ACIER,),);
STRUCTUR=AFFE_MODELE(MAILLAGE=MAYA,
                     AFFE=_F(TOUT='OUI',
                             PHENOMENE='MECANIQUE',
                             MODELISATION='3D',),);

# CALCULS DES MATRICES DE MASSE ET DE RIGIDITE
K_ELEM=CALC_MATR_ELEM(OPTION='RIGI_MECA',
                      MODELE=STRUCTUR,
                      CHAM_MATER=MATER,);
M_ELEM=CALC_MATR_ELEM(OPTION='MASS_MECA',
                      MODELE=STRUCTUR,
                      CHAM_MATER=MATER,);
NUMERO=NUME_DDL(MATR_RIGI=K_ELEM,);
K_ASSE=ASSE_MATRICE(MATR_ELEM=K_ELEM,
                    NUME_DDL=NUMERO,);

M_ASSE=ASSE_MATRICE(MATR_ELEM=M_ELEM,
                    NUME_DDL=NUMERO,);

#--------------------------------------------------------------------
# ANALYSE MODALE
# QUESTION 1: CALCUL BANDE AVEC SORENSEN

MODE_SOB=MODE_ITER_SIMULT(MATR_RIGI=K_ASSE,
                          MATR_MASS=M_ASSE,
                          METHODE='SORENSEN',
                          CALC_FREQ=_F(OPTION='BANDE',
                                       FREQ=(-.3,2700.0,),
#                                       SEUIL_FREQ=.1,
#                                       NMAX_ITER_SHIFT=6,
                                       ),
                          TITRE='1/ METHODE DE SORENSEN',);

TEST_RESU(RESU=_F(NUME_ORDRE=1,
                  PARA='FREQ',
                  RESULTAT=MODE_SOB,
                  VALE_CALC=-1.3737736757768E-04,
                  VALE_REFE=0.,
                  TOLE_MACHINE=2.E-03,
# TOLE_MACHINE JUSTIFIEE CAR ON TESTE UN ZERO NUMERIQUE (FREQUENCE DE MODE DE CORPS RIGIDE)
# ISSUE DE MODE_ITER_SIMULT - ON NE PEUT PAS CONTROLER CE ZERO A MIEUX QUE 1.E-3
# A COMPARER A LA PREMIERE FREQUENCE NON NULLE QUI VAUT 2613 HZ
                  PRECISION=1.E-03,
                  REFERENCE='NON_DEFINI',
                  CRITERE='ABSOLU',
                  ),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=7,
                  PARA='FREQ',
                  RESULTAT=MODE_SOB,
                  VALE_CALC=2469.6406072596001,
                  CRITERE='RELATIF',
                  ),
          )

MODE_QZ=MODE_ITER_SIMULT(MATR_RIGI=K_ASSE,
                          MATR_MASS=M_ASSE,
                          METHODE='QZ',
                          TYPE_QZ='QZ_QR',
                          CALC_FREQ=_F(OPTION='BANDE',
                                       FREQ=(0.0,2700.0,),),
                          TITRE='1/ METHODE QZ',);

TEST_RESU(RESU=_F(NUME_ORDRE=1,
                  PARA='FREQ',
                  REFERENCE='NON_DEFINI',
                  RESULTAT=MODE_QZ,
                  VALE_CALC=-1.9359975896016E-03,
                  VALE_REFE=0.0,
                  TOLE_MACHINE=2.E-03,
# TOLE_MACHINE JUSTIFIEE CAR ON TESTE UN ZERO NUMERIQUE (FREQUENCE DE MODE DE CORPS RIGIDE)
# ISSUE DE MODE_ITER_SIMULT - ON NE PEUT PAS CONTROLER CE ZERO A MIEUX QUE 1.E-3
# A COMPARER A LA PREMIERE FREQUENCE NON NULLE QUI VAUT 2613 HZ
                  CRITERE='ABSOLU',
                  PRECISION=3.E-3,
                  ),
          )

TEST_RESU(RESU=_F(NUME_ORDRE=16,
                  PARA='FREQ',
                  RESULTAT=MODE_QZ,
                  VALE_CALC=2613.6412395658999,
                  CRITERE='RELATIF',
                  ),
          )

# IMPRESSIONS DES PARAMETRES EN SUS DANS .RESU
IMPR_RESU(RESU=_F(RESULTAT=MODE_SOB,
                  TOUT_CHAM='NON',
                  TOUT_PARA='OUI',),);

# IMPRESSION FORMAT CASTEM DES MODES PROPRES
IMPR_RESU(FORMAT='CASTEM',UNITE=55,
          RESU=(_F(
                   MAILLAGE=MAYA,),
                _F(
                   RESULTAT=MODE_SOB,),),);

# AFFICHAGE GIBI INTERACTIF VIA PYTHON
#os.system('cp %spost.dcas .' %directory)
#os.system('/logiciels/aster/outils/gibi2000.x post.dcas')

# FIN DE LA QUESTION 1.
#FIN();

# QUESTION 2: CALCUL BANDE AVEC LANCZOS SANS OU AVEC OPTION MODE RIGIDE

#MODE_LAS=MODE_ITER_SIMULT(MATR_RIGI=K_ASSE,
#                          MATR_MASS=M_ASSE,
#                          METHODE='TRI_DIAG',
#                          OPTION='SANS',
#                          CALC_FREQ=_F(OPTION='BANDE',
#                                       FREQ=(0.0,2800.0,),),
#                          VERI_MODE=_F(STOP_ERREUR='NON',),
#                          TITRE='2/ METHODE DE LANCZOS AVEC OPTION MODE RIGIDE',);
#IMPR_RESU(RESU=_F(RESULTAT=MODE_LAS,
#                  TOUT_CHAM='NON',
#                  TOUT_PARA='OUI',),);
MODE_LAA=MODE_ITER_SIMULT(MATR_RIGI=K_ASSE,
                          MATR_MASS=M_ASSE,
                          METHODE='TRI_DIAG',
                          OPTION='MODE_RIGIDE',
                          CALC_FREQ=_F(OPTION='BANDE',
                                       FREQ=(0.0,2800.0,),),
                          TITRE='2/ METHODE DE LANCZOS AVEC OPTION',);

IMPR_RESU(RESU=_F(RESULTAT=MODE_LAA,
                  TOUT_CHAM='NON',
                  TOUT_PARA='OUI',),);

TEST_RESU(RESU=_F(NUME_ORDRE=11,
                  PARA='FREQ',
                  RESULTAT=MODE_LAA,
                  VALE_CALC=2470.841,
                  ),
          )

# FIN DE LA QUESTION 2.
#FIN();

# QUESTION 3: CALCUL DES 16 PLUS PETITES AVEC SORENSEN
# ON UTILISE "SEUIL_FREQ" POUR EVITER LE SOUCI AVEC LES CORPS RIGIDES

MODE_SOP=MODE_ITER_SIMULT(MATR_RIGI=K_ASSE,
                           MATR_MASS=M_ASSE,
                           METHODE='SORENSEN',
                           CALC_FREQ=_F(OPTION='PLUS_PETITE',
                                       SEUIL_FREQ=1.,
                                        NMAX_FREQ=16,),
                          )

IMPR_RESU(RESU=_F(RESULTAT=MODE_SOP,
                  TOUT_CHAM='NON',
                  TOUT_PARA='OUI',),);

FIN();
