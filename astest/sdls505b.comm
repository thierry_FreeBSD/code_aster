# person_in_charge: patrick.massin at edf.fr
# DATE   : 10/07/2000
# AUTEUR : F. LEBOUVIER (DELTACAD)
# TITRE  : FLAMBEMENT D'UNE ENVELOPPE CYLINDRIQUE SOUS PRESSION
# ======================================================================
# COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR   
# (AT YOUR OPTION) ANY LATER VERSION.                                 
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT 
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF          
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU    
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                            
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE   
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,       
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.      
# ======================================================================
#          EXTERNE
#---------------------------------------------------------------------
#  - CALCUL DE STABILITE D'UNE ENVELOPPE CYLINDRIQUE MINCE
#    CALCUL DES CHARGES CRITIQUES CONDUISANT AU FLAMBEMENT ELASTIQUE
#  - MODELISATION AVEC DES ELEMENTS 'COQUE_3D'(QUAD9)
#---------------------------------------------------------------------
#
DEBUT( CODE=_F(NIV_PUB_WEB='INTERNET'))
#
MA=LIRE_MAILLAGE(FORMAT='MED', )

#
MAIL=CREA_MAILLAGE(     MAILLAGE=MA,
                       MODI_MAILLE=_F( TOUT = 'OUI',    
                                    OPTION = 'QUAD8_9',  
                                    )
                     )

MAIL=DEFI_GROUP(reuse=MAIL,  MAILLAGE=MAIL,         
                   CREA_GROUP_NO=_F( NOM = 'A',   NOEUD = 'NO7',  ))

#
MOEL=AFFE_MODELE(   MAILLAGE=MAIL,
                          AFFE=_F( TOUT = 'OUI',          
                                PHENOMENE = 'MECANIQUE',     
                                MODELISATION = 'COQUE_3D',  
                                )
                    )

#
#---------------------------------------------------------------------
#                     CARACTERISTIQUES ELEMENTAIRES
#---------------------------------------------------------------------
#
CAEL=AFFE_CARA_ELEM(   MODELE=MOEL,
                          COQUE=_F( GROUP_MA = 'TOUT_ELT',  
                                 EPAIS = 0.00045,     
                                 A_CIS = 9000.,     
                                 ANGL_REP = (90.,0.,),  
                                 )
                       )

#
#---------------------------------------------------------------------
#                     CARACTERISTIQUES MATERIAUX
#---------------------------------------------------------------------
#
MATERIAU=DEFI_MATERIAU(   ELAS=_F( E = 2.E+11,   
                                  NU = 0.3,  
                                  )
                          )

#
CHAM_MAT=AFFE_MATERIAU(   MAILLAGE=MAIL,
                                AFFE=_F( TOUT = 'OUI',   MATER = MATERIAU,  )
                          )

#
#---------------------------------------------------------------------
#                     CHARGEMENTS
#---------------------------------------------------------------------
#
CON_LI=AFFE_CHAR_MECA(        MODELE=MOEL,DDL_IMPO=(
                              _F( GROUP_NO = 'O_PRIME',  
                                        DX = 0.,   DY = 0.,  
                                       ),
                              _F( GROUP_NO = 'E',  
                                        DY = 0.,  
                                       ),
                              _F( GROUP_NO = 'CONTOUR',  
                                        DZ = 0.,  
                                       ))
                         )

#
CHARGE=AFFE_CHAR_MECA(        MODELE=MOEL,
                           FORCE_COQUE=_F( GROUP_MA = 'CYLINDRE',  
                                        PRES = -1.,      
                                        ),
                           FORCE_ARETE=_F( GROUP_MA = 'ARETE',  
                                         FZ = -0.025,        
                                        ) 
                        )

#
#---------------------------------------------------------------------
#                        RESOLUTION
#---------------------------------------------------------------------
#
RES=MECA_STATIQUE(      MODELE=MOEL,
                      CHAM_MATER=CHAM_MAT,
                       CARA_ELEM=CAEL,EXCIT=(
                           _F( CHARGE = CON_LI,  ),
                           _F( CHARGE = CHARGE,  )),
                    )

#
SIG=CREA_CHAMP(   RESULTAT=RES,
                   OPERATION='EXTR',
                   TYPE_CHAM='ELGA_SIEF_R',
                    NOM_CHAM='SIEF_ELGA',
                   TYPE_MAXI='MINI',
                   TYPE_RESU='VALE'
                 )

#
MEL_RI_G=CALC_MATR_ELEM(     OPTION='RIGI_GEOM',
                               MODELE=MOEL,
                            CARA_ELEM=CAEL,
                            SIEF_ELGA=SIG
                          )

#
MEL_RI_M=CALC_MATR_ELEM(      OPTION='RIGI_MECA',
                                MODELE=MOEL,
                            CHAM_MATER=CHAM_MAT,
                             CARA_ELEM=CAEL,
                                CHARGE=(CHARGE, CON_LI,)
                          )

#
NUM=NUME_DDL(   MATR_RIGI=MEL_RI_M)

#
MAS_RI_M=ASSE_MATRICE(   MATR_ELEM=MEL_RI_M,   NUME_DDL=NUM)

#
MAS_RI_G=ASSE_MATRICE(   MATR_ELEM=MEL_RI_G,   NUME_DDL=NUM)

#
RESULT=MODE_ITER_SIMULT(      MATR_RIGI=MAS_RI_M,
                                MATR_RIGI_GEOM=MAS_RI_G,
#                               METHODE='TRI_DIAG',
                               METHODE='SORENSEN',
                             CALC_CHAR_CRIT=_F( OPTION = 'PLUS_PETITE',     ),
                             TYPE_RESU='MODE_FLAMB',
                             VERI_MODE=_F( SEUIL = 5.E-5,   )

                           )


RES_NORM=NORM_MODE(    MODE=RESULT,
                        NORME='TRAN'
                      )

MODE_1=CREA_CHAMP(     RESULTAT=RES_NORM,
                        OPERATION='EXTR',
                        TYPE_CHAM='NOEU_DEPL_R',
                         NOM_CHAM='DEPL',
                        NUME_MODE=10
                       )

RESU2=MODI_REPERE(
                  RESULTAT    = RES_NORM,
                  NUME_ORDRE  = 10,
                  MODI_CHAM   = (_F(NOM_CHAM    = 'DEPL',
                                   NOM_CMP     = ('DX','DY','DZ',),
                                   TYPE_CHAM   = 'VECT_3D',),),
                 REPERE     = 'CYLINDRIQUE',
                 AFFE = _F(
                                   ORIGINE    = (0.0, 0.0, 0.0,),
                                   AXE_Z      = (0.0, 0.0, 1.0,),
                                  ),
                  INFO=2,
                )

#
#---------------------------------------------------------------------
#                        VERIFICATION DES RESULTATS
#---------------------------------------------------------------------
#
TEST_RESU(RESU=_F(PARA='CHAR_CRIT',
                  NUME_MODE=10,
                  RESULTAT=RESULT,
                  VALE_CALC=-1557602.30488,
                  VALE_REFE=-1.523000E6,
                  REFERENCE='NON_DEFINI',
                  CRITERE='RELATIF',
                  PRECISION=0.050999999999999997,),)

TEST_RESU(CHAM_NO=(_F(NOEUD='NO180',
                      CRITERE='RELATIF',
                      NOM_CMP='DX',
                      PRECISION=0.29999999999999999,
                      TOLE_MACHINE=2.E-04,
                      CHAM_GD=MODE_1,
                      VALE_CALC=0.100269294622,
                      VALE_REFE=0.13595420899999999,
                      REFERENCE='NON_DEFINI',),
                   _F(NOEUD='NO180',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.14999999999999999,
                      CHAM_GD=MODE_1,
                      VALE_CALC=-0.661804759739,
                      VALE_REFE=-0.77440584000000001,
                      REFERENCE='NON_DEFINI',),
                   _F(NOEUD='NO221',
                      CRITERE='RELATIF',
                      NOM_CMP='DX',
                      PRECISION=1.E-2,
                      CHAM_GD=MODE_1,
                      VALE_CALC=0.177452165684,
                      VALE_REFE=0.17743796000000001,
                      REFERENCE='NON_DEFINI',),
                   _F(NOEUD='NO221',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=1.E-2,
                      CHAM_GD=MODE_1,
                      VALE_CALC=0.766029616244,
                      VALE_REFE=0.76596580000000003,
                      REFERENCE='NON_DEFINI',),
                   _F(NOEUD='NO180',
                      CRITERE='RELATIF',
                      NOM_CMP='DX',
                      CHAM_GD=MODE_1,
                      VALE_CALC=0.100269294622,
                      TOLE_MACHINE=1.E-3,),
                   _F(NOEUD='NO180',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      CHAM_GD=MODE_1,
                      VALE_CALC=-0.661804759739,
                      TOLE_MACHINE=1.E-3,),
                   _F(NOEUD='NO221',
                      CRITERE='RELATIF',
                      NOM_CMP='DX',
                      CHAM_GD=MODE_1,
                      VALE_CALC=0.177452165684,
                      TOLE_MACHINE=1.E-3,),
                   _F(NOEUD='NO221',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      CHAM_GD=MODE_1,
                      VALE_CALC=0.766029616244,
                      TOLE_MACHINE=1.E-3,),
                   ),
          )

TEST_RESU(RESU=(_F(NUME_ORDRE=10,
                   RESULTAT=RESU2,
                   NOM_CHAM='DEPL',
                   NOEUD='NO180',
                   NOM_CMP='DX',
                   VALE_CALC=-0.668539959731,
                   VALE_REFE=-0.78598699999999999,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   PRECISION=0.20000000000000001,),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU2,
                   NOM_CHAM='DEPL',
                   NOEUD='NO221',
                   NOM_CMP='DX',
                   VALE_CALC=0.786052784263,
                   VALE_REFE=0.78598699999999999,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   PRECISION=0.025000000000000001,),
                _F(NUME_ORDRE=10,
                   RESULTAT=RESU2,
                   NOM_CHAM='DEPL',
                   NOEUD='NO221',
                   NOM_CMP='DZ',
                   VALE_CALC=-0.0202895150509,
                   VALE_REFE=-0.02028837,
                   REFERENCE='NON_DEFINI',
                   CRITERE='RELATIF',
                   TOLE_MACHINE=1.e-5,
                   PRECISION=1.E-2,),
                ),
          )

#
FIN()
#
