# TITRE CHOC D'UNE POUTRE SUR UN APPUI : CALCUL DE REFERENCE
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# CAS_TEST__: SDNL104A
#
# CALCUL DE LA REPONSE TRANSITOIRE NON-LINEAIRE D'UNE POUTRE EN FLEXION
# CHOQUANT SUR UN APPUI ELASTIQUE.
# CE CALCUL SERT DE REFERENCE AUX CAS TESTS PAR SOUS-STRUCTURATION.
#
DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='OUI'))

#
MAYA=LIRE_MAILLAGE( )

MAYA=DEFI_GROUP( reuse=MAYA,   MAILLAGE=MAYA,
  CREA_GROUP_MA=_F(  NOM = 'TOUT', TOUT = 'OUI'))

#
MATERIO1=DEFI_MATERIAU(  ELAS=_F( RHO = 1.E06,  NU = 0.3,  E = 1.E10))

#
CHMAT=AFFE_MATERIAU(  MAILLAGE=MAYA,
                              AFFE=_F( TOUT = 'OUI',  MATER = MATERIO1))

#
BARRE=AFFE_MODELE(  MAILLAGE=MAYA,
                            AFFE=_F( TOUT = 'OUI',
                                  MODELISATION = 'POU_D_E',
                                  PHENOMENE = 'MECANIQUE'))

#
CARA=AFFE_CARA_ELEM(  MODELE=BARRE,
                           POUTRE=_F(
       GROUP_MA = 'TOUT',
    SECTION = 'CERCLE',
                                   CARA = 'R',      VALE = 0.1))

#
GUIDAGE=AFFE_CHAR_MECA(    MODELE=BARRE,DDL_IMPO=(
                           _F( TOUT = 'OUI',
                                     DX = 0., DZ = 0., DRX = 0., DRY = 0.),
                           _F( NOEUD = 'N1',    DY = 0.,  DRZ = 0.)))

#
CHARTRAN=AFFE_CHAR_MECA(        MODELE=BARRE,
                           FORCE_NODALE=_F( NOEUD = 'N11',  FY = -1000.))

#
K_ELEM=CALC_MATR_ELEM(      MODELE=BARRE,
                             CARA_ELEM=CARA,
                            CHAM_MATER=CHMAT,
                               OPTION='RIGI_MECA',
                                CHARGE=GUIDAGE )

#
M_ELEM=CALC_MATR_ELEM(      MODELE=BARRE,
                             CARA_ELEM=CARA,
                            CHAM_MATER=CHMAT,
                               OPTION='MASS_MECA',
                                CHARGE=GUIDAGE )

#
V_ELEM=CALC_VECT_ELEM( OPTION='CHAR_MECA',
                            CHARGE=CHARTRAN)

#
NUM=NUME_DDL(  MATR_RIGI=K_ELEM)

#
K_ASSE=ASSE_MATRICE( MATR_ELEM=K_ELEM,   NUME_DDL=NUM)

M_ASSE=ASSE_MATRICE( MATR_ELEM=M_ELEM,   NUME_DDL=NUM)

V_ASSE=ASSE_VECTEUR( VECT_ELEM=V_ELEM,   NUME_DDL=NUM)

#
MODES=MODE_ITER_SIMULT(     MATR_RIGI=K_ASSE,
                                 MATR_MASS=M_ASSE,
                             CALC_FREQ=_F( NMAX_FREQ = 5) )


#
###
###### PROJECTIONS SUR BASE MODALE :
###
#
NUMEMODE=NUME_DDL_GENE(       BASE=MODES,
                           STOCKAGE='DIAG')

#
K_PROJ=PROJ_MATR_BASE(            BASE=MODES,
                             NUME_DDL_GENE=NUMEMODE,
                                 MATR_ASSE=K_ASSE)

#
M_PROJ=PROJ_MATR_BASE(            BASE=MODES,
                             NUME_DDL_GENE=NUMEMODE,
                                 MATR_ASSE=M_ASSE)

#
F_PROJ=PROJ_VECT_BASE(            BASE=MODES,
                             NUME_DDL_GENE=NUMEMODE,
                                 VECT_ASSE=V_ASSE)

#
# DEFINITION DE L'OBSTACLE
#
OBSTACLE=DEFI_OBSTACLE( TYPE='CERCLE')

#
###
###### CALCULS TRANSITOIRES SUR BASE MODALE
###
#
TRAN_GE1=DYNA_VIBRA( TYPE_CALCUL='TRAN',
                   BASE_CALCUL='GENE',
                   SCHEMA_TEMPS=_F(SCHEMA='EULER',),
                             MATR_MASS=M_PROJ,
                             MATR_RIGI=K_PROJ,
                                EXCIT=_F(VECT_ASSE_GENE = F_PROJ,
                                       COEF_MULT = 1.),
                             INCREMENT=_F(  INST_INIT = 0.,
                                         INST_FIN = 1.,
                                         PAS = 1.E-4),
                            ARCHIVAGE=_F( PAS_ARCH = 10),
                                 CHOC=_F( NOEUD_1 = 'N11',
                                       OBSTACLE = OBSTACLE,
                                       ORIG_OBST = (1., 0., 0.,),
                                       NORM_OBST = (1., 0., 0.,),
                                       JEU = 1.E-4,
                                       RIGI_NOR = 1.E+8)
                           )

#
TRAN_GE2=DYNA_VIBRA(  TYPE_CALCUL='TRAN',
                   BASE_CALCUL='GENE',
                   SCHEMA_TEMPS=_F(SCHEMA='DEVOGE',),
                             MATR_MASS=M_PROJ,
                             MATR_RIGI=K_PROJ,
                                EXCIT=_F(VECT_ASSE_GENE = F_PROJ,
                                       COEF_MULT = 1.),
                             INCREMENT=_F(  INST_INIT = 0.,
                                         INST_FIN = 1.,
                                         PAS = 1.E-4),
                            ARCHIVAGE=_F( PAS_ARCH = 10),
                                 CHOC=_F( NOEUD_1 = 'N11',
                                       OBSTACLE = OBSTACLE,
                                       ORIG_OBST = (1., 0., 0.,),
                                       NORM_OBST = (1., 0., 0.,),
                                       JEU = 1.E-4,
                                       RIGI_NOR = 1.E+8)
                           )

#
TRAN_GE3=DYNA_VIBRA(  TYPE_CALCUL='TRAN',
                   BASE_CALCUL='GENE',
                   SCHEMA_TEMPS=_F(SCHEMA='ADAPT_ORDRE2',),
                             MATR_MASS=M_PROJ,
                             MATR_RIGI=K_PROJ,
                                EXCIT=_F(VECT_ASSE_GENE = F_PROJ,
                                       COEF_MULT = 1.),
                             INCREMENT=_F(  INST_INIT = 0.,
                                         INST_FIN = 1.,
                                         PAS_MAXI = 1.E-4,
                                         PAS = 1.E-5),
                            ARCHIVAGE=_F( PAS_ARCH = 10),
                                 CHOC=_F( NOEUD_1 = 'N11',
                                       OBSTACLE = OBSTACLE,
                                       ORIG_OBST = (1., 0., 0.,),
                                       NORM_OBST = (1., 0., 0.,),
                                       JEU = 1.E-4,
                                       RIGI_NOR = 1.E+8)
                            )

#
LIST_R=DEFI_LIST_REEL(       DEBUT=0.,
                           INTERVALLE=_F( JUSQU_A = 1.,
                                       NOMBRE = 10))

#
TRAN1=REST_GENE_PHYS(   RESU_GENE=TRAN_GE1,
                            TOUT_CHAM='OUI',
                             LIST_INST=LIST_R,
                             INTERPOL='LIN')

#
TRAN2=REST_GENE_PHYS(   RESU_GENE=TRAN_GE2,
                            TOUT_CHAM='OUI',
                             LIST_INST=LIST_R,
                             INTERPOL='LIN')

#
TRAN3=REST_GENE_PHYS(   RESU_GENE=TRAN_GE3,
                            TOUT_CHAM='OUI',
                             LIST_INST=LIST_R,
                             INTERPOL='LIN')

#
TEST_RESU(RESU=(
               _F(INST=1.0,
                  RESULTAT=TRAN1,
                  NOM_CHAM='DEPL',
                  NOEUD='N11',
                  NOM_CMP='DY',
                  VALE_CALC=-1.2548329076485E-04,),
               _F(INST=1.0,
                  RESULTAT=TRAN1,
                  NOM_CHAM='VITE',
                  NOEUD='N11',
                  NOM_CMP='DY',
                  VALE_CALC=8.3521301648811E-04,),
               _F(INST=1.0,
                  RESULTAT=TRAN1,
                  NOM_CHAM='ACCE',
                  NOEUD='N11',
                  NOM_CMP='DY',
                  VALE_CALC=0.36395279092512,),
               _F(INST=1.0,
                  RESULTAT=TRAN2,
                  NOM_CHAM='DEPL',
                  NOEUD='N11',
                  NOM_CMP='DY',
                  VALE_CALC=-1.254370289034E-04,),
               _F(INST=1.0,
                  RESULTAT=TRAN2,
                  NOM_CHAM='VITE',
                  NOEUD='N11',
                  NOM_CMP='DY',
                  VALE_CALC=8.4098546805027E-04,),
               _F(INST=1.0,
                  RESULTAT=TRAN2,
                  NOM_CHAM='ACCE',
                  NOEUD='N11',
                  NOM_CMP='DY',
                  VALE_CALC=0.28545359738930,),
               _F(INST=1.0,
                  RESULTAT=TRAN3,
                  NOM_CHAM='DEPL',
                  NOEUD='N11',
                  NOM_CMP='DY',
                  VALE_CALC=-1.2552515413372E-04,),
               _F(INST=1.0,
                  RESULTAT=TRAN3,
                  NOM_CHAM='VITE',
                  NOEUD='N11',
                  NOM_CMP='DY',
                  VALE_CALC=8.3285564859441E-04,),
               _F(INST=1.0,
                  RESULTAT=TRAN3,
                  NOM_CHAM='ACCE',
                  NOEUD='N11',
                  NOM_CMP='DY',
                  VALE_CALC=0.34578137533016,),
               ))

#
FIN()
#
