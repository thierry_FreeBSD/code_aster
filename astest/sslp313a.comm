# TITRE MURAKAMI 4.2 FISSURE INCLINEE DANS UNE PLAQUE ILLIMITEE
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# SOUMISE A UNE TRACTION UNIFORME A L'INFINI (MATERIAU HOMOGENE)
#

DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET'), DEBUG=_F(SDVERI='OUI'))

MA=LIRE_MAILLAGE(VERI_MAIL=_F(VERIF='OUI'),FORMAT='MED',)

MA=DEFI_GROUP( reuse=MA,   MAILLAGE=MA,
               CREA_GROUP_NO=_F( GROUP_MA = ( 'LL2',  'LL4', ))
                 )

MO=AFFE_MODELE(  MAILLAGE=MA,
                 AFFE=_F(  TOUT = 'OUI',
                           PHENOMENE = 'MECANIQUE',
                           MODELISATION = 'C_PLAN')  )

MAT=DEFI_MATERIAU( ELAS=_F(  E = 2.E11,
                            NU = 0.3,
                            ALPHA = 0.)  )

CHMAT=AFFE_MATERIAU(  MAILLAGE=MA,
                      AFFE=_F(  TOUT = 'OUI',  MATER = MAT)
                      )

CH=AFFE_CHAR_MECA(  MODELE=MO,
                    DDL_IMPO=( _F(  GROUP_NO = 'PS6',  DX = 0.),
                               _F(  GROUP_NO = 'LL4',  DY = 0.)),
                    FORCE_CONTOUR=_F( GROUP_MA = 'LL2',  FY = 100.E6)
                    )

CHAMDEPL=MECA_STATIQUE(  MODELE=MO,
                         CHAM_MATER=CHMAT,
                         EXCIT=_F( CHARGE = CH)
                          )

FOND1=DEFI_FOND_FISS(  MAILLAGE=MA,
                       FOND_FISS=_F( GROUP_NO = ('P0',)),
                       LEVRE_SUP=_F(GROUP_MA='LPOP8',),
                       LEVRE_INF=_F(GROUP_MA='LPOP8B',),
                     )

THETA01=CALC_THETA(  MODELE=MO,
                     THETA_2D=_F(  GROUP_NO = ('P0',),
                                   MODULE = 1.,
                                   R_INF = 0.00001875,
                                   R_SUP = 0.000075),
                     DIRECTION=(0.7986, 0.6018, 0.,)
                     )

G01=CALC_G(          RESULTAT=CHAMDEPL,
                     THETA=_F(THETA=THETA01),
                   )

GK01=CALC_G(              RESULTAT=CHAMDEPL,
                          THETA=_F(
                                   R_INF = 0.00001875,
                                   R_SUP = 0.000075,
                                   DIRECTION=(0.7986, 0.6018, 0.,),
                                   FOND_FISS=FOND1),
                          OPTION='CALC_K_G'
                      )

# references analytiques
Gref  = 1.001883E2
K1ref = 3.574968087E6
K2ref = 2.693931681E6

TEST_TABLE(REFERENCE='ANALYTIQUE',
           PRECISION=0.02,
           VALE_CALC=101.259394601,
           VALE_REFE=100.1883,
           NOM_PARA='G',
           TABLE=GK01,)

TEST_TABLE(REFERENCE='ANALYTIQUE',
           PRECISION=1.E-2,
           VALE_CALC= 3.60378020E+06,
           VALE_REFE=3.5749680869999998E6,
           NOM_PARA='K1',
           TABLE=GK01,)

TEST_TABLE(REFERENCE='ANALYTIQUE',
           PRECISION=1.E-2,
           VALE_CALC= 2.70021912E+06,
           VALE_REFE=2.6939316809999999E6,
           NOM_PARA='K2',
           TABLE=GK01,)

# ------------------------------------------------------------------

TABL_K=POST_K1_K2_K3(  MODELISATION='C_PLAN',
                       MATER=MAT,
                       RESULTAT = CHAMDEPL,
                       FOND_FISS = FOND1,
                       )

TEST_TABLE(REFERENCE='ANALYTIQUE',
           PRECISION=0.040000000000000001,
           VALE_CALC=96.852005483,
           VALE_REFE=100.1883,
           NOM_PARA='G',
           TABLE=TABL_K,)

TEST_TABLE(REFERENCE='ANALYTIQUE',
           PRECISION=0.012,
           VALE_CALC= 3.53793072E+06,
           VALE_REFE=3.5749680869999998E6,
           NOM_PARA='K1',
           TABLE=TABL_K,)

TEST_TABLE(REFERENCE='ANALYTIQUE',
           PRECISION=0.029999999999999999,
           VALE_CALC= 2.61790896E+06,
           VALE_REFE=2.6939316809999999E6,
           NOM_PARA='K2',
           TABLE=TABL_K,)

# ---------------
# on double ces tests de tests de non-regression

TEST_TABLE(
           VALE_CALC=96.852005483479005,
           
           NOM_PARA='G',
           TABLE=TABL_K,
           )

TEST_TABLE(
           VALE_CALC=3.5379307159775002E6,
           
           NOM_PARA='K1',
           TABLE=TABL_K,
           )

TEST_TABLE(
           VALE_CALC=2.6179089643526999E6,
           
           NOM_PARA='K2',
           TABLE=TABL_K,
           )

FIN()
#
