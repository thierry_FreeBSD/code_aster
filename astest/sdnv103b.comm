# TITRE IMPACT D UNE BARRE DE TAYLOR ELASTOPLASTIQUE
# ======================================================================
# COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
#
DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET'))

MA=LIRE_MAILLAGE(FORMAT='MED', )

MA=DEFI_GROUP(reuse=MA,MAILLAGE=MA,
              CREA_GROUP_NO=_F( TOUT_GROUP_MA = 'OUI'),
              )

MODE=AFFE_MODELE(
            MAILLAGE=MA,
            
            AFFE=(
           _F( TOUT='OUI',
               PHENOMENE = 'MECANIQUE',
               MODELISATION = ('AXIS','AXIS_SI')),),);

MA=MODI_MAILLAGE( reuse=MA,
             MAILLAGE=MA,
             ORIE_PEAU_2D=_F(  GROUP_MA = ('SZZ', 'RIGID')),
             INFO=2);

ACIER=DEFI_MATERIAU(
                ELAS=_F( E = 117.E3, NU = 0.35,  RHO = 8.930E-9),
                ECRO_LINE=_F(D_SIGM_EPSI = 100., SY = 400.));

CHMAT = AFFE_MATERIAU(
            MAILLAGE=MA,
            AFFE=(
              _F( GROUP_MA = ('VOL','VOL_RIG'), MATER = ACIER),),
                        );

CH=AFFE_CHAR_MECA(
            MODELE=MODE,
            DDL_IMPO=(
             _F( GROUP_MA = 'RIGID',      DX = 0., DY = 0.,),
             _F( GROUP_MA = 'VOL_RIG',    DX = 0., DY = 0.),
             _F( GROUP_MA = 'AXE',        DX = 0.),
             ),
               );


CHCONT  = DEFI_CONTACT(MODELE         = MODE,
                       FORMULATION    = 'DISCRETE',
                       REAC_GEOM      = 'CONTROLE',
                       NB_ITER_GEOM   = 2,
                       FROTTEMENT     = 'COULOMB',

                       ZONE =_F(
                             ALGO_CONT      = 'LAGRANGIEN',
                             ALGO_FROT      = 'PENALISATION',
                             COULOMB        = 0.25,
                             E_T            = 1.E5,
                             APPARIEMENT    = 'MAIT_ESCL',
                             GROUP_MA_MAIT  = 'RIGID',
                             GROUP_MA_ESCL  = 'SZZ',
                             COEF_MATR_FROT = 0.4,
                               ),
                      );

VIT_0=CREA_CHAMP( OPERATION='AFFE',
                  TYPE_CHAM='NOEU_DEPL_R',

                  MODELE=MODE,
                  AFFE=(
                     _F(TOUT='OUI',
                        NOM_CMP = ('DX', 'DY'),
                        VALE = (  0.0,  -227000.,)),
                     _F(GROUP_NO = 'VOL_RIG',
                        NOM_CMP = ('DX', 'DY'),
                        VALE = (  0.0,  0.0,   )),
                     _F(GROUP_NO = 'RIGID',
                        NOM_CMP = ('DX', 'DY'),
                        VALE = (  0.0,  0.0, )),
                    ));

L_INST=DEFI_LIST_REEL(DEBUT=-1.E-5,INTERVALLE=(
                         _F( JUSQU_A = -1.E-7, NOMBRE = 1),
                         _F( JUSQU_A =  1.E-6, NOMBRE = 4),
                         _F( JUSQU_A = 80.E-6, NOMBRE = 158))
                         )

L_SAUV=DEFI_LIST_REEL(DEBUT=-1.E-5,INTERVALLE=(
                         _F( JUSQU_A = 1.E-6, NOMBRE = 2),
                         _F( JUSQU_A = 80.E-6, NOMBRE = 79))
                         )

DEFLIST =DEFI_LIST_INST(DEFI_LIST=_F(LIST_INST = L_INST),
                        ECHEC=_F(ACTION='DECOUPE',
                                 SUBD_METHODE='MANUEL',
                                 SUBD_PAS=4,
                                 SUBD_PAS_MINI=1.E-30,),)

RESU=DYNA_NON_LINE(MODELE=MODE,
                   CHAM_MATER=CHMAT,
                   EXCIT=_F(CHARGE=CH,
                            ),
                   CONTACT = CHCONT,
                   ETAT_INIT=_F(VITE = VIT_0,
                                ),
                   COMPORTEMENT=_F(RELATION='VMIS_ISOT_LINE',
                                DEFORMATION='SIMO_MIEHE',
                                TOUT='OUI',),
                   INCREMENT=_F(LIST_INST=DEFLIST,
                                INST_FIN=8.E-05,
                                ),
                  SCHEMA_TEMPS=_F(SCHEMA='NEWMARK',
                                FORMULATION='DEPLACEMENT',
                                BETA = 0.49,   # Beta
                                GAMMA = 0.9  ), # Gamma
                  NEWTON=_F(REAC_INCR=1,
                            MATRICE='TANGENTE',
                            REAC_ITER=1,),
                   CONVERGENCE=_F(RESI_GLOB_RELA=1.E-6,
                                  ITER_GLOB_MAXI=20,
                                  ARRET='OUI',),
                   SOLVEUR=_F(SYME='OUI',
                              METHODE='LDLT', ),
                   ARCHIVAGE=_F(LIST_INST=L_SAUV),
                   );

#####################################################
# IMPRESSION DES RESULTATS SOUS DIFFERENTS FORMATS
#####################################################

IMPR_RESU(FORMAT='CASTEM',
          RESU=_F(MAILLAGE=MA,
                  RESULTAT=RESU,
                  NOM_CHAM=('DEPL'),),);

IMPR_RESU(FORMAT='RESULTAT',
          RESU=_F(RESULTAT=RESU,
                  GROUP_NO=('B1','A2'),
                  NOM_CHAM=('DEPL','VITE','ACCE'),),);


VITE_A2=POST_RELEVE_T(ACTION=_F(INTITULE='VITE EN A2',
                                GROUP_NO= 'A2',
                                RESULTAT=RESU,
                                NOM_CHAM='VITE',
                                NOM_CMP='DY',
                                OPERATION='EXTRACTION',),);

DEPL_A1=POST_RELEVE_T(ACTION=_F(INTITULE='DEPL EN A1',
                                GROUP_NO= 'A1',
                                RESULTAT=RESU,
                                NOM_CHAM='DEPL',
                                NOM_CMP='DY',
                                OPERATION='EXTRACTION',),);

DEPL_B1=POST_RELEVE_T(ACTION=_F(INTITULE='DEPL EN B1',
                                GROUP_NO= 'B1',
                                RESULTAT=RESU,
                                NOM_CHAM='DEPL',
                                NOM_CMP='DY',
                                OPERATION='EXTRACTION',),);


IMPR_TABLE(FORMAT='AGRAF',
           UNITE=25,
           TABLE=VITE_A2,
           NOM_PARA=('INST','DY',),
           TITRE='VITE EN A2',)

IMPR_TABLE(FORMAT='AGRAF',
           UNITE=25,
           TABLE=DEPL_A1,
           NOM_PARA=('INST','DY',),
           TITRE='DEPL EN A1',)

IMPR_TABLE(FORMAT='AGRAF',
           UNITE=25,
           TABLE=DEPL_B1,
           NOM_PARA=('INST','DY',),
           TITRE='DEPL EN B1',)

#####################################################
# TESTS DE DEPLACEMENTS, VITESSES ET ACCELERATIONS
#####################################################

TEST_RESU(RESU=(_F(GROUP_NO='B1',
                   INST=8.0E-05,
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=2.911177533,
                   VALE_REFE=3.87,
                   CRITERE='RELATIF',
                   PRECISION=0.286,),
                _F(GROUP_NO='A2',
                   INST=8.0E-05,
                   REFERENCE='SOURCE_EXTERNE',
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC=-12.691675675,
                   VALE_REFE=-13.46,
                   CRITERE='RELATIF',
                   PRECISION=0.068, ),))

TEST_RESU(RESU=(_F(GROUP_NO='B1',
                   INST=8.0E-05,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC=2.9111775334429,
                   CRITERE='RELATIF',
                   ),
                _F(GROUP_NO='A2',
                   INST=8.0E-05,
                   RESULTAT=RESU,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DY',
                   VALE_CALC=-12.691675675044999,
                   CRITERE='RELATIF',
                   ),
                _F(GROUP_NO='B1',
                   INST=8.0E-05,
                   RESULTAT=RESU,
                   NOM_CHAM='VITE',
                   NOM_CMP='DX',
                   VALE_CALC=-849.73270947623996,
                   CRITERE='RELATIF',
                   ),
                _F(GROUP_NO='A2',
                   INST=8.0E-05,
                   RESULTAT=RESU,
                   NOM_CHAM='VITE',
                   NOM_CMP='DY',
                   VALE_CALC=1.5804983850896E4,
                   CRITERE='RELATIF',
                   ),
                ),
          )

#####################################################
# TESTS SUR LA VALEUR DU JEU ET DU FROTTEMENT
#####################################################

TABJEU = POST_RELEVE_T (ACTION = _F(GROUP_NO='B1',
                                 INTITULE = 'MESSAGE',
                                 RESULTAT = RESU,
                                 TOUT_CMP='OUI',
                                 NOM_CHAM   = 'VALE_CONT',
                                 OPERATION = 'EXTRACTION'))

IMPR_TABLE(TABLE=TABJEU)

# JEU
########
TEST_TABLE(CRITERE='ABSOLU',
           VALE_CALC=0.0,
           NOM_PARA='JEU',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=10,),
           )

TEST_TABLE(CRITERE='ABSOLU',
           VALE_CALC=0.0,
           NOM_PARA='JEU',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=20,),
           )

TEST_TABLE(CRITERE='ABSOLU',
           VALE_CALC=0.0,
           NOM_PARA='JEU',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=30,),
           )

TEST_TABLE(CRITERE='ABSOLU',
           VALE_CALC=0.0,
           NOM_PARA='JEU',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=40,),
           )

TEST_TABLE(CRITERE='ABSOLU',
           VALE_CALC=0.0,
           NOM_PARA='JEU',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=50,),
           )

TEST_TABLE(CRITERE='ABSOLU',
           VALE_CALC=0.0,
           NOM_PARA='JEU',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=60,),
           )

TEST_TABLE(CRITERE='ABSOLU',
           VALE_CALC=0.0,
           NOM_PARA='JEU',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=70,),
           )

TEST_TABLE(CRITERE='ABSOLU',
           VALE_CALC=0.0,
           NOM_PARA='JEU',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=80,),
           )

# R
#######
TEST_TABLE(CRITERE='RELATIF',
           VALE_CALC=95.900213845232997,
           NOM_PARA='R',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=10,),
           )

TEST_TABLE(CRITERE='RELATIF',
           VALE_CALC=146.12608403837999,
           NOM_PARA='R',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=20,),
           )

TEST_TABLE(CRITERE='RELATIF',
           VALE_CALC=241.79870957283001,
           NOM_PARA='R',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=30,),
           )

TEST_TABLE(CRITERE='RELATIF',
           VALE_CALC=333.20191974471999,
           NOM_PARA='R',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=40,),
           )

TEST_TABLE(CRITERE='RELATIF',
           VALE_CALC=156.09427387822001,
           NOM_PARA='R',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=50,),
           )

TEST_TABLE(CRITERE='RELATIF',
           VALE_CALC=151.64633838026,
           NOM_PARA='R',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=60,),
           )

TEST_TABLE(CRITERE='RELATIF',
           VALE_CALC=150.68007803105999,
           NOM_PARA='R',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=70,),
           )

TEST_TABLE(CRITERE='RELATIF',
           VALE_CALC=150.46524301740001,
           NOM_PARA='R',
           TABLE=TABJEU,
           FILTRE=_F(NOM_PARA='NUME_ORDRE',
                     VALE_I=80,),
           )

FIN()
