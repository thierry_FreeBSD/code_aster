# TITRE TRACTION HOMOGENE LOI ELASTIQUE FRAGILE
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
#
#
# ======================================================================



DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='OUI'))

# AFFECTATION DE LA GEOMETRIE


MA=LIRE_MAILLAGE( )

# DEFINITION DU COMPORTEMENT DES MATERIAUX


ACIER=DEFI_MATERIAU(


                ELAS=_F(
                E = 200000.,
                NU = 0.),

                ECRO_LINE=_F(
                SY = 2000.,
                D_SIGM_EPSI = -50000.),

                NON_LOCAL=_F(
                LONG_CARA = 10.)
              )

MAT=AFFE_MATERIAU(

              MAILLAGE=MA,

                AFFE=_F(
                TOUT = 'OUI',
                MATER = ACIER)
              )

# CHOIX DU TYPE D''ELEMENTS FINIS


MO_MECA=AFFE_MODELE(

               MAILLAGE=MA,

                AFFE=_F(
                TOUT = 'OUI',
                PHENOMENE = 'MECANIQUE',
                MODELISATION = 'D_PLAN_GRAD_EPSI')
              )


# DEFINITION DU CHARGEMENT


RAMPE=DEFI_FONCTION(

                  NOM_PARA='INST',
                      VALE=(0., 0., 5., 5.,),
                PROL_DROITE='EXCLU',
               PROL_GAUCHE='EXCLU',
                     VERIF='CROISSANT'
              )

LIAISON=AFFE_CHAR_MECA(

                      MODELE=MO_MECA,

                DDL_IMPO=_F(
                GROUP_NO = 'BAS',
                DX = 0.,
                DY = 0.)
              )

TRACTION=AFFE_CHAR_MECA(

                      MODELE=MO_MECA,

                DDL_IMPO=_F(
                GROUP_NO = 'HAUT',
                DY = 0.01)
              )

# DISCRETISATION ET CALCUL


INSTANTS=DEFI_LIST_REEL(

                   DEBUT=0.,

                INTERVALLE=_F(
                JUSQU_A = 5.,
                NOMBRE = 5)
              )

EVOL=STAT_NON_LINE(
                        CHAM_MATER=MAT,
                         INCREMENT=_F(
                LIST_INST = INSTANTS,
                NUME_INST_FIN = 2),
                            MODELE=MO_MECA,EXCIT=(
                             _F(
                CHARGE = LIAISON), _F(
                CHARGE = TRACTION,
                FONC_MULT = RAMPE)),
                         COMPORTEMENT=_F(
                RELATION = 'ENDO_FRAGILE'),
                            NEWTON=_F(
                MATRICE = 'TANGENTE',
                PREDICTION = 'TANGENTE',
                REAC_INCR = 1),
                       CONVERGENCE=_F(
                RESI_GLOB_RELA = 1.E-4,
                ITER_GLOB_MAXI = 15,
                ARRET = 'OUI'),
              )

EVOL=STAT_NON_LINE( reuse=EVOL,
                        CHAM_MATER=MAT,
                         INCREMENT=_F(
                LIST_INST = INSTANTS,
                NUME_INST_FIN = 3),
                         ETAT_INIT=_F(
                EVOL_NOLI = EVOL,
                NUME_ORDRE = 2),
                            MODELE=MO_MECA,EXCIT=(
                             _F(
                CHARGE = LIAISON), _F(
                CHARGE = TRACTION,
                FONC_MULT = RAMPE)),
                         COMPORTEMENT=_F(
                RELATION = 'ENDO_FRAGILE'),
                            NEWTON=_F(
                MATRICE = 'TANGENTE',
                PREDICTION = 'TANGENTE',
                REAC_ITER = 1),
                       CONVERGENCE=_F(
                RESI_GLOB_RELA = 1.E-4,
                ITER_GLOB_MAXI = 15,
                ARRET = 'OUI'),)

TEST_RESU(RESU=(_F(INST=3.0,
                   REFERENCE='ANALYTIQUE',
                   POINT=1,
                   RESULTAT=EVOL,
                   NOM_CHAM='VARI_ELGA',
                   NOM_CMP='V1',
                   VALE_CALC=0.833333333,
                   VALE_REFE=0.83333299999999999,
                   PRECISION=1.E-3,
                   MAILLE='M1',),
                _F(INST=3.0,
                   REFERENCE='ANALYTIQUE',
                   POINT=1,
                   RESULTAT=EVOL,
                   NOM_CHAM='SIEF_ELGA',
                   NOM_CMP='SIYY',
                   VALE_CALC= 1.00000000E+03,
                   VALE_REFE=1000.0,
                   PRECISION=1.E-3,
                   MAILLE='M1',),
                ),
          )

FIN()
#
#
