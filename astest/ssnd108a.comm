# person_in_charge: jean-michel.proix at edf.fr
# TITRE TRACTIONS-ROTATIONS MULTIPLES GDEF_HYPOELAS VISCOPLASTIQUE
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================

DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET',VISU_EFICAS='NON'),DEBUG=_F(SDVERI='NON'))
# SDVERI='NON' car la verification est trop couteuse en CPU

compor=[]
compor.append(['LEMAITRE','GDEF_HYPO_ELAS']) # reference
compor.append(['LEMAITRE','GDEF_HYPO_ELAS'])
compor.append(['VISC_ENDO_LEMA','GDEF_HYPO_ELAS'])
compor.append(['VISC_CIN2_CHAB','GDEF_HYPO_ELAS'])
compor.append(['VISC_CIN1_CHAB','GDEF_HYPO_ELAS'])
compor.append(['VISC_CIN2_MEMO','GDEF_HYPO_ELAS'])
compor.append(['LEMAITRE','GDEF_LOG'])
angle_degres=[45.0]*len(compor)
angle_degres[0]=0.

E=2.E5
SY=1.E-6
Q_0 = 0.0;
Q_M = 0.0;
MU = 1.0;
ETA = 0.;
R_D=1.
N=20.

acier=DEFI_MATERIAU(ELAS=_F(E=2e+05,NU=0.3,),
                    LEMAITRE =_F( N = N,UN_SUR_K = 1.E-3, UN_SUR_M = 0., ),
                    CIN1_CHAB=_F(R_0=SY,
                                 R_I=SY,
                                 C_I=0.,
                                 G_0=0.),
                    CIN2_CHAB=_F(R_0=SY,
                                 R_I=SY,
                                 C1_I=0.,
                                 C2_I=0.,
                                 G1_0=0.,
                                 G2_0=0.,),
                    MEMO_ECRO=_F(
                                  MU =MU,
                                  Q_M=Q_M,
                                  Q_0=Q_0,
                                  ETA=ETA,
                                 ),
                    VISC_ENDO=_F(SY=0.,
                                 R_D=R_D,
                                 A_D=1.E7),
                    );

#lire le maillage
MA=LIRE_MAILLAGE();

mat=AFFE_MATERIAU(MAILLAGE=MA,
                  AFFE=_F(TOUT='OUI',MATER=acier,),);

#affecter les elements
modele=AFFE_MODELE(MAILLAGE=MA,
                   AFFE=_F(TOUT='OUI',
                           PHENOMENE='MECANIQUE',
                           MODELISATION='3D',),);


#-----------------------------donness Traction-Rotation ------------------------------
dureetrac=0.5
dureetracrot=1.
tfintrac=dureetrac
npastrac=10
npasrota=5
nbincr=4
reaciter=1
iterintemaxi=100
Rota2=[None]*len(compor)*nbincr
Trac2=[None]*len(compor)*nbincr
Ftra2=[None]*len(compor)*nbincr
DZMAX2=200

#conditions limites et charge
DZMAX=500.0
clim_tra=AFFE_CHAR_MECA(MODELE=modele,
                        FACE_IMPO=(_F(GROUP_MA='FINF',
                                     DNOR=0.0,),
                                  _F(GROUP_MA='FSUP',
                                     DNOR=DZMAX),
                                  _F(GROUP_MA='FY0',
                                     DNOR=0.0,),
                                  _F(GROUP_MA='FY1', # DEFORMATION PLANE
                                     DNOR=0.0,),),
                        DDL_IMPO=(_F(GROUP_NO='NAXE',
                                     DX=0.0,
                                     ),),);
F_trac=DEFI_FONCTION(NOM_PARA='INST',VALE=(0.0,0.0,tfintrac,1.0,),PROL_DROITE='LINEAIRE',);
# pour la rotation
ClFixe=AFFE_CHAR_MECA(MODELE=modele,DDL_IMPO=(
                        _F(GROUP_NO='NAXE',DX=0.0,DZ=0.0),
                        _F(GROUP_MA='FY0', DY=0.0,),
                        _F(GROUP_MA='FY1', DY=0.0,),),);

INCLUDE(UNITE=30)

#listes d'instants initiale
L_inst=DEFI_LIST_REEL(DEBUT=0.0,INTERVALLE=_F(JUSQU_A=dureetrac,NOMBRE=npastrac),)
DEFLIST1 = DEFI_LIST_INST(DEFI_LIST=_F(METHODE   ='MANUEL',LIST_INST = L_inst,))

#### boucle sur les comportements
resu=[None]*len(compor)
vmis=[None]*len(compor)
epsg=[None]*len(compor)
icl=-1
for icomp,comp in enumerate(compor) :

    resu[icomp]=STAT_NON_LINE(MODELE=modele,CHAM_MATER=mat,
                   EXCIT=_F(CHARGE=clim_tra, FONC_MULT=F_trac,),
                   COMPORTEMENT=_F(RELATION=comp[0],DEFORMATION=comp[1],ITER_INTE_MAXI=iterintemaxi),
                   INCREMENT=_F(LIST_INST=DEFLIST1),
                   NEWTON=_F(MATRICE='TANGENTE',REAC_ITER=1,#PREDICTION='ELASTIQUE',
                   ),
                             );

    # increments de rotation puis traction
    for incr in range(nbincr):

        tini=dureetrac+ dureetracrot*incr
        tfinrot=dureetracrot+dureetracrot*incr
        tfintrac=dureetrac+dureetracrot+dureetracrot*incr
        # appel a la macro dans INCLUDE
        icl+=1
        Rota2[icl]=CHAR_ROTA(ANGLE_DEGRES=angle_degres[icomp],TINI=tini,TFIN=tfinrot,RESU=resu[icomp],MAIL=MA)

        Linst2=DEFI_LIST_REEL(DEBUT=tini,INTERVALLE=_F(JUSQU_A=tfinrot,NOMBRE=npasrota),);
        DEFLIST2 = DEFI_LIST_INST(DEFI_LIST=_F(METHODE   ='MANUEL',LIST_INST = Linst2,))

        resu[icomp]=STAT_NON_LINE(reuse =resu[icomp],
                           MODELE=modele,CHAM_MATER=mat,
                           EXCIT=(_F(CHARGE=Rota2[icl],TYPE_CHARGE='DIDI',),
                                  _F(CHARGE=ClFixe),),
                           COMPORTEMENT=_F(RELATION=comp[0],DEFORMATION=comp[1],
                                        ITER_INTE_MAXI=iterintemaxi),
                           ETAT_INIT=_F(EVOL_NOLI=resu[icomp],),
                           INCREMENT=_F(LIST_INST=DEFLIST2,),
                           NEWTON=_F(MATRICE='TANGENTE',REAC_ITER=reaciter,#PREDICTION='ELASTIQUE',
                           ),
                           );
        DETRUIRE(CONCEPT=_F(NOM=(Linst2,DEFLIST2)),INFO=1)
        # nouvelle traction
        Trac2[icl]=AFFE_CHAR_MECA(MODELE=modele,LIAISON_OBLIQUE= (
                                _F(GROUP_NO='FSUP',ANGL_NAUT=(0.,-angle_degres[icomp]*(incr+1),0.),DZ=DZMAX2),
                                _F(GROUP_NO='NX1Z0',ANGL_NAUT=(0.,-angle_degres[icomp]*(incr+1),0.),DZ=0.0,),
                                _F(GROUP_NO='NX0Z1',ANGL_NAUT=(0.,-angle_degres[icomp]*(incr+1),0.),DX=0.0,),),
                                DDL_IMPO=(_F(GROUP_MA='FY0', DY=0.0,),
                                          _F(GROUP_MA='FY1', DY=0.0,),
                                          _F(GROUP_NO='NAXE', DX=0.0,DZ=0.0),
                                          ),)
        Ftra2[icl]=DEFI_FONCTION(NOM_PARA='INST',VALE=(tfinrot,0.,tfintrac,1.,),PROL_DROITE='LINEAIRE',);
        Linst2=DEFI_LIST_REEL(DEBUT=tfinrot,INTERVALLE=_F(JUSQU_A=tfintrac,NOMBRE=npastrac),)
        DEFLIST2 = DEFI_LIST_INST(DEFI_LIST=_F(METHODE   ='MANUEL',LIST_INST = Linst2,))

        resu[icomp]=STAT_NON_LINE(reuse = resu[icomp],MODELE=modele,CHAM_MATER=mat,
                           EXCIT=(_F(CHARGE=Trac2[icl],TYPE_CHARGE='DIDI',FONC_MULT=Ftra2[icl]),),
                           COMPORTEMENT=_F(RELATION=comp[0], DEFORMATION=comp[1],
                                        ITER_INTE_MAXI=iterintemaxi),
                           ETAT_INIT=_F(EVOL_NOLI=resu[icomp]),
                           INCREMENT=_F(LIST_INST=DEFLIST2,INST_FIN=tfintrac),
                           NEWTON=_F(MATRICE='TANGENTE',REAC_ITER=reaciter,#PREDICTION='ELASTIQUE',
                           ),
                                           );
        DETRUIRE(CONCEPT=_F(NOM=(Linst2,DEFLIST2)),INFO=1)


    resu[icomp]=CALC_CHAMP(reuse=resu[icomp],RESULTAT=resu[icomp],CRITERES=('SIEQ_ELGA'),DEFORMATION=('EPSG_ELGA'))


    vmis[icomp]=RECU_FONCTION(RESULTAT=resu[icomp],NOM_CHAM='SIEQ_ELGA',
                              NOM_CMP='VMIS',MAILLE='M2',POINT=1,);
    epsg[icomp]=RECU_FONCTION(RESULTAT=resu[icomp],NOM_CHAM='EPSG_ELGA',
                              NOM_CMP='EPZZ',MAILLE='M2',POINT=1,);
    TEST_FONCTION(VALEUR=(_F(VALE_CALC=681.487251355,
                             VALE_REFE=681.5,
                             VALE_PARA=1.0,
                             REFERENCE='AUTRE_ASTER',
                             TOLE_MACHINE=2.E-3,          # TODO TOLE_MACHINE
                             PRECISION=2.E-3,
                             NOM_PARA='INST',
                             FONCTION=vmis[icomp],),
                          _F(VALE_CALC=680.414531579,
                             VALE_REFE=680.39999999999998,
                             VALE_PARA=2.0,
                             REFERENCE='AUTRE_ASTER',
                             TOLE_MACHINE=1.E-3,          # TODO TOLE_MACHINE
                             PRECISION=1.E-3,
                             NOM_PARA='INST',
                             FONCTION=vmis[icomp],),
                          _F(VALE_CALC=680.282350264,
                             VALE_REFE=680.29999999999995,
                             VALE_PARA=3.0,
                             REFERENCE='AUTRE_ASTER',
                             TOLE_MACHINE=1.E-3,          # TODO TOLE_MACHINE
                             PRECISION=1.E-3,
                             NOM_PARA='INST',
                             FONCTION=vmis[icomp],),
                          _F(VALE_CALC=680.160000641,
                             VALE_REFE=680.20000000000005,
                             VALE_PARA=4.0,
                             REFERENCE='AUTRE_ASTER',
                             TOLE_MACHINE=1.E-3,          # TODO TOLE_MACHINE
                             PRECISION=1.E-3,
                             NOM_PARA='INST',
                             FONCTION=vmis[icomp],),
                          ),
                  )




DEFI_FICHIER( UNITE=38, FICHIER='./REPE_OUT/courbes_vmis_visco.agr')
IMPR_FONCTION(FORMAT='XMGRACE',UNITE=38,
                  COURBE=(
                   _F(FONCTION=vmis[0],LEGENDE=compor[0][0]+'_'+compor[0][1]),
                   _F(FONCTION=vmis[1],LEGENDE=compor[1][0]+'_'+compor[1][1]),
                   _F(FONCTION=vmis[2],LEGENDE=compor[2][0]+'_'+compor[2][1]),
                   _F(FONCTION=vmis[3],LEGENDE=compor[3][0]+'_'+compor[3][1]),
                   _F(FONCTION=vmis[4],LEGENDE=compor[4][0]+'_'+compor[4][1]),
                   _F(FONCTION=vmis[5],LEGENDE=compor[5][0]+'_'+compor[5][1]),
                   _F(FONCTION=vmis[6],LEGENDE=compor[6][0]+'_'+compor[6][1]),
                   ),
                  TITRE='courbes_vmis_viscoplastique');
DEFI_FICHIER(ACTION='LIBERER',UNITE=38)


DEFI_FICHIER( UNITE=38, FICHIER='./REPE_OUT/courbes_epsg_visco.agr')
IMPR_FONCTION(FORMAT='XMGRACE',UNITE=38,
                  COURBE=(
                   _F(FONCTION=epsg[0],LEGENDE=compor[0][0]+'_'+compor[0][1]),
                   _F(FONCTION=epsg[1],LEGENDE=compor[1][0]+'_'+compor[1][1]),
                   _F(FONCTION=epsg[2],LEGENDE=compor[2][0]+'_'+compor[2][1]),
                   _F(FONCTION=epsg[3],LEGENDE=compor[3][0]+'_'+compor[3][1]),
                   _F(FONCTION=epsg[4],LEGENDE=compor[4][0]+'_'+compor[4][1]),
                   _F(FONCTION=epsg[5],LEGENDE=compor[5][0]+'_'+compor[5][1]),
                   _F(FONCTION=epsg[6],LEGENDE=compor[6][0]+'_'+compor[6][1]),
                   ),
                  TITRE='courbes_epsg_viscoplastique');
DEFI_FICHIER(ACTION='LIBERER',UNITE=38)


FIN()
