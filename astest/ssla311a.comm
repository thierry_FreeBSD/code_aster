# TITRE MURAKAMI 9.12 FISSURE CIRCULAIRE SOUMISE A UNE CHARGE ANNULAIRE
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR   
# (AT YOUR OPTION) ANY LATER VERSION.                                 
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT 
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF          
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU    
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                            
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE   
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,       
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.      
# ======================================================================
#
# C/A = 0.9

DEBUT( CODE=_F(NIV_PUB_WEB='INTERNET'))

MA=LIRE_MAILLAGE(VERI_MAIL=_F(VERIF='OUI'),FORMAT='MED',)

MA=DEFI_GROUP( reuse=MA,   MAILLAGE=MA, 
                     CREA_GROUP_NO=_F( 
                         GROUP_MA = ( 'LPOP8',  'LPOP11',  'LP8P9', ))
                 )

MO=AFFE_MODELE(  MAILLAGE=MA,
                      AFFE=_F(  TOUT = 'OUI',
                             PHENOMENE = 'MECANIQUE',
                             MODELISATION = 'AXIS') )

MAT=DEFI_MATERIAU(ELAS=_F(  E = 2.E11,
                             NU = 0.3,
                             ALPHA = 0.) )

CHMAT=AFFE_MATERIAU(  MAILLAGE=MA,
                       AFFE=_F(  TOUT = 'OUI',
                              MATER = MAT) )

CH=AFFE_CHAR_MECA(  MODELE=MO,DDL_IMPO=(
                            _F(  GROUP_NO = 'LP8P9', DX = 0.),
                                     _F(  GROUP_NO = 'LPOP11', DY = 0.)),
                             FORCE_NODALE=_F(  GROUP_NO = 'PB',
                                            FY = 159.15)   )

CHAMDEPL=MECA_STATIQUE(        MODELE=MO,
                              CHAM_MATER=CHMAT,
                              EXCIT=_F( CHARGE = CH)
                           )


THETA0=CALC_THETA(    MODELE=MO,
                        THETA_2D=_F(  GROUP_NO = ('P0',),
                                   MODULE = 1.,
                                   R_INF = 0.0025,
                                   R_SUP = 0.0075),
                         DIRECTION=(1., 0., 0.,)

                     )

G0=CALC_G(              RESULTAT=CHAMDEPL,
                        THETA=_F(THETA=THETA0, SYME='OUI'),
                    )

#
# LA VALEUR DE REFERENCE VAUT 7.72E-4, LE RAYON LA FISSURE VAUT 0.1 :
# ET DONC LA VALEUR A TESTER VAUT 7.72E-4*0.1=7.72E-5
#

TEST_TABLE(PRECISION=0.02,
           VALE_CALC=7.80932358732E-05,
           VALE_REFE=7.7200000000000006E-05,
           REFERENCE='NON_DEFINI',
           NOM_PARA='G',
           TABLE=G0,)

FIN()
#
