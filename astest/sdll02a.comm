# TITRE POUTRE ELANCEE, ENCASTREE-LIBRE, REPLIEE SUR ELLE-MEME
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# CAS_TEST__: SDLL02A
# SDLL02/A                 COMMANDES                             16/1/95
# POUTRE ELANCEE ENCASTREE-LIBRE REPLIEE SUR ELLE-MEME
# SECTION RECT: 5*50MM --- L=2*0.5M --- 2D
#                                                  REF: SFM.VPCS SDLL02
# MODELISATION POU_D_E: 21 NOEUDS  --   20 MAILLES SEG2
# POINTS  A    B    C
# MODELISATION :
#    CONDITIONS AUX LIMITES IMPOSEES PAR AFFE_CHAR_MECA
#    RECHERCHE DES ELEMENTS PROPRES : METHODE TRI_DIAG
#=======================================================================

DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='OUI'))

MA=LIRE_MAILLAGE(  )

MA=DEFI_GROUP( reuse=MA,   MAILLAGE=MA,
  CREA_GROUP_MA=_F(  NOM = 'TOUT', TOUT = 'OUI'))

MO=AFFE_MODELE(  MAILLAGE=MA,
                  AFFE=_F(  TOUT = 'OUI',    PHENOMENE = 'MECANIQUE',
                         MODELISATION = 'POU_D_E')  )

CARELEM=AFFE_CARA_ELEM(  MODELE=MO,
             POUTRE=_F(
       GROUP_MA = 'TOUT',
     SECTION = 'RECTANGLE',
                      CARA = (  'HY',   'HZ', ),
                      VALE = ( 0.005,  0.050, ))  )

MAT=DEFI_MATERIAU( ELAS=_F(  E = 2.1E+11,    NU = 0.3,    RHO = 7800.) )

CHMAT=AFFE_MATERIAU(  MAILLAGE=MA,
          AFFE=_F(  TOUT = 'OUI',     MATER = MAT) )

#   PB PLAN --- POINT A ENCASTRE
CH1=AFFE_CHAR_MECA(  MODELE=MO,DDL_IMPO=(
       _F(  TOUT = 'OUI', DZ = 0., DRX = 0., DRY = 0.),
                _F(  NOEUD = 'A',   DX = 0., DY = 0.,  DRZ = 0.)) )

# ------------------------------------------------------------------
# MODELISATION

MELR1=CALC_MATR_ELEM(  MODELE=MO,           CHARGE=CH1,
                        CARA_ELEM=CARELEM,
                        CHAM_MATER=CHMAT,   OPTION='RIGI_MECA' )

MELM1=CALC_MATR_ELEM(  MODELE=MO,           CHARGE=CH1,
                        CARA_ELEM=CARELEM,
                        CHAM_MATER=CHMAT,   OPTION='MASS_MECA' )

NUM1=NUME_DDL(  MATR_RIGI=MELR1 )

MATASSR1=ASSE_MATRICE(  MATR_ELEM=MELR1,   NUME_DDL=NUM1 )

MATASSM1=ASSE_MATRICE(  MATR_ELEM=MELM1,   NUME_DDL=NUM1 )

#--------------------------------------------------------------------
#--------------------------------------------------------------------


FREQ1=MODE_ITER_SIMULT(
              METHODE='TRI_DIAG',
               MATR_RIGI=MATASSR1,        MATR_MASS=MATASSM1,
               CALC_FREQ=_F(  OPTION = 'PLUS_PETITE',
                           NMAX_FREQ = 8),
                TITRE='METHODE DE LANCZOS' )


FREQ1=NORM_MODE(reuse=FREQ1,   MODE=FREQ1,   NORME='TRAN' )

# ---------------------------------------------------------------------
MODE11=CREA_CHAMP(  OPERATION='EXTR', TYPE_CHAM='NOEU_DEPL_R',
NOM_CHAM='DEPL', RESULTAT=FREQ1,
                    # FREQ: 11.7642
                    # NUMERO_MODE: 1
                      NUME_ORDRE=1
                           )

MODE21=CREA_CHAMP(  OPERATION='EXTR', TYPE_CHAM='NOEU_DEPL_R',
NOM_CHAM='DEPL', RESULTAT=FREQ1,
                    # FREQ: 11.7642
                    # NUMERO_MODE: 2
                      NUME_ORDRE=2
                           )

MODE31=CREA_CHAMP(  OPERATION='EXTR', TYPE_CHAM='NOEU_DEPL_R',
NOM_CHAM='DEPL', RESULTAT=FREQ1,
                    # FREQ: 105.8811
                    # NUMERO_MODE: 3
                      NUME_ORDRE=3
                           )

MODE41=CREA_CHAMP(  OPERATION='EXTR', TYPE_CHAM='NOEU_DEPL_R',
NOM_CHAM='DEPL', RESULTAT=FREQ1,
                      NUME_ORDRE=4
                           )

MODE71=CREA_CHAMP(  OPERATION='EXTR', TYPE_CHAM='NOEU_DEPL_R',
NOM_CHAM='DEPL', RESULTAT=FREQ1,
                      NUME_ORDRE=7
                           )

MODE81=CREA_CHAMP(  OPERATION='EXTR', TYPE_CHAM='NOEU_DEPL_R',
NOM_CHAM='DEPL', RESULTAT=FREQ1,
                      NUME_ORDRE=8
                           )

# ---------------------------------------------------------------------


TEST_RESU(RESU=(_F(NUME_ORDRE=1,
                   PARA='FREQ',
                   RESULTAT=FREQ1,
                   VALE_CALC=11.764183404,
                   VALE_REFE=11.76,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=2,
                   PARA='FREQ',
                   RESULTAT=FREQ1,
                   VALE_CALC=11.764183438,
                   VALE_REFE=11.76,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=3,
                   PARA='FREQ',
                   RESULTAT=FREQ1,
                   VALE_CALC=105.881130032,
                   VALE_REFE=105.88,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=4,
                   PARA='FREQ',
                   RESULTAT=FREQ1,
                   VALE_CALC=105.881203672,
                   VALE_REFE=105.88,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=5,
                   PARA='FREQ',
                   RESULTAT=FREQ1,
                   VALE_CALC=294.177995325,
                   VALE_REFE=294.1,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=6,
                   PARA='FREQ',
                   RESULTAT=FREQ1,
                   VALE_CALC=294.180625511,
                   VALE_REFE=294.1,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=7,
                   PARA='FREQ',
                   RESULTAT=FREQ1,
                   VALE_CALC=576.980226710,
                   VALE_REFE=576.44,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                _F(NUME_ORDRE=8,
                   PARA='FREQ',
                   RESULTAT=FREQ1,
                   VALE_CALC=577.007933721,
                   VALE_REFE=576.44,
                   REFERENCE='ANALYTIQUE',
                   CRITERE='RELATIF',
                   PRECISION=1.E-2,),
                ),)

TEST_RESU(CHAM_NO=(_F(NOEUD='B',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE11,
                      VALE_CALC=-0.707174643,
                      VALE_REFE=-0.707,
                      TOLE_MACHINE=1.E-3,       # ajustement pour clap0f0q
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='C',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE11,
                      VALE_CALC=1.0,
                      VALE_REFE=1.0,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='B',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.04,
                      CHAM_GD=MODE21,
                      VALE_CALC=0.706960347,
                      VALE_REFE=0.707,
                      TOLE_MACHINE=1.E-3,       # ajustement pour clap0f0q
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='C',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE21,
                      VALE_CALC=1.0,
                      VALE_REFE=1.0,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='B',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE31,
                      VALE_CALC=0.707106773,
                      VALE_REFE=0.707,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='C',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE31,
                      VALE_CALC=1.0,
                      VALE_REFE=1.0,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='B',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE41,
                      VALE_CALC=-0.370147396,
                      VALE_REFE=-0.37,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='C',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE41,
                      VALE_CALC=0.523467428,
                      VALE_REFE=0.523,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='B',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE71,
                      VALE_CALC=0.707106784,
                      VALE_REFE=0.707,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='C',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE71,
                      VALE_CALC=1.0,
                      VALE_REFE=1.0,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='B',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE81,
                      VALE_CALC=-0.388465784,
                      VALE_REFE=-0.388,
                      REFERENCE='ANALYTIQUE',),
                   _F(NOEUD='C',
                      CRITERE='RELATIF',
                      NOM_CMP='DY',
                      PRECISION=0.02,
                      CHAM_GD=MODE81,
                      VALE_CALC=0.549373593,
                      VALE_REFE=0.549,
                      REFERENCE='ANALYTIQUE',),
                   ),
          )

FIN()
