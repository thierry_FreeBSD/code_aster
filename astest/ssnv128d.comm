# TITRE CONTACT AVEC FROTTEMENT D'UNE PLAQUE SUR UN PLAN RIGIDE
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# ======================================================================
# CAS_TEST__:SSNV128D
# TEST DU GRECO CALCUL 2D
# MODELE
# MAILLAGE : ELEMENT 2D TRIANGLE A 6 NOEUDS
# UNITES   : NEWTON , METRE , SECONDE (N,M,S)
#

DEBUT( CODE=_F(NIV_PUB_WEB='INTERNET'),
IGNORE_ALARM=('CALCULEL2_63'),DEBUG=_F(SDVERI='OUI'))

# <CALCULEL2_63> : DANS UN MODELE, IL EXISTE DES ELEMENTS DE TYPE "BORD" QUI N'ONT PAS DE VOISIN AVEC RIGIDITE
#  LE MODELE PROVOQUANT CES ALARMES CONTIENT UNE SURFACE RIGIDE POUR LE CONTACT


#......................................................................
#  CALCUL TEST ELEMENTS FINIS DE CONTACT 2D TRIA6
#  PLAQUE AVEC CONTACT ET FROTTEMENT SUR UN PLAN RIGIDE
#......................................................................
#
MA=LIRE_MAILLAGE(VERI_MAIL=_F(VERIF='OUI'),FORMAT='MED',)
#
MO=AFFE_MODELE(MAILLAGE=MA,
               AFFE=_F(TOUT = 'OUI',
                       PHENOMENE = 'MECANIQUE',
                       MODELISATION = 'D_PLAN',)
              )
#
MA=MODI_MAILLAGE(reuse=MA,
                 MAILLAGE=MA,
                 ORIE_PEAU_2D=(_F(GROUP_MA = 'LPRESV',),
                               _F(GROUP_MA = 'LPRESH',))
                )
#
MATPLA=DEFI_MATERIAU(ELAS=_F(E = 1.3E11,
                             NU = 0.2,)
                    )
#
CHMAT=AFFE_MATERIAU(MAILLAGE=MA,
                    AFFE=_F(TOUT = 'OUI',
                             MATER = MATPLA,)
                    )
#
CHA1=AFFE_CHAR_MECA(MODELE=MO,
                    DDL_IMPO=(_F(GROUP_MA = 'LBATI',  DX = 0.0, DY = 0.0,),
                              _F(GROUP_NO = 'PPS',    DX = 0.0, DY = 0.0,),
                              _F(GROUP_MA = 'LBLOCX', DX = 0.0,)),
                    PRES_REP=(_F(GROUP_MA = 'LPRESV', PRES = 5.E07,),
                              _F(GROUP_MA = 'LPRESH', PRES = 15.E07,))
                   )
#
CHA2=DEFI_CONTACT(MODELE         = MO,
                  FORMULATION    = 'DISCRETE',
                  FROTTEMENT     = 'COULOMB',
                  REAC_GEOM      = 'CONTROLE',
                  NB_ITER_GEOM   = 2,
                  ZONE=_F(
                          GROUP_MA_MAIT  = 'LBATI',
                          GROUP_MA_ESCL  = 'LCONTA',
                          SANS_GROUP_NO  = 'PPS',
                          NORMALE        = 'MAIT',
                          ALGO_CONT      = 'LAGRANGIEN',
                          ALGO_FROT      = 'LAGRANGIEN',
                          COULOMB        = 1.0,
                        )
                  )
#
CHA3=DEFI_CONTACT(MODELE         = MO,
                  FORMULATION    = 'DISCRETE',
                  FROTTEMENT     = 'COULOMB',
                  REAC_GEOM      = 'AUTOMATIQUE',
                  ZONE=_F(
                          GROUP_MA_MAIT  = 'LBATI',
                          GROUP_MA_ESCL  = 'LCONTA',
                          APPARIEMENT    = 'NODAL',
                          NORMALE        = 'ESCL',
                          ALGO_CONT      = 'LAGRANGIEN',
                          ALGO_FROT      = 'PENALISATION',
                          E_T            = 1.0E11,
                          COULOMB        = 1.0,
                          COEF_MATR_FROT = 0.4,
                        )
                  )

#
#
RAMPE=DEFI_FONCTION(NOM_PARA='INST',
                    PROL_GAUCHE='LINEAIRE',
                    PROL_DROITE='LINEAIRE',
                    VALE=(0.0,0.0,1.0,1.0,)
                    )
#
L_INST=DEFI_LIST_REEL(DEBUT=0.,
                      INTERVALLE=(_F(JUSQU_A = 1.0, NOMBRE = 5,))
                      )
#
L_INST2=DEFI_LIST_REEL(DEBUT=0.,
                      INTERVALLE=(_F(JUSQU_A = 1.0, NOMBRE = 1,))
                      )
#
#-----------------------------------------------------------
#
U2M=STAT_NON_LINE(SOLVEUR=_F(SYME='OUI',),
                  MODELE    = MO,
                  CHAM_MATER = CHMAT,
                  EXCIT      =(_F(CHARGE = CHA1,
                                  FONC_MULT = RAMPE,),
                               ),
                  CONTACT    = CHA2,
                  COMPORTEMENT  = _F(RELATION = 'ELAS',),
                  NEWTON     = _F(REAC_ITER = 0,),
                  INCREMENT  = _F(LIST_INST = L_INST2,),
                  CONVERGENCE= _F(ARRET = 'OUI',
                                 ITER_GLOB_MAXI = 200,
                                 RESI_GLOB_RELA = 1.0E-5,),
#                  INFO=2,
                 )
#
U2M=CALC_CHAMP(reuse=U2M,CONTRAINTE=('SIGM_ELNO'),VARI_INTERNE=('VARI_ELNO'),RESULTAT=U2M)

#
DEPL2M=CREA_CHAMP(OPERATION='EXTR',
                  TYPE_CHAM='NOEU_DEPL_R',
                  NOM_CHAM='DEPL',
                  RESULTAT=U2M,
                  INST=1.0
                 )
#
TEST_RESU(RESU=(_F(GROUP_NO='PPA',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.85813954E-05,
                   VALE_REFE=2.8600000000000001E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPB',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.72134877E-05,
                   VALE_REFE=2.72E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPC',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.28636998E-05,
                   VALE_REFE=2.2799999999999999E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPD',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.98053658E-05,
                   VALE_REFE=1.98E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPE',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.54126958E-05,
                   VALE_REFE=1.5E-05,
                   PRECISION=0.050000000000000003,
                   ),
                ),
          )

#
#-----------------------------------------------------------
#
U2L=STAT_NON_LINE(MODELE    = MO,
                  CHAM_MATER = CHMAT,
                  EXCIT      =(_F(CHARGE = CHA1,
                                  FONC_MULT = RAMPE,),
                               ),
                  CONTACT    = CHA2,
                  COMPORTEMENT  = _F(RELATION = 'ELAS',),
                  SOLVEUR    = _F(SYME='OUI',
                                  METHODE = 'LDLT',),
                  NEWTON     = _F(REAC_ITER = 0,),
                  INCREMENT  = _F(LIST_INST = L_INST2,),
                  CONVERGENCE= _F(ARRET = 'OUI',
                                 ITER_GLOB_MAXI = 200,
                                 RESI_GLOB_RELA = 1.0E-5,),
#                  INFO=2,
                 )
#
U2L=CALC_CHAMP(reuse=U2L,CONTRAINTE=('SIGM_ELNO'),VARI_INTERNE=('VARI_ELNO'),RESULTAT=U2L)

#
DEPL2L=CREA_CHAMP(OPERATION='EXTR',
                  TYPE_CHAM='NOEU_DEPL_R',
                  NOM_CHAM='DEPL',
                  RESULTAT=U2L,
                  INST=1.0
                )
#
TEST_RESU(RESU=(_F(GROUP_NO='PPA',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.85813954E-05,
                   VALE_REFE=2.8600000000000001E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPB',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.72134877E-05,
                   VALE_REFE=2.72E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPC',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.28636998E-05,
                   VALE_REFE=2.2799999999999999E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPD',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.98053658E-05,
                   VALE_REFE=1.98E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPE',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U2L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.54126958E-05,
                   VALE_REFE=1.5E-05,
                   PRECISION=0.050000000000000003,
                   ),
                ),
          )

#
#-----------------------------------------------------------
#
U3M=STAT_NON_LINE(SOLVEUR=_F(SYME='OUI',),
                  MODELE      = MO,
                 CHAM_MATER  = CHMAT,
                 EXCIT       =(_F(CHARGE = CHA1,
                                  FONC_MULT = RAMPE,),
                               ),
                 CONTACT    = CHA3,
                 COMPORTEMENT   = _F(RELATION = 'ELAS',),
                 NEWTON      = _F(REAC_ITER = 1,),
                 INCREMENT   = _F(LIST_INST = L_INST,),
                 CONVERGENCE = _F(ARRET = 'OUI',
                                  ITER_GLOB_MAXI = 200,
                                  RESI_GLOB_RELA = 1.0E-5,),
#                 INFO=2,
               )
#
U3M=CALC_CHAMP(reuse=U3M,CONTRAINTE=('SIGM_ELNO'),VARI_INTERNE=('VARI_ELNO'),RESULTAT=U3M)

#
DEPLA3M=CREA_CHAMP(OPERATION='EXTR',
                 TYPE_CHAM='NOEU_DEPL_R',
                 NOM_CHAM='DEPL',
                 RESULTAT=U3M,
                 INST=1.0
                )
#
TEST_RESU(RESU=(_F(GROUP_NO='PPA',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.87220866E-05,
                   VALE_REFE=2.8600000000000001E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPB',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.73554763E-05,
                   VALE_REFE=2.72E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPC',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.29958762E-05,
                   VALE_REFE=2.2799999999999999E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPD',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.99432886E-05,
                   VALE_REFE=1.98E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPE',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3M,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.55686827E-05,
                   VALE_REFE=1.5E-05,
                   PRECISION=0.050000000000000003,
                   ),
                ),
          )

#
#-----------------------------------------------------------
#
U3L=STAT_NON_LINE(MODELE      = MO,
                 CHAM_MATER  = CHMAT,
                 EXCIT       =(_F(CHARGE = CHA1,
                                  FONC_MULT = RAMPE,),
                               ),
                 CONTACT    = CHA3,
                 COMPORTEMENT   = _F(RELATION = 'ELAS',),
                 SOLVEUR     = _F(SYME='OUI',
                                  METHODE = 'LDLT',),
                 NEWTON      = _F(REAC_ITER = 1,),
                 INCREMENT   = _F(LIST_INST = L_INST,),
                 CONVERGENCE = _F(ARRET = 'OUI',
                                  ITER_GLOB_MAXI = 200,
                                  RESI_GLOB_RELA = 1.0E-5,),
#                 INFO=2,
               )
#
U3L=CALC_CHAMP(reuse=U3L,CONTRAINTE=('SIGM_ELNO'),VARI_INTERNE=('VARI_ELNO'),RESULTAT=U3L)

#
DEPLA3L=CREA_CHAMP(OPERATION='EXTR',
                 TYPE_CHAM='NOEU_DEPL_R',
                 NOM_CHAM='DEPL',
                 RESULTAT=U3L,
                 INST=1.0
                )
#
TEST_RESU(RESU=(_F(GROUP_NO='PPA',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.87220866E-05,
                   VALE_REFE=2.8600000000000001E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPB',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.73554763E-05,
                   VALE_REFE=2.72E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPC',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 2.29958762E-05,
                   VALE_REFE=2.2799999999999999E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPD',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.99432886E-05,
                   VALE_REFE=1.98E-05,
                   PRECISION=0.050000000000000003,
                   ),
                _F(GROUP_NO='PPE',
                   INST=1.0,
                   REFERENCE='AUTRE_ASTER',
                   RESULTAT=U3L,
                   NOM_CHAM='DEPL',
                   NOM_CMP='DX',
                   VALE_CALC= 1.55686827E-05,
                   VALE_REFE=1.5E-05,
                   PRECISION=0.050000000000000003,
                   ),
                ),
          )

#
FIN()
#
