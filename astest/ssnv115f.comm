# TITRE TOLE ONDULEE ENCASTREE SUR UN BORD EN DEFORMATION PLANE
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
#             SELON OZ
#=======================================================================
#

DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET'),DEBUG=_F(SDVERI='OUI'))

#

MATER=DEFI_MATERIAU( ELAS=_F(  E = 2000.,   NU = 0.3),
                     ECRO_LINE=_F(  D_SIGM_EPSI = 200.,   SY = 1.E+2) )

#

MAILL=LIRE_MAILLAGE(FORMAT='MED',   )

MAILL=DEFI_GROUP( reuse=MAILL,   MAILLAGE=MAILL,
  CREA_GROUP_MA=_F(  NOM = 'TOUT', TOUT = 'OUI'))

#

CHMAT=AFFE_MATERIAU(  MAILLAGE=MAILL,
                          AFFE=_F(  TOUT = 'OUI',   MATER = MATER) )

#

MODEL=AFFE_MODELE(  MAILLAGE=MAILL,
                        AFFE=_F( TOUT = 'OUI',   MODELISATION = 'COQUE_D_PLAN',
                              PHENOMENE = 'MECANIQUE') )

#

ZERO=DEFI_CONSTANTE(    VALE=0.0 )

VAL_FX=DEFI_CONSTANTE(    VALE=5.0 )

#

CHARGE=AFFE_CHAR_MECA_F(  MODELE=MODEL,
            DDL_IMPO=_F( GROUP_NO = 'O',      DX = ZERO, DY = ZERO, DRZ = ZERO),
           FORCE_NODALE=_F( GROUP_NO = 'A',   FX = VAL_FX)   )

#

CARELEM=AFFE_CARA_ELEM(  MODELE=MODEL,
                 COQUE=_F(COQUE_NCOU=7,
       GROUP_MA = 'TOUT',
 EPAIS = 0.05,
                       A_CIS = 0.833333333,     MODI_METRIQUE = 'NON') )

#

RAMPE=DEFI_FONCTION(  NOM_PARA='INST',
            PROL_GAUCHE='LINEAIRE', PROL_DROITE='LINEAIRE',
            VALE=(0.0,0.0,1.0,1.0,) )

#

L_INST=DEFI_LIST_REEL(  DEBUT=0.0,
                INTERVALLE=_F( JUSQU_A = 1.,  NOMBRE = 10) )

#
#
# INTEGRATION PAR COUCHES DANS L'EPAISSEUR DE LA COQUE
# COQUE_NCOU : 7
# MATRICE TANGENTE
#

U=STAT_NON_LINE(
                            MODELE=MODEL,
                        CHAM_MATER=CHMAT,
                         CARA_ELEM=CARELEM,
                             EXCIT=_F(
                CHARGE = CHARGE,
                FONC_MULT = RAMPE),
                         COMPORTEMENT=_F(
                RELATION = 'VMIS_ISOT_LINE',
                ITER_INTE_MAXI = 30,
               ),
                         INCREMENT=_F(
                LIST_INST = L_INST,
                NUME_INST_FIN = 1),
                            NEWTON=_F(
                MATRICE = 'TANGENTE',
                REAC_ITER = 1),
                       CONVERGENCE=_F(
                ITER_GLOB_MAXI = 10,
                RESI_GLOB_RELA = 1.E-6),
                     RECH_LINEAIRE=_F(
                ITER_LINE_MAXI = 3,
                RESI_LINE_RELA = 1.E-5)
              )

#
U=STAT_NON_LINE( reuse=U,
                            MODELE=MODEL,
                        CHAM_MATER=CHMAT,
                         CARA_ELEM=CARELEM,
                             EXCIT=_F(
                CHARGE = CHARGE,
                FONC_MULT = RAMPE),
                         COMPORTEMENT=_F(
                RELATION = 'VMIS_ISOT_LINE',
                ITER_INTE_MAXI = 30,
                RESI_INTE_RELA = 1.E-8,
               ),
                         ETAT_INIT=_F(
                EVOL_NOLI = U,
                NUME_ORDRE = 1),
                         INCREMENT=_F(
                LIST_INST = L_INST),
                            NEWTON=_F(
                MATRICE = 'TANGENTE',
                REAC_ITER = 1),
                       CONVERGENCE=_F(
                ITER_GLOB_MAXI = 10,
                RESI_GLOB_RELA = 1.E-6),
                     RECH_LINEAIRE=_F(
                ITER_LINE_MAXI = 3,
                RESI_LINE_RELA = 1.E-5)
              )

#

TEST_RESU(RESU=(_F(NUME_ORDRE=10,
                   RESULTAT=U,
                   NOM_CHAM='DEPL',
                   NOEUD='NO000002',
                   NOM_CMP='DX',
                   VALE_CALC=0.027535065,
                   VALE_REFE=0.02743,
                   REFERENCE='NON_DEFINI',
                   PRECISION=0.014999999999999999,),
                _F(NUME_ORDRE=10,
                   RESULTAT=U,
                   NOM_CHAM='DEPL',
                   NOEUD='NO000002',
                   NOM_CMP='DY',
                   VALE_CALC=-0.284769948,
                   VALE_REFE=-0.28039999999999998,
                   REFERENCE='NON_DEFINI',
                   PRECISION=0.017999999999999999,),
                ),
          )

#
# INTEGRATION PAR COUCHES DANS L'EPAISSEUR DE LA COQUE
# COQUE_NCOU : 7
# MATRICE ELASTIQUE
#

V=STAT_NON_LINE(
                            MODELE=MODEL,
                        CHAM_MATER=CHMAT,
                         CARA_ELEM=CARELEM,
                             EXCIT=_F(
                CHARGE = CHARGE,
                FONC_MULT = RAMPE),
                         COMPORTEMENT=_F(
                RELATION = 'VMIS_ISOT_LINE',
                ITER_INTE_MAXI = 30,
                RESI_INTE_RELA = 1.E-8,
               ),
                         INCREMENT=_F(
                LIST_INST = L_INST,
                NUME_INST_FIN = 1),
                            NEWTON=_F(
                MATRICE = 'ELASTIQUE'),
                       CONVERGENCE=_F(
                ITER_GLOB_MAXI = 300,
                RESI_GLOB_RELA = 1.E-6)
              )

#
V=STAT_NON_LINE( reuse=V,
                            MODELE=MODEL,
                        CHAM_MATER=CHMAT,
                         CARA_ELEM=CARELEM,
                             EXCIT=_F(
                CHARGE = CHARGE,
                FONC_MULT = RAMPE),
                         COMPORTEMENT=_F(
                RELATION = 'VMIS_ISOT_LINE',
                ITER_INTE_MAXI = 30,
                RESI_INTE_RELA = 1.E-8,
               ),
                         ETAT_INIT=_F(
                EVOL_NOLI = V,
                NUME_ORDRE = 1),
                         INCREMENT=_F(
                LIST_INST = L_INST),
                            NEWTON=_F(
                MATRICE = 'ELASTIQUE'),
                       CONVERGENCE=_F(
                ITER_GLOB_MAXI = 300,
                RESI_GLOB_RELA = 1.E-6)
              )

#

TEST_RESU(RESU=(_F(NUME_ORDRE=10,
                   RESULTAT=V,
                   NOM_CHAM='DEPL',
                   NOEUD='NO000002',
                   NOM_CMP='DX',
                   VALE_CALC=0.027534961,
                   VALE_REFE=0.02743,
                   REFERENCE='NON_DEFINI',
                   PRECISION=0.014999999999999999,),
                _F(NUME_ORDRE=10,
                   RESULTAT=V,
                   NOM_CHAM='DEPL',
                   NOEUD='NO000002',
                   NOM_CMP='DY',
                   VALE_CALC=-0.284768843,
                   VALE_REFE=-0.28039999999999998,
                   REFERENCE='NON_DEFINI',
                   PRECISION=0.017999999999999999,),
                ),
          )

#

#---------------------------------------------------------------------
FIN()
#
