# TITRE DISQUE FISSURE EN PRESENCE DE CONTRAINTES INITIALES
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY  
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY  
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR     
# (AT YOUR OPTION) ANY LATER VERSION.                                                    
#                                                                       
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT   
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF            
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              
#                                                                       
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,         
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        
# ======================================================================
DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET'), DEBUG=_F(SDVERI='OUI'))


#####################
##CALCUL THERMIQUE
#####################

#Maillage mecanique (Barsoum)
MAILMECA=LIRE_MAILLAGE(UNITE=20,
                       FORMAT='MED',
                        );

MAILMECA=MODI_MAILLAGE(reuse =MAILMECA,
                       MAILLAGE=MAILMECA,
                       MODI_MAILLE=_F(OPTION='NOEUD_QUART',
                       GROUP_NO_FOND='G',),);

MAILMECA=MODI_MAILLAGE(reuse =MAILMECA,
                       MAILLAGE=MAILMECA,
                       MODI_MAILLE=_F(OPTION='NOEUD_QUART',
                       GROUP_NO_FOND='H',),);

TEMPVARX=DEFI_FONCTION(NOM_PARA='X',VALE=(-100,260,
                                           0,130,
                                           22,100,
                                           100,200),);

CHP_TEMP=CREA_CHAMP( OPERATION='AFFE', TYPE_CHAM='NOEU_TEMP_F',
                     MAILLAGE=MAILMECA,
                     AFFE=_F(TOUT = 'OUI',
                     NOM_CMP = 'TEMP',
                     VALE_F = TEMPVARX))

CTE250=DEFI_FONCTION(NOM_PARA='X',
                     VALE=(-100,250,
                            100,250),);

CHTEMP0=CREA_CHAMP( OPERATION='AFFE',
                    TYPE_CHAM='NOEU_TEMP_F',
                    MAILLAGE=MAILMECA,
                    AFFE=_F(TOUT = 'OUI',
                            NOM_CMP = 'TEMP',
                            VALE_F = CTE250))

#Calcul thermique
LIST1=DEFI_LIST_REEL(DEBUT=0,
                     INTERVALLE=_F(JUSQU_A=2,NOMBRE=2,),);

RESUTHER=CREA_RESU(OPERATION='AFFE',
                   TYPE_RESU='EVOL_THER',
                   NOM_CHAM='TEMP',
                   AFFE=(_F(INST = -1,
                            CHAM_GD = CHTEMP0),
                         _F(LIST_INST = LIST1,
                            CHAM_GD = CHP_TEMP),)
                   )

##CALCUL MECANIQUE AVEC TEMPERATURE, FISSURE FERMEE => RESUMECA, pour champs
#de contrainte initiale

MO=AFFE_MODELE(MAILLAGE=MAILMECA,
               AFFE=_F(TOUT='OUI',
                       PHENOMENE='MECANIQUE',
                       MODELISATION='D_PLAN',),);

##Materiau dependant de la temperature
MAMEC=DEFI_MATERIAU(ELAS=_F(E=210000,
                            NU=0.3,
                            ALPHA=10e-6,),);

MAMECA=AFFE_MATERIAU(MAILLAGE=MAILMECA,
                     AFFE=_F(TOUT='OUI',
                             MATER=MAMEC,),
                     AFFE_VARC=_F(TOUT='OUI',
                                  NOM_VARC='TEMP',
                                  EVOL = RESUTHER,
                                  VALE_REF=250,),);
##Chargement : encastrement
ENCASTRE=AFFE_CHAR_MECA(MODELE=MO,
                        DDL_IMPO=_F(GROUP_NO='Contour',
                                    DX=0,
                                    DY=0,),);
##Chargement : fissure fermee
BLOCFISS=AFFE_CHAR_MECA(MODELE=MO,
                        LIAISON_GROUP=(_F(GROUP_NO_1='lev_sup',
                                          GROUP_NO_2='lev_inf',
                                          DDL_1='DX',
                                          COEF_MULT_1=1,
                                          DDL_2='DX',
                                          COEF_MULT_2=-1,
                                          COEF_IMPO=0,),
                                       _F(GROUP_NO_1='lev_sup',
                                          GROUP_NO_2='lev_inf',
                                          DDL_1='DY',
                                          COEF_MULT_1=1,
                                          DDL_2='DY',
                                          COEF_MULT_2=-1,
                                          COEF_IMPO=0,),),);

INSTMECA=DEFI_LIST_REEL(DEBUT=-1,
                        INTERVALLE=(_F(JUSQU_A=0,PAS=1,),
                                    _F(JUSQU_A=1,PAS=1,),),);

##Calcul thermomecanique avec fissure fermee pour extraction des contraintes
RESUMECA=STAT_NON_LINE(MODELE=MO,
                       CHAM_MATER=MAMECA,
                       EXCIT=(_F(CHARGE=ENCASTRE,),
                              _F(CHARGE=BLOCFISS,),),
                       COMPORTEMENT=_F(RELATION='ELAS',),
                       INCREMENT=_F(LIST_INST=INSTMECA,),);
RESUMECA=CALC_CHAMP(reuse=RESUMECA,
                    RESULTAT=RESUMECA,
                    CONTRAINTE=('SIEF_ELNO','SIEF_NOEU',),
                    DEFORMATION=('EPSI_ELGA','EPSP_ELNO',),
                    CRITERES=('SIEQ_ELGA',),
                    VARI_INTERNE=('VARI_ELNO',),);

##Extraction des contraintes au noeuds et points de Gauss
SIEFELGA=CREA_CHAMP(TYPE_CHAM='ELGA_SIEF_R',
                    OPERATION='EXTR',
                    RESULTAT=RESUMECA,
                    NOM_CHAM='SIEF_ELGA',
                    INST=1,);

SIEFELNO=CREA_CHAMP(TYPE_CHAM='ELNO_SIEF_R',
                    OPERATION='EXTR',
                    RESULTAT=RESUMECA,
                    NOM_CHAM='SIEF_ELNO',
                    INST=1,);
SIEFNOEU=CREA_CHAMP(TYPE_CHAM='NOEU_SIEF_R',
                    OPERATION='EXTR',
                    RESULTAT=RESUMECA,
                    NOM_CHAM='SIEF_NOEU',
                    INST=1,);
##Calcul thermomecanique sans etat init ouverture de la fissure
RESOUVFI=STAT_NON_LINE(MODELE=MO,
                       CHAM_MATER=MAMECA,
                       EXCIT=(_F(CHARGE=ENCASTRE,),),
                       COMPORTEMENT=_F(RELATION='ELAS',),
                       INCREMENT=_F(LIST_INST=INSTMECA,),);

RESOUVFI=CALC_CHAMP(reuse=RESOUVFI,
                    RESULTAT=RESOUVFI,
                    CONTRAINTE=('SIEF_ELNO',),
                    DEFORMATION=('EPSI_ELGA','EPSP_ELNO',),
                    CRITERES=('SIEQ_ELGA',),
                    VARI_INTERNE=('VARI_ELNO',),);

##Definition du fond de fissure et des couronnes pour reutilisation dans resuini

FISS1=DEFI_FOND_FISS(MAILLAGE=MAILMECA,
                     FOND_FISS=_F(GROUP_NO='G',),
                     LEVRE_SUP=_F(GROUP_MA='lev_sup',),
                     LEVRE_INF=_F(GROUP_MA='lev_inf',),);

THETA1=CALC_THETA(MODELE=MO,
                  DIRECTION=(-1,0,0,),
                  THETA_2D=_F(GROUP_NO='G',
                              MODULE=1,
                              R_INF=0,
                              R_SUP=0.5,),);

THETA20=CALC_THETA(MODELE=MO,
                   DIRECTION=(-1,0,0,),
                   THETA_2D=_F(GROUP_NO='G',
                               MODULE=1,
                               R_INF=0.5,
                               R_SUP=11.6184,),);

THETAc2=CALC_THETA(MODELE=MO,
                   DIRECTION=(-1,0,0,),
                   THETA_2D=_F(GROUP_NO='G',
                               MODULE=1,
                               R_INF=10,
                               R_SUP=20,),);

########## Calcul de G en presence du champ de contrainte residuelle genere par le calcul thermique) #########
## Definition de la liste d'instants
LIST3=DEFI_LIST_REEL(DEBUT=2,
                     INTERVALLE=_F(JUSQU_A=3,PAS=1,),);

## Affectation du materiau mecanique (sans chargement thermique)
MAMECA2=AFFE_MATERIAU(MAILLAGE=MAILMECA,
                      AFFE=_F(TOUT='OUI',MATER=MAMEC,),);
## Resolution ouverture de la fissure en presence d'un champ de contrainte residuelle
RESUINI=STAT_NON_LINE(MODELE=MO,
                      CHAM_MATER=MAMECA2,
                      EXCIT=(_F(CHARGE=ENCASTRE,),),
                      COMPORTEMENT=_F(RELATION='ELAS',),
                      ETAT_INIT=_F(SIGM=SIEFELGA,),
                      CONVERGENCE=_F(RESI_GLOB_MAXI=1.E-10),
                      INCREMENT=_F(LIST_INST=LIST3),);
## Calcul postraitement
RESUINI=CALC_CHAMP(reuse=RESUINI,
                   RESULTAT=RESUINI,
                   DEFORMATION=('EPSI_ELGA','EPSP_ELNO',),
                   CRITERES=('SIEQ_ELGA',),
                   VARI_INTERNE=('VARI_ELNO',),);

## Calcul de G en utilisant les couronnes precedemment definies (dans le calcul thermique)
# et les differentes possibilites de l'etat initial.
GINGA=CALC_G(THETA=_F(THETA=THETA1,
                      #FOND_FISS=FISS1,
                      ),
             RESULTAT=RESUINI,
             INST=3,
             COMPORTEMENT=_F(RELATION='ELAS'),
             ETAT_INIT =_F(SIGM=SIEFELGA),
             OPTION='CALC_G')

GINNO=CALC_G(THETA=_F(THETA=THETA20,
#                      FOND_FISS=FISS1,
                      ),
             RESULTAT=RESUINI,
             INST=3,
             COMPORTEMENT=_F(RELATION='ELAS'),
             ETAT_INIT =_F(SIGM=SIEFNOEU),
             OPTION='CALC_G')

GINELNO=CALC_G(THETA=_F(THETA=THETAc2,
                        #FOND_FISS=FISS1,
                        ),
               RESULTAT=RESUINI,
               INST=3,
               COMPORTEMENT=_F(RELATION='ELAS'),
               ETAT_INIT=_F(SIGM=SIEFELNO),
               OPTION='CALC_G')

GTHER=CALC_G(THETA=_F(THETA=THETAc2,
                      #FOND_FISS=FISS1,
                      ),
             RESULTAT=RESOUVFI,
             INST=1,
             COMPORTEMENT=_F(RELATION='ELAS',),
             OPTION='CALC_G')




## Impression des resultats
TEST_TABLE(PRECISION=0.02,
           VALE_CALC=55.3956139743,
           VALE_REFE=55.359999999999999,
           REFERENCE='SOURCE_EXTERNE',
           NOM_PARA='G',
           TABLE=GINGA,
           FILTRE=_F(NOM_PARA='INST',
                     VALE=3,),
           )

TEST_TABLE(PRECISION=0.02,
           VALE_CALC=55.3638572265,
           VALE_REFE=55.359999999999999,
           REFERENCE='SOURCE_EXTERNE',
           NOM_PARA='G',
           TABLE=GINNO,
           FILTRE=_F(NOM_PARA='INST',
                     VALE=3,),
           )

TEST_TABLE(PRECISION=0.02,
           VALE_CALC=55.370419349,
           VALE_REFE=55.359999999999999,
           REFERENCE='SOURCE_EXTERNE',
           NOM_PARA='G',
           TABLE=GINELNO,
           FILTRE=_F(NOM_PARA='INST',
                     VALE=3,),
           )

TEST_TABLE(VALE_CALC=55.3651620513,
           NOM_PARA='G',
           TABLE=GTHER,
           FILTRE=_F(NOM_PARA='INST',
                     VALE=1,),
           )

FIN();
