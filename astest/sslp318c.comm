# TITRE PROPAGATION D UNE FISSURE XFEM NON DEBOUCHANTE SOLLICITE EN MODE I
# ======================================================================
# COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY  
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY  
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR     
# (AT YOUR OPTION) ANY LATER VERSION.                                                    
#                                                                       
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT   
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF            
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              
#                                                                       
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,         
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        
# ======================================================================
# person_in_charge: daniele.colombo at ifpen.fr

DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET',VISU_EFICAS='NON'),DEBUG=_F(SDVERI='OUI'),)

#***********************************
# MAILLAGE ET MODELE
#***********************************

MaiPlate=LIRE_MAILLAGE(FORMAT='MED',
                       INFO_MED=1,);

MaiPlate=MODI_MAILLAGE(reuse =MaiPlate,
                       MAILLAGE=MaiPlate,
                       ORIE_PEAU_2D=_F(GROUP_MA='force',),);

plate=AFFE_MODELE(MAILLAGE=MaiPlate,
                   AFFE=(_F(GROUP_MA=('All',),
                           PHENOMENE='MECANIQUE',
                           MODELISATION='C_PLAN',),
                        _F(GROUP_MA=('force',),
                           PHENOMENE='MECANIQUE',
                           MODELISATION='C_PLAN',),),);

#***********************************
# MATERIAU
#***********************************

steel=DEFI_MATERIAU(ELAS=_F(E=2.06E11,
                            NU=0.33,),);

champma=AFFE_MATERIAU(MAILLAGE=MaiPlate,
                      AFFE=_F(TOUT='OUI',
                              MATER=steel,),);

#***********************************
# DEFINITION DE LA FISSURE
#***********************************

# LONGUEUR DE DEMI FISSURE INITIALE
a0 = 150

# DEFINITION DES LEVEL SETS
LN = FORMULE(VALE='Y',
             NOM_PARA=('Y'));

LT = FORMULE(VALE='abs(X)-a0',
             NOM_PARA=('X'));

# NOMBRE DE PROPAGATIONS
NPS = 3
NPS = NPS+2

Fiss = [None]*NPS

Fiss[1]=DEFI_FISS_XFEM(MODELE=plate,
                       DEFI_FISS=_F(FONC_LT=LT,
                                    FONC_LN=LN,),);

# FUNCTION POUR AJOUTER LA COLONNE NUME_ORDRE
# A LA TABLE ISSUE PAR CALC_G
FOND1=FORMULE(NOM_PARA='NUME_ORDRE',
              VALE='0*NUME_ORDRE+1',);
FOND2=FORMULE(NOM_PARA='NUME_ORDRE',
              VALE='0*NUME_ORDRE+2',);

#****************************
# PROPAGATION DE LA FISSURE
#****************************

force = [None]*NPS
vinc = [None]*NPS
ModX = [None]*NPS
ChgX = [None]*NPS
ResX = [None]*NPS
SIFR = [None]*NPS
SIFL = [None]*NPS
SIF = [None]*NPS

RI = 2*25.
RS = 2*RI

# AVANCE DE LA FISSURE A CHAQUE ITERATION
da_fiss = 30.

for i in range(1,NPS-1) :

    ModX[i]=MODI_MODELE_XFEM(MODELE_IN=plate,
                         FISSURE=Fiss[i]);

    vinc[i]=AFFE_CHAR_MECA(MODELE=ModX[i],
                           DDL_IMPO=(_F(GROUP_NO='incastro',
                                        DX=0,
                                        DY=0,),
                                     _F(GROUP_NO='carrello',
                                        DY=0,),),);

    force[i]=AFFE_CHAR_MECA(MODELE=ModX[i],
                            PRES_REP=_F(GROUP_MA='force',
                                        PRES=-1E6,),);


    ResX[i]=MECA_STATIQUE(MODELE=ModX[i],
                         CHAM_MATER=champma,
                         EXCIT=(_F(CHARGE=force[i],),
                                _F(CHARGE=vinc[i],),
                                ),
                         INST=1.,);

#   CALCULE DES FACTEURS D'INTENSITE DE CONTRAINTES POUR LE FOND
#   DE FISSURE A GAUCHE
    SIFL[i]=CALC_G(THETA=_F(FISSURE=Fiss[i],),
                   RESULTAT=ResX[i],
                   OPTION='CALC_K_G',);

#   CALCULE DES FACTEURS D'INTENSITE DE CONTRAINTES POUR LE FOND
#   DE FISSURE A DROITE
    SIFR[i]=CALC_G(THETA=_F(FISSURE=Fiss[i],
                            NUME_FOND=2,),
                   RESULTAT=ResX[i],
                   OPTION='CALC_K_G',);

#   ON MODIFIE LA TABLE DES SIF POUR AJOUTER LA COLONNE NUME_FISS QUI EST
#   NECESSAIRE POUR PROPA_FISS (VOIR DOCUMENTATION). EN PLUS, ON FAIT L'UNION
#   DE LES DEUX TABLES.
    SIF[i]=CALC_TABLE(TABLE=SIFR[i],
                      ACTION=(_F(OPERATION='COMB',
                                TABLE=SIFL[i],
                                NOM_PARA=('NUME_ORDRE','NUME_FOND'),),
                             ),
                      );

    IMPR_TABLE(TABLE=SIF[i],);

    if ( i != NPS ) :
      Fiss[i+1]=CO('Fiss_%d'%(i+1))
      PROPA_FISS(MODELE=ModX[i],
                 FISSURE=(_F(FISS_ACTUELLE=Fiss[i],
                             FISS_PROPAGEE=Fiss[i+1],
                             TABLE=SIF[i],
                            ),
                          ),
                 DA_MAX=da_fiss,
                 METHODE_PROPA='GEOMETRIQUE',
                 LOI_PROPA=_F(LOI='PARIS',
                              C=1.,
                              M=1.,
                              MATER=steel),
                 COMP_LINE=_F(COEF_MULT_MINI=0.,
                              COEF_MULT_MAXI=1.,
                             ),
                 RAYON=RS,
                 INFO=0,);

#----------------------------------------------
#         VERIFICATION DES RESULTATS
#----------------------------------------------

W=1000;
sigma=1E6
KI_calc =[2.2914396426887E+07,2.5803538819812E+07, 2.8809311044511E+07  ]
KII_calc =[7488.1641027818, 4193.0445613429, 5155.0375733729]

for i in range(1,NPS-1) :
   
   a=a0+da_fiss*(i-1)
   # VALEUR ANALYTIQUE DE KI (BROEK)
   KI_broek=sigma*sqrt(3.1415*a/cos(3.1415*a/W));

#  TOLERANCE SUR KII. LA VALEUR ANALYTIQUE EST ZERO CAR LA FISSURE
#  PROPAGE EN MODE I. CELA N'EST PAS VERIFIER EXACTEMENT POUR LE
#  MODELE FEM. ON ASSUME QUE LA VALEUR DE KII EST ZERO SI
#  ELLE EST EGAL A 1% DE LA VALEUR DE KI.
   TOL_K2=0.01*KI_broek;

   TEST_TABLE(REFERENCE='ANALYTIQUE',
              PRECISION=0.050000000000000003,
              VALE_CALC=KI_calc[i-1],
              VALE_REFE=KI_broek,
              NOM_PARA='K1',
              TYPE_TEST='MAX',
              TABLE=SIF[i],)

   TEST_TABLE(CRITERE='ABSOLU',
              REFERENCE='ANALYTIQUE',
              PRECISION=TOL_K2,
              VALE_CALC=KII_calc[i-1],
              TOLE_MACHINE=1.E-5,       #TODO TOLE_MACHINE variable entre les machines
              VALE_REFE=0.0,
              NOM_PARA='K2',
              TYPE_TEST='MAX',
              TABLE=SIF[i],)

#----------------------------------------------
#         EDITION DE FICHIERS MED
#----------------------------------------------

MAXFM = [None]*NPS
MOVIS = [None]*NPS
DEPL = [None]*NPS


for i in range(1,NPS-1) :
   MAXFM[i]=POST_MAIL_XFEM(MODELE=ModX[i]);

   MOVIS[i]=AFFE_MODELE(MAILLAGE=MAXFM[i],
                         AFFE=_F(TOUT='OUI',
                                 PHENOMENE='MECANIQUE',
                                 MODELISATION='C_PLAN',),) 

   DEPL[i]=POST_CHAM_XFEM(
                          MODELE_VISU   = MOVIS[i],
                          RESULTAT=ResX[i],
                          );

   DEFI_FICHIER(UNITE=31,);
   IMPR_RESU(FORMAT='MED',
             UNITE=31,
             RESU=_F(RESULTAT=DEPL[i],),);


FIN();
