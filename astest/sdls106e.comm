# TITRE CALCUL MODAL D'UNE PLAQUE RECTANGULAIRE SIMPLEMENT APPUYEE SUR TOUS SES BORDS
#
# ======================================================================
# COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY  
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY  
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR     
# (AT YOUR OPTION) ANY LATER VERSION.                                                    
#                                                                       
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT   
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF            
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU      
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.                              
#                                                                       
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE     
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,         
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.        
# ======================================================================

########################################################################

#           SOUS-STRUCTURATION CLASSIQUE

#           PLAQUE APPUYEE SUR SES BORDS DECOUPEE EN 2 PARTIES

#           AVEC INTERFACES DE TYPE CRAIG-BAMPTON 

#           MAILLAGES INCOMPATIBLES ET MODES DE COUPLAGE

#           RACCORDEMENT AVEC ELIMINATION DES CONTRAINTES

########################################################################

#
#123678901234567890123456789012345678901234567890123456789012345678901
#
DEBUT(CODE=_F(NIV_PUB_WEB='INTERNET'),
      DEBUG=_F(SDVERI='OUI'))

#

PRE_IDEAS(UNITE_MAILLAGE=20,
          UNITE_IDEAS=19,
          );
          
PRE_IDEAS(UNITE_MAILLAGE=30,
          UNITE_IDEAS=29,
          );         

MAILLA1=LIRE_MAILLAGE(UNITE=20,FORMAT='ASTER',)
MAILLA2=LIRE_MAILLAGE(UNITE=30,FORMAT='ASTER',)


MODELE1=AFFE_MODELE(MAILLAGE=MAILLA1,
                    AFFE=_F( GROUP_MA = 'CALCUL',
                             PHENOMENE = 'MECANIQUE',
                             MODELISATION = 'DKT'),);

MODELE2=AFFE_MODELE(MAILLAGE=MAILLA2,
                    AFFE=_F( GROUP_MA = 'CALCUL',
                             PHENOMENE = 'MECANIQUE',
                             MODELISATION = 'DKT'),);
                             
                             
#
MATER=DEFI_MATERIAU(  ELAS=_F( E = 2.1E11,  NU = 0.30,  RHO = 7800.0))

#
CHAMAT1=AFFE_MATERIAU(  MAILLAGE=MAILLA1,
                       AFFE=_F( GROUP_MA = 'CALCUL',
                                    MATER = MATER))
CHAMAT2=AFFE_MATERIAU(  MAILLAGE=MAILLA2,
                       AFFE=_F( GROUP_MA = 'CALCUL',
                                    MATER = MATER))                                    
#
PARAM1=AFFE_CARA_ELEM(  MODELE=MODELE1,
                            COQUE=_F( EPAIS = 0.01,
                                   GROUP_MA = 'CALCUL'))
PARAM2=AFFE_CARA_ELEM(  MODELE=MODELE2,
                            COQUE=_F( EPAIS = 0.01,
                                   GROUP_MA = 'CALCUL'))
#


########################################################################
#
#        PLAQUE APPUYEE-APPUYEE DECOUPEE EN 2 PARTIES
#   SOUS-STRUCTURATION CLASSIQUE - INTERFACES TYPE CRAIG-BAMPTON
#
########################################################################
#
#
# SOUS-STRUCTURE 1 APPUYEE-ENCASTREE - INTERFACE TYPE CRAIG-BAMPTON
#
CHARGE_1=AFFE_CHAR_MECA(  MODELE=MODELE1,DDL_IMPO=(
               _F( GROUP_NO = 'BLOQUE', DX = 0.0, DY = 0.0, DZ = 0.0),
               _F( GROUP_NO = 'GAUCHE', DX = 0.0, DY = 0.0, DZ = 0.0,
                         DRX = 0.0, DRY = 0.0, DRZ = 0.0),
               _F( GROUP_NO = 'BAS', DX = 0.0, DY = 0.0, DZ = 0.0),
               _F( GROUP_NO = 'HAUT', DX = 0.0, DY = 0.0, DZ = 0.0)))

MACEL1 = CREA_ELEM_SSD(
                 MODELE = MODELE1,
                 CHARGE = CHARGE_1,
                 CHAM_MATER = CHAMAT1,
                 CARA_ELEM = PARAM1,
                 INTERFACE = _F( NOM = 'GAUCHE',
                               TYPE = 'CRAIGB',
                               MASQUE = ('DX','DY','DRZ',),
                               GROUP_NO = 'GAUCHE'),
                 BASE_MODALE = _F( TYPE = 'RITZ',
                                   NMAX_MODE_INTF = 20,),
                 CALC_FREQ = _F(NMAX_FREQ = 6,),)
#
# SOUS-STRUCTURE 2 ENCASTREE-APPUYEE - INTERFACE TYPE CRAIG-BAMPTON
#
CHARGE_2=AFFE_CHAR_MECA(  MODELE=MODELE2,DDL_IMPO=(
               _F( GROUP_NO = 'BLOQUE', DX = 0.0, DY = 0.0, DZ = 0.0,
                         DRX = 0.0, DRY = 0.0, DRZ = 0.0),
               _F( GROUP_NO = 'GAUCHE', DX = 0.0, DY = 0.0, DZ = 0.0),
               _F( GROUP_NO = 'BAS', DX = 0.0, DY = 0.0, DZ = 0.0),
               _F( GROUP_NO = 'HAUT', DX = 0.0, DY = 0.0, DZ = 0.0)))

MACEL2 = CREA_ELEM_SSD(
                 MODELE = MODELE2,
                 CHARGE = CHARGE_2,
                 CHAM_MATER = CHAMAT2,
                 CARA_ELEM = PARAM2,
                 INTERFACE = _F( NOM = 'BLOQUE',
                                TYPE = 'CRAIGB',
                                MASQUE = ('DX','DY','DRZ'),
                                GROUP_NO = 'BLOQUE'),
                 BASE_MODALE = _F( TYPE = 'RITZ',
                                   NMAX_MODE_INTF = 20,),
                 CALC_FREQ = _F(NMAX_FREQ = 6,),)

#
#   CALCUL SUR MODELE GENERALISE
#
ASSE_ELEM_SSD(
                 RESU_ASSE_SSD = _F(
                               MODELE = CO('MODEGE'),
                               NUME_DDL_GENE = CO('NUMEGE'),
                               RIGI_GENE = CO('RIGGEN'),
                               MASS_GENE = CO('MASGEN'),
                             ),
                 SOUS_STRUC = (
                            _F(NOM = 'CARRE1',
                               MACR_ELEM_DYNA = MACEL1,),
                            _F(NOM = 'CARRE2',
                               MACR_ELEM_DYNA = MACEL2,
                               TRANS = (0.,0.5,0.),
                               ANGL_NAUT = (-90.,0.,0.),),
                             ),
                 LIAISON = (
                           _F(SOUS_STRUC_1 = 'CARRE1',
                              INTERFACE_1 = 'GAUCHE',
                              GROUP_MA_MAIT_1 = 'CALCUL',
                              OPTION = 'REDUIT',
                              SOUS_STRUC_2 = 'CARRE2',
                              INTERFACE_2 = 'BLOQUE',),
                           ),
                 VERIF = _F( STOP_ERREUR = 'OUI',
                             PRECISION = 1.E-6,
                             CRITERE = 'RELATIF'),
                 METHODE = 'ELIMINE',
               ),

#
#   CALCUL DES MODES PROPRES DE LA STRUCTURE GLOBALE
#
RESGEN=MODE_ITER_SIMULT( MATR_RIGI=RIGGEN,
                         MATR_MASS=MASGEN,
                         METHODE='SORENSEN',
                         CALC_FREQ=_F( OPTION = 'PLUS_PETITE',
                                       NMAX_FREQ=10,),
                         VERI_MODE=_F(PREC_SHIFT=5.0000000000000001E-3,
                                      STOP_ERREUR='NON',
                                      STURM='NON',
                                      SEUIL=9.9999999999999995E-01),
                         STOP_BANDE_VIDE='NON',
                        )

#
#   CREATION DU MAILLAGE SQUELETTE DE LA STRUCTURE GLOBALE
#

SQUEL=DEFI_SQUELETTE(  MODELE_GENE=MODEGE,SOUS_STRUC=(
                            _F( NOM = 'CARRE1',
                                GROUP_MA = 'CALCUL',
                              ),
                            _F( NOM = 'CARRE2',
                                GROUP_MA = 'CALCUL',
                                ))
                                )
#
#   RESTITUTION SUR MAILLAGE SQUELETTE
#
MODGLO=REST_SOUS_STRUC(RESU_GENE=RESGEN,
                       SQUELETTE=SQUEL,
                       TOUT_ORDRE='OUI',
                       TOUT_CHAM='OUI')

MODCOT1=REST_SOUS_STRUC(RESU_GENE=RESGEN,
                       TOUT_ORDRE='OUI',
                       SOUS_STRUC='CARRE1',);

MODCOT2=REST_SOUS_STRUC(RESU_GENE=RESGEN,
                       TOUT_ORDRE='OUI',
                       SOUS_STRUC='CARRE2',);

# VERIFICATION CONTINUITE DEPLACEMENT AUX INTERFACES
T1_DZ=POST_RELEVE_T(ACTION=_F(INTITULE='CARRE1',
                      NOEUD=('N9'),
                      RESULTAT=MODCOT1,
                      NOM_CHAM='DEPL',
                      TOUT_ORDRE='OUI',
                      NOM_CMP='DZ',
                      OPERATION='EXTRACTION',
                      ),);

T2_DZ=POST_RELEVE_T(ACTION=_F(INTITULE='CARRE2',
                      NOEUD=('N70'),
                      RESULTAT=MODCOT2,
                      NOM_CHAM='DEPL',
                      TOUT_ORDRE='OUI',
                      NOM_CMP='DZ',
                      OPERATION='EXTRACTION',
                      ),);

F_DZ=FORMULE(NOM_PARA=('DZ_C1', 'DZ_C2',),
                VALE='abs (DZ_C1 - DZ_C2)')

TC_DZ=CALC_TABLE(TABLE=T1_DZ,
              ACTION=(_F(OPERATION='RENOMME',
                                  NOM_PARA=('DZ', 'DZ_C1'),),
                               _F(OPERATION='COMB',
                                  TABLE=T2_DZ,
                                  NOM_PARA=('NUME_ORDRE',),),
                               _F(OPERATION='RENOMME',
                                  NOM_PARA=('DZ', 'DZ_C2'),),
                               _F(OPERATION='OPER',
                                  NOM_PARA='ECART',
                                  FORMULE=F_DZ),
                      ),
              TITRE='Comparaison DZ ssd 1 / ssd 2')

#DRY de ssd 1 correspond a -DRX de ssd 2

T1_DRY=POST_RELEVE_T(ACTION=_F(INTITULE='CARRE1',
                      NOEUD=('N9'),
                      RESULTAT=MODCOT1,
                      NOM_CHAM='DEPL',
                      TOUT_ORDRE='OUI',
                      NOM_CMP='DRY',
                      OPERATION='EXTRACTION',
                      ),);

T2_DRX=POST_RELEVE_T(ACTION=_F(INTITULE='CARRE2',
                      NOEUD=('N70'),
                      RESULTAT=MODCOT2,
                      NOM_CHAM='DEPL',
                      TOUT_ORDRE='OUI',
                      NOM_CMP='DRX',
                      OPERATION='EXTRACTION',
                      ),);

F_DRXY=FORMULE(NOM_PARA=('DRY_C1', 'DRX_C2',),
                VALE='abs (DRY_C1 + DRX_C2)')

TC_DRXY=CALC_TABLE(TABLE=T1_DRY,
              ACTION=(_F(OPERATION='RENOMME',
                                  NOM_PARA=('DRY', 'DRY_C1'),),
                               _F(OPERATION='COMB',
                                  TABLE=T2_DRX,
                                  NOM_PARA=('NUME_ORDRE',),),
                               _F(OPERATION='RENOMME',
                                  NOM_PARA=('DRX', 'DRX_C2'),),
                               _F(OPERATION='OPER',
                                  NOM_PARA='ECART',
                                  FORMULE=F_DRXY),
                      ),
              TITRE='Comparaison DRXY ssd 1 / ssd 2')

#IMPR_TABLE(TABLE=TC_DZ)
#IMPR_TABLE(TABLE=TC_DRXY)

TEST_TABLE(CRITERE='ABSOLU',
           REFERENCE='ANALYTIQUE',
           PRECISION=5.E-3,
           TOLE_MACHINE=5.E-3,      #TODO variabilite
           VALE_CALC= 0.00397790378725,
           VALE_REFE=0.0,
           NOM_PARA='ECART',
           TYPE_TEST='SOMM',
           TABLE=TC_DZ,)

TEST_TABLE(CRITERE='ABSOLU',
           REFERENCE='ANALYTIQUE',
           PRECISION=0.02,         #7.0E-3,
           TOLE_MACHINE=0.02,      #TODO variabilite
           VALE_CALC=0.0115681675186,
           VALE_REFE=0.0,
           NOM_PARA='ECART',
           TYPE_TEST='SOMM',
           TABLE=TC_DRXY,)

# VERIFICATION NULLITE DU TRAVAIL DES FORCES DE LIAISON
# SDVERI:NON EXISTENCE D'OBJETS NON CONFORMES POUR LES table_container
DEBUG(SDVERI='NON');

CALC_ENR=CALC_CORR_SSD(MODELE_GENE=MODEGE,
                     RESU_GENE=RESGEN,
                     SHIFT=1.,
                     UNITE=6)

DEBUG(SDVERI='OUI');

TEST_RESU(RESU=(_F(PARA='FREQ',
                   NUME_MODE=1,
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=MODGLO,
                   VALE_CALC=17.1307294807,
                   VALE_REFE=17.120000000000001,
                   CRITERE='RELATIF',
                   TOLE_MACHINE=1.E-4,      #TODO variabilite
                   PRECISION=0.0125,),
                _F(PARA='FREQ',
                   NUME_MODE=2,
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=MODGLO,
                   VALE_CALC=35.9499761313,
                   VALE_REFE=35.609999999999999,
                   CRITERE='RELATIF',
                   TOLE_MACHINE=1.E-4,      #TODO variabilite
                   PRECISION=0.0125,),
                _F(PARA='FREQ',
                   NUME_MODE=3,
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=MODGLO,
                   VALE_CALC=50.1499255678,
                   VALE_REFE=49.990000000000002,
                   CRITERE='RELATIF',
                   TOLE_MACHINE=1.E-3,      #TODO variabilite
                   PRECISION=0.012500000000000001,),
                _F(PARA='FREQ',
                   NUME_MODE=4,
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=MODGLO,
                   VALE_CALC=66.5561069933,
                   VALE_REFE=66.420000000000002,
                   CRITERE='RELATIF',
                   TOLE_MACHINE=1.E-4,      #TODO variabilite
                   PRECISION=0.0125,),
                _F(PARA='FREQ',
                   NUME_MODE=5,
                   REFERENCE='ANALYTIQUE',
                   RESULTAT=MODGLO,
                   VALE_CALC=68.9003935432,
                   VALE_REFE=68.480000000000004,
                   CRITERE='RELATIF',
                   TOLE_MACHINE=1.E-4,      #TODO variabilite
                   PRECISION=0.0125,),
                ),
          )

FIN()
