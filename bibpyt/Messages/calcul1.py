# coding=utf-8
# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# person_in_charge: josselin.delmas at edf.fr

cata_msg = {

1:_(u"""
La table que l'on tente d'enrichir dans la commande CALCUL n'est pas du type attendu.
Elle n'a pas le bon nombre de paramètres.
"""),

2:_(u"""
La table que l'on tente d'enrichir dans la commande CALCUL n'est pas du type attendu.
Elle n'a pas les bons paramètres.
"""),

3:_(u"""
On trouve plusieurs lignes dans la table pour l'instant %(r1)f .
"""),

4:_(u"""
L'objet %(k1)s à l'instant %(r1)f existe déjà dans la table fournie.
On l'écrase pour le remplacer par le nouveau.
"""),

5 : _(u"""
A cause des erreurs précédentes, le code s'arrête.
  Le champ des variables internes fourni à CALCUL n'est pas cohérent avec le comportement donné par le mot-clef COMPORTEMENT. 
"""),

}
