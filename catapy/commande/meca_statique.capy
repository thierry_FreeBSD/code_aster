# ======================================================================
# COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
# THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
# IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
# THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
# (AT YOUR OPTION) ANY LATER VERSION.
#
# THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
# WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
# MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
# GENERAL PUBLIC LICENSE FOR MORE DETAILS.
#
# YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
# ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
#    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
# ======================================================================
# person_in_charge: jacques.pellet at edf.fr
MECA_STATIQUE=OPER(nom="MECA_STATIQUE",op=46,sd_prod=evol_elas,
                   fr="Résoudre un problème de mécanique statique linéaire",reentrant='f',
            UIinfo={"groupes":("Résolution","Mécanique",)},
         regles=(EXCLUS("INST","LIST_INST"),
                 AU_MOINS_UN('CHAM_MATER','CARA_ELEM',),),
         MODELE          =SIMP(statut='o',typ=modele_sdaster),
         CHAM_MATER      =SIMP(statut='f',typ=cham_mater,
         fr="le CHAM_MATER est nécessaire, sauf si le modèle ne contient que des éléments discrets (modélisations DIS_XXX)",
         ang="CHAM_MATER is compulsory, except if the model contains only discret elements (modelizations DIS_XXX)"),
         CARA_ELEM       =SIMP(statut='f',typ=cara_elem,
         fr="le CARA_ELEM est nécessaire dès que le modèle contient des éléments de structure : coques, poutres, ...",
         ang="CARA_ELEM is compulsory as soon as the model contains structural elements : plates, beams, ..."),
         TITRE           =SIMP(statut='f',typ='TXM',max='**'),
         EXCIT           =FACT(statut='o',max='**',
           CHARGE          =SIMP(statut='o',typ=(char_meca,char_cine_meca)),
           FONC_MULT       =SIMP(statut='f',typ=(fonction_sdaster,nappe_sdaster,formule)),
           TYPE_CHARGE     =SIMP(statut='f',typ='TXM',defaut="FIXE",into=("FIXE",) ),
         ),
         INST            =SIMP(statut='f',typ='R',defaut= 0.E+0 ),
         LIST_INST       =SIMP(statut='f',typ=listr8_sdaster),
         INST_FIN        =SIMP(statut='f',typ='R'),
         OPTION          =SIMP(statut='f',typ='TXM',into=("SIEF_ELGA","SANS"),defaut="SIEF_ELGA",max=1,
             fr="Seule option : contraintes aux points de Gauss. Utilisez CALC_CHAMP pour les autres options.",
                          ),

#-------------------------------------------------------------------
#        Catalogue commun SOLVEUR
         SOLVEUR         =C_SOLVEUR('MECA_STATIQUE'),
#-------------------------------------------------------------------
         INFO            =SIMP(statut='f',typ='I',defaut=1,into=(1,2) ),
)  ;
