%& LIBRARY OPTIONS
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
% person_in_charge: jacques.pellet at edf.fr
EPSI_ELGA
       <<  EPSI_ELGA : DEFORMATIONS PAR ELEMENT AUX POINTS DE GAUSS >>
OPTION__
   IN__
        PGEOMER  GEOM_R 'MAIL!.COORDO'
       <<  PGEOMER : COORDONNEES DES NOEUDS >>
        PDEPLAR  DEPL_R 'RESU!DEPL!N'
       <<  PDEPLAR : DEPLACEMENTS INSTANT ACTUEL >>
        PMATERC  ADRSJEVE 'CHMA!.MATE_CODE'
       <<  PMATERC : CHAMP DE MATERIAU >>
        PTEMPSR  INST_R 'VOLA!&&CCPARA.CH_INST_R'
       <<  PTEMPSR :  INSTANT ACTUEL >>
        PVARCPR  VARI_R 'VOLA!&&CCPARA.VARI_INT_N'
       <<  PVARCPR : VARIABLES DE COMMANDE  >>
        PVARCRR  VARI_R 'VOLA!&&CCPARA.VARI_INT_REF'
       <<  PVARCRR : VARIABLES DE COMMANDE  DE REFERENCE>>
        PCACOQU  CACOQU 'CARA!.CARCOQUE'
       <<  PCACOQU : CARACTERISTIQUE DE COQUE,
           NECESSITE DE FOURNIR LE CONCEPT PRODUIT PAR AFFE_CARA_ELEM  >>
        PCOMPOR  COMPOR 'RESU!COMPORTEMENT!N'
       <<  PCOMPOR : COMPORTEMENT >>
        PCAGEPO  CAGEPO 'CARA!.CARGEOPO'
       <<  PCAGEPO : CARACTERISTIQUE POUTRES, SECTION RECTANGLE OU CERCLE,
           NECESSITE DE FOURNIR LE CONCEPT PRODUIT PAR AFFE_CARA_ELEM  >>
        PCAORIE  CAORIE 'CARA!.CARORIEN'
       <<  PCAORIE : ORIENTATION LOCALE D'UN ELEMENT DE POUTRE OU DE TUYAU,
           ISSUE DE AFFE_CARA_ELEM MOT CLE ORIENTATION >>
        PNBSP_I  NBSP_I 'CARA!.CANBSP'
       <<  PNBSP_I :  NOMBRE DE SOUS_POINTS >>
        PFIBRES  CAFI_R 'CARA!.CAFIBR'
       <<  PFIBRES :  CARACTERISTIQUE DES FIBRES POUR POUTRES MULTI FIBRES,
           NECESSITE DE FOURNIR LE CONCEPT PRODUIT PAR AFFE_CARA_ELEM  >>
        PCAMASS  CAMASS 'CARA!.CARMASSI'
       <<  PCAMASS : CARACTERISTIQUE DE MASSIF,
           NECESSITE DE FOURNIR LE CONCEPT PRODUIT PAR AFFE_CARA_ELEM  >>
        PHARMON  HARMON 'VOLA!&&CCPARA.NUME_MODE'
       <<  PHARMON : NUMERO D'HARMONIQUE DE FOURIER >>
        PSTRXRR  STRX_R 'RESU!STRX_ELGA!N'
       <<  PSTRXRR : CHAMPS ELEMENTS DE STRUCTURE INSTANT ACTUEL >>

   OUT__
        PDEFOPG EPSI_R ELGA__
       <<  PDEFOPG : DEFORMATIONS AUX POINTS DE GAUSS >>
        PDEFOPC EPSI_C ELGA__
       <<  PDEFOPC : DEFORMATIONS COMPLEXES AUX POINTS DE GAUSS >>
