%& LIBRARY TYPELEM
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_MEDPL2_XHC
TYPE_GENE__

%  CATALOGUES DES ELEMENTS 2D_DP X-FEM HEAVISIDE AVEC CONTACT (QUE LINEAIRES ET QUADRATIQUES)

ENTETE__ ELEMENT__ MEDPTR3_XHC      MAILLE__ TRIA3
   ELREFE__  TR3       GAUSS__  RIGI=FPG3  XINT=FPG12 FPG1=FPG1 NOEU_S=NOEU_S  NOEU=NOEU XFEM=XFEM36   FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  SE2       GAUSS__  RIGI=FPG2 MASS=FPG3 FPG2=FPG2 FPG3=FPG3 FPG4=FPG4 NOEU=NOEU GAUSS=FPG3  SIMP=SIMP COTES=COTES SIMP1=SIMP1 COTES1=COTES1 COTES2=COTES2
   ATTRIBUT__  XFEM=XHC XLAG=NOEUD
   ENS_NOEUD__  EN1     =     1   2   3
ENTETE__ ELEMENT__ MEDPQU4_XHC      MAILLE__ QUAD4
   ELREFE__  QU4       GAUSS__  RIGI=FPG4   NOEU_S=NOEU_S FPG1=FPG1 NOEU=NOEU XFEM=XFEM72   FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TR3       GAUSS__  RIGI=FPG3  XINT=FPG12
   ELREFE__  SE2       GAUSS__  RIGI=FPG2 MASS=FPG3 FPG2=FPG2 FPG3=FPG3 FPG4=FPG4 NOEU=NOEU GAUSS=FPG3  SIMP=SIMP COTES=COTES SIMP1=SIMP1 COTES1=COTES1 COTES2=COTES2
   ATTRIBUT__  XFEM=XHC XLAG=NOEUD
   ENS_NOEUD__  EN1     =     1   2   3   4
ENTETE__ ELEMENT__ MEDPTR6_XHC      MAILLE__ TRIA6
   ELREFE__  TR6       GAUSS__  RIGI=FPG3  XINT=FPG12   NOEU_S=NOEU_S FPG1=FPG1 NOEU=NOEU XFEM=XFEM72   FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  SE3       GAUSS__  RIGI=FPG4 MASS=NOEU FPG2=FPG2 FPG3=FPG3 FPG4=FPG4 NOEU=NOEU GAUSS=FPG3  SIMP=SIMP COTES=COTES SIMP1=SIMP1 COTES1=COTES1 COTES2=COTES2
   ELREFE__  SE2       GAUSS__  RIGI=FPG2 MASS=FPG3 FPG2=FPG2 FPG3=FPG3 FPG4=FPG4 NOEU=NOEU GAUSS=FPG3  SIMP=SIMP COTES=COTES SIMP1=SIMP1 COTES1=COTES1 COTES2=COTES2
   ATTRIBUT__  XFEM=XHC XLAG=NOEUD
   ENS_NOEUD__  EN3     =     4   5   6
   ENS_NOEUD__  EN1     =     1   2   3
ENTETE__ ELEMENT__ MEDPQU8_XHC      MAILLE__ QUAD8
   ELREFE__  QU8       GAUSS__  RIGI=FPG9   NOEU_S=NOEU_S FPG1=FPG1 NOEU=NOEU XFEM=XFEM144  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TR6       GAUSS__  RIGI=FPG3  XINT=FPG12
   ELREFE__  SE3       GAUSS__  RIGI=FPG4 MASS=NOEU FPG2=FPG2 FPG3=FPG3 FPG4=FPG4 NOEU=NOEU GAUSS=FPG3  SIMP=SIMP COTES=COTES SIMP1=SIMP1 COTES1=COTES1 COTES2=COTES2
   ELREFE__  SE2       GAUSS__  RIGI=FPG2 MASS=FPG3 FPG2=FPG2 FPG3=FPG3 FPG4=FPG4 NOEU=NOEU GAUSS=FPG3  SIMP=SIMP COTES=COTES SIMP1=SIMP1 COTES1=COTES1 COTES2=COTES2
   ATTRIBUT__  XFEM=XHC XLAG=NOEUD
   ENS_NOEUD__  EN3     =     5   6   7   8
   ENS_NOEUD__  EN1     =     1   2   3   4

MODE_LOCAL__
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        W        )
    ENORME   = NEUT_R   ELEM__         (X1       )
    EMNEUT_R = NEUT_R   ELEM__         (X[30]     )
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CCAMASS  = CAMASS   ELEM__         (C        ALPHA    )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC   PERTURB
                                        TOLDEBO  ITEDEBO  TSSEUIL TSAMPL TSRETOUR   )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1
                                        XXXX2    KIT[9])
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CONTX_R  = XCONTAC  ELEM__         (RHON     MU       RHOTK    INTEG   COECH    COSTCO   COSTFR
                                        COPECO   COPEFR   RELA)
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    CPRESSF  = PRES_F   ELEM__         (PRES     CISA    )
    CROTATR  = ROTA_R   ELEM__         (OME      AR       BR       CR       X        Y
                                        Z        )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    CTYPEPI  = PILO_K   ELEM__         (TYPE     )
    CBORNPI  = PILO_R   ELEM__         (A0       A1       )
    CCDTAU   = PILO_R   ELEM__         (A0       )
    ECOPILO  = PILO_R   ELGA__ XFEM    (A0       A1       A2       A3       ETA      )
    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN1    (DX     DY      H1X      H1Y
                                         LAGS_C  LAGS_F1  )
                                 EN3    (DX     DY      H1X      H1Y  )
    DDL_MECC = DEPL_R   ELNO__ IDEN__  (DX      DY      )
    DDL_NOZ1 = SIZZ_R   ELNO__ IDEN__  (SIZZ     )
    E1NEUTR  = NEUT_R   ELEM__         (X1       )
    E2NEUTI  = NEUT_I   ELEM__         (X1       X2      )
    E3NEUTI  = N120_I   ELEM__         (X[3]     )
    E6NEUTI  = N128_I   ELEM__         (X[6]     )
    E9NEUTI  = N120_I   ELEM__         (X[9]     )
    E36NEUI  = N512_I   ELEM__         (X[36]    )
    E2NEUTR  = NEUT_R   ELEM__         (X[2]     )
    E3NEUTR  = NEUT_R   ELEM__         (X[3]     )
    E4NEUTR  = NEUT_R   ELEM__         (X[4]     )
    E6NEUTR  = N120_R   ELEM__         (X[6]     )
    E06NEUR  = N132_R   ELEM__         (X[6]     )
    E08NEUR  = N120_R   ELEM__         (X[8]     )
    E10NEUTR = NEUT_R   ELEM__         (X[10]    )
    E12NEUTR = N120_R   ELEM__         (X[12]    )
    E15NEUTR = N120_R   ELEM__         (X[15]    )
    E20NEUTR = NEUT_R   ELEM__         (X[20]    )
    E22NEUTR = N480_R   ELEM__         (X[22]    )
    ECODRET  = CODE_I   ELEM__         (IRET     )
    ECONTPG  = SIEF_R   ELGA__ XFEM    (SIXX     SIYY     SIZZ     SIXY     )
    ECONTPC  = SIEF_C   ELGA__ XFEM    (SIXX     SIYY     SIZZ     SIXY     )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     )
    ECONTNC  = SIEF_C   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    EDEFONO  = EPSI_R   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     )
    EDOMGGA  = DOMA_R   ELGA__ RIGI    (DOMA     )
    EGGEOM_R = GEOM_R   ELGA__ XFEM    (X        Y        )
    EGINST_R = INST_R   ELGA__ RIGI    (INST     )
    EGNEUT_F = NEUT_F   ELGA__ XFEM    (X[30])
    EGNEUT_R = NEUT_R   ELGA__ XFEM    (X[30])
    EKTHETA  = G        ELEM__         (GTHETA   FIC1     FIC2     K1       K2       )
    EGTHETA  = G        ELEM__         (GTHETA   )
    EPRESNO  = PRES_R   ELNO__ IDEN__  (PRES     CISA     )
    I1NEUT_I = NEUT_I   ELEM__         (X1       )
    N1NEUT_R = NEUT_R   ELNO__ IDEN__  (X1       )
    N2NEUT_R = NEUT_R   ELNO__ IDEN__  (X1       X2       )
    N6NEUT_R = NEUT_R   ELNO__ IDEN__  (X[6])
    NDEPLAC  = DEPL_C   ELNO__ IDEN__  (DX       DY       )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        )
    CGEOMER  = GEOM_R   ELEM__         (X        Y        )
    CFORCEF  = FORC_F   ELEM__         (FX       FY       )
    NFORCER  = FORC_R   ELNO__ IDEN__  (FX       FY       )
    EFORCER  = FORC_R   ELGA__ XFEM    (FX       FY       )
    STANO_I  = N120_I   ELNO__ IDEN__  (X1       )
    XFGEOM_R = GEOM_R   ELGA__ XFEM    (X        Y        )
    ZVARIPG  = VARI_R   ELGA__ XFEM    (VARI     )
    E1NEUTK  = NEUT_K8  ELEM__         (Z1       )
    CFREQR   = FREQ_R   ELEM__         (FREQ     )
    EREFCO   = PREC     ELEM__         (SIGM     DEPL)

VECTEUR__
    MVECTUC = VDEP_C NDEPLAC
    MVECTUR = VDEP_R DDL_MECA
    MVECZZR = VSIZ_R DDL_NOZ1

MATRICE__
    MMATUUC = MDEP_C NDEPLAC NDEPLAC
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    MMATUNS = MDNS_R DDL_MECA DDL_MECA
    MMATZZR = MSIZ_R DDL_NOZ1 DDL_NOZ1

OPTION__
    CALC_G           288  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  N6NEUT_R PBASLOR  E3NEUTI  PCFACE   
                                E36NEUI  PCNSETO  CCOMPOR  PCOMPOR  DDL_MECA PDEPLAR  NFORCER  PFRVOLU  
                                NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  E3NEUTI  PLONGCO  
                                N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  CPESANR  PPESANR  
                                E6NEUTR  PPINTER  E06NEUR  PPINTTO  E22NEUTR PPMILTO  EPRESNO  PPRESSR  
                                CROTATR  PROTATR  DDL_MECC PTHETAR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                              OUT__  EGTHETA  PGTHETA
    CALC_G_F         288  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  N6NEUT_R PBASLOR  E3NEUTI  PCFACE   
                                E36NEUI  PCNSETO  CCOMPOR  PCOMPOR  DDL_MECA PDEPLAR  CFORCEF  PFFVOLU  
                                NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  E3NEUTI  PLONGCO  
                                N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  CPESANR  PPESANR  
                                E6NEUTR  PPINTER  E06NEUR  PPINTTO  E22NEUTR PPMILTO  CPRESSF  PPRESSF  
                                CROTATR  PROTATR  CTEMPSR  PTEMPSR  DDL_MECC PTHETAR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  
                              OUT__  EGTHETA  PGTHETA
    CALC_GTP         288  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  N6NEUT_R PBASLOR  E3NEUTI  PCFACE   
                                E36NEUI  PCNSETO  CCOMPOR  PCOMPOR  DDL_MECA PDEPLAR  NFORCER  PFRVOLU  
                                NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  E3NEUTI  PLONGCO  
                                N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  CPESANR  PPESANR  
                                E6NEUTR  PPINTER  E06NEUR  PPINTTO  E22NEUTR PPMILTO  EPRESNO  PPRESSR  
                                CROTATR  PROTATR  DDL_MECC PTHETAR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                              OUT__  EGTHETA  PGTHETA
    CALC_GTP_F       288  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  N6NEUT_R PBASLOR  E3NEUTI  PCFACE   
                                E36NEUI  PCNSETO  CCOMPOR  PCOMPOR  DDL_MECA PDEPLAR  CFORCEF  PFFVOLU  
                                NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  E3NEUTI  PLONGCO  
                                N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  CPESANR  PPESANR  
                                E6NEUTR  PPINTER  E06NEUR  PPINTTO  E22NEUTR PPMILTO  CPRESSF  PPRESSF  
                                CROTATR  PROTATR  CTEMPSR  PTEMPSR  DDL_MECC PTHETAR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  
                              OUT__  EGTHETA  PGTHETA                              
    CALC_K_G         297  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  N6NEUT_R PBASLOR  E3NEUTI  PCFACE   
                                E36NEUI  PCNSETO  CCOMPOR  PCOMPOR  DDL_MECA PDEPLAR  NFORCER  PFRVOLU  
                                NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  E3NEUTI  PLONGCO  
                                N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  CPESANR  PPESANR  
                                E6NEUTR  PPINTER  E06NEUR  PPINTTO  E22NEUTR PPMILTO  EPRESNO  PPRESSR  
                                CFREQR   PPULPRO  CROTATR  PROTATR  DDL_MECC PTHETAR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  
                              OUT__  EKTHETA  PGTHETA
    CALC_K_G_F       297  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  N6NEUT_R PBASLOR  E3NEUTI  PCFACE   
                                E36NEUI  PCNSETO  CCOMPOR  PCOMPOR  DDL_MECA PDEPLAR  CFORCEF  PFFVOLU  
                                NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  E3NEUTI  PLONGCO  
                                N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  CPESANR  PPESANR  
                                E6NEUTR  PPINTER  E06NEUR  PPINTTO  E22NEUTR PPMILTO  CPRESSF  PPRESSF  
                                CFREQR   PPULPRO  CROTATR  PROTATR  CTEMPSR  PTEMPSR  DDL_MECC PTHETAR  
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                              OUT__  EKTHETA  PGTHETA
    CARA_GEOM         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CFL_XFEM            118   IN__   NGEOMER  PGEOMER
                              OUT__  E1NEUTR  PLONCAR
    CHAR_LIMITE       -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_CONT   534  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  E3NEUTI  PCFACE   E3NEUTR  PCOHES   
                                DDL_MECA PDEPL_M  DDL_MECA PDEPL_P  CONTX_R  PDONCO   NGEOMER  PGEOMER  
                                I1NEUT_I PINDCOI  E3NEUTI  PLONGCO  N1NEUT_R PLST     CMATERC  PMATERC  
                                E6NEUTR  PPINTER  E1NEUTR  PSEUIL   STANO_I  PSTANO   
                          OUT__ MVECTUR  PVECTUR  
    CHAR_MECA_EPSA_R  -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_FF2D2D    440   IN__   CFORCEF  PFF2D2D  NGEOMER  PGEOMER  CTEMPSR  PTEMPSR
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_FR2D2D 440  IN__  E36NEUI  PCNSETO  NFORCER  PFR2D2D  NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  
                                E9NEUTI  PLONCHA  N1NEUT_R PLSN     N1NEUT_R PLST     E06NEUR  PPINTTO  
                                E22NEUTR PPMILTO  STANO_I  PSTANO   
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_FROT   534  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  E3NEUTI  PCFACE   E3NEUTR  PCOHES   
                                DDL_MECA PDEPL_M  DDL_MECA PDEPL_P  CONTX_R  PDONCO   NGEOMER  PGEOMER  
                                I1NEUT_I PINDCOI  E3NEUTI  PLONGCO  N1NEUT_R PLST     CMATERC  PMATERC  
                                E6NEUTR  PPINTER  E1NEUTR  PSEUIL   STANO_I  PSTANO   
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_HYDR_R  -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_META_Z  -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_PESA_R 441  IN__  E36NEUI  PCNSETO  NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  
                                N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  CPESANR  PPESANR  
                                E06NEUR  PPINTTO  E22NEUTR PPMILTO  STANO_I  PSTANO   ZVARCPG  PVARCPR  
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_PRES_F  37  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  E3NEUTI  PCFACE   NGEOMER  PGEOMER  
                                E3NEUTI  PLONGCO  N1NEUT_R PLST     E6NEUTR  PPINTER  CPRESSF  PPRESSF  
                                STANO_I  PSTANO   CTEMPSR  PTEMPSR  
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_PRES_R  37  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  E3NEUTI  PCFACE   NGEOMER  PGEOMER  
                                E3NEUTI  PLONGCO  N1NEUT_R PLST     E6NEUTR  PPINTER  EPRESNO  PPRESSR  
                                STANO_I  PSTANO   
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_PTOT_R  -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_ROTA_R 441  IN__  E36NEUI  PCNSETO  NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  
                                N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  E06NEUR  PPINTTO  
                                E22NEUTR PPMILTO  CROTATR  PROTATR  STANO_I  PSTANO   
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_SECH_R  -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_TEMP_R  -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    COOR_ELGA           479   IN__   NGEOMER  PGEOMER
                              OUT__  EGGEOP_R PCOORPG
    ECIN_ELEM         -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    ENEL_ELEM         -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    ENER_TOTALE       -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    EPOT_ELEM         -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    FORC_NODA        542  IN__  N6NEUT_R PBASLOR  E36NEUI  PCNSETO  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  
                                DDL_MECA PDEPLMR  NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  
                                N1NEUT_R PLSN     N1NEUT_R PLST     E06NEUR  PPINTTO  E22NEUTR PPMILTO  
                                STANO_I  PSTANO   ZVARCPG  PVARCPR  
                              OUT__  MVECTUR  PVECTUR
    FULL_MECA        539  IN__  N6NEUT_R PBASLOR  CCAMASS  PCAMASS  CCARCRI  PCARCRI  E36NEUI  PCNSETO  
                                CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  
                                NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                E9NEUTI  PLONCHA  N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  
                                E06NEUR  PPINTTO  E22NEUTR PPMILTO  STANO_I  PSTANO   ZVARCPG  PVARCMR  
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARIPG  PVARIMP  ZVARIPG  PVARIMR  
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUNS  PMATUNS  MMATUUR  PMATUUR
                                     ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    GEOM_FAC         519  IN__  E1NEUTK  NOMFIS   DDL_MECA PDEPLA   E6NEUTR  PGESCLO  E3NEUTI  PLONGCO  
                                N1NEUT_R PLST     E6NEUTR  PPINTER  
                          OUT__ E08NEUR  PBASESC  E08NEUR  PBASMAI  E6NEUTR  PNEWGEM  E6NEUTR  PNEWGES  
    GRAD_NEUT_R         24    IN__   NGEOMER  PGEOMER  N1NEUT_R PNEUTER
                              OUT__  N2NEUT_R PGNEUTR
    INDIC_ENER        -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    INDIC_SEUIL       -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    INIT_VARC         99  IN__  
                          OUT__ ZVARCPG  PVARCPR  
    MASS_INER         -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    MOY_NOEU_S          118   IN__   N1NEUT_R PNEUTR
                              OUT__  E1NEUTR  PMOYEL
    NORME_L2          -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    NSPG_NBVA           496   IN__   CCOMPO2  PCOMPOR
                              OUT__  EDCEL_I  PDCEL_I
    PILO_PRED_XLAS   547  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  CCDTAU   PCDTAU   E3NEUTI  PCFACE   
                                E3NEUTR  PCOHES   DDL_MECA PDDEPLR  DDL_MECA PDEPL0R  DDL_MECA PDEPL1R  
                                DDL_MECA PDEPLMR  CONTX_R  PDONCO   NGEOMER  PGEOMER  I1NEUT_I PINDCOI  
                                E3NEUTI  PLONGCO  N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  
                                E6NEUTR  PPINTER  
                              OUT__  ECOPILO  PCOPILO
    RAPH_MECA        539  IN__  N6NEUT_R PBASLOR  CCAMASS  PCAMASS  CCARCRI  PCARCRI  E36NEUI  PCNSETO  
                                CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  
                                NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                E9NEUTI  PLONCHA  N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  
                                E06NEUR  PPINTTO  E22NEUTR PPMILTO  STANO_I  PSTANO   ZVARCPG  PVARCMR  
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARIPG  PVARIMP  ZVARIPG  PVARIMR  
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    REFE_FORC_NODA   542  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  N6NEUT_R PBASLOR  E3NEUTI  PCFACE   
                                E36NEUI  PCNSETO  CCOMPOR  PCOMPOR  DDL_MECA PDEPLMR  NGEOMER  PGEOMER  
                                E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  E3NEUTI  PLONFA   N1NEUT_R PLSN     
                                N1NEUT_R PLST     E6NEUTR  PPINTER  E06NEUR  PPINTTO  E22NEUTR PPMILTO  
                                EREFCO   PREFCO   
                              OUT__  MVECTUR  PVECTUR
    REPERE_LOCAL     133  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER  
                          OUT__ CGEOMER  PREPLO1  CGEOMER  PREPLO2  
    RICE_TRACEY       -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    RIGI_CONT        533  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  E3NEUTI  PCFACE   E3NEUTR  PCOHES   
                                DDL_MECA PDEPL_M  DDL_MECA PDEPL_P  CONTX_R  PDONCO   NGEOMER  PGEOMER  
                                I1NEUT_I PINDCOI  E3NEUTI  PLONGCO  N1NEUT_R PLSN     N1NEUT_R PLST     
                                CMATERC  PMATERC  E6NEUTR  PPINTER  E1NEUTR  PSEUIL   STANO_I  PSTANO   
                          OUT__ E3NEUTR  PCOHESO  MMATUNS  PMATUNS  MMATUUR  PMATUUR  
    RIGI_FROT        533  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  E3NEUTI  PCFACE   E3NEUTR  PCOHES   
                                DDL_MECA PDEPL_M  DDL_MECA PDEPL_P  CONTX_R  PDONCO   NGEOMER  PGEOMER  
                                I1NEUT_I PINDCOI  E3NEUTI  PLONGCO  N1NEUT_R PLSN     N1NEUT_R PLST     
                                CMATERC  PMATERC  E6NEUTR  PPINTER  E1NEUTR  PSEUIL   STANO_I  PSTANO   
                          OUT__ MMATUNS  PMATUNS  MMATUUR  PMATUUR  
    RIGI_MECA         81  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  
                              OUT__  MMATUUR  PMATUUR
    RIGI_MECA_TANG   539  IN__  N6NEUT_R PBASLOR  CCAMASS  PCAMASS  CCARCRI  PCARCRI  E36NEUI  PCNSETO  
                                CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  
                                NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                E9NEUTI  PLONCHA  N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  
                                E06NEUR  PPINTTO  E22NEUTR PPMILTO  STANO_I  PSTANO   ZVARCPG  PVARCMR  
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARIPG  PVARIMR  
                              OUT__  MMATUNS  PMATUNS  MMATUUR  PMATUUR
    SIEQ_ELGA         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    SIEQ_ELNO         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    SIGM_ELGA           546   IN__   ECONTPG  PSIEFR
                          OUT__ ECONTPC  PSIGMC   ECONTPG  PSIGMR   
    SIGM_ELNO         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    SYME_MDNS_R         222   IN__   MMATUNS  PNOSYM
                              OUT__  MMATUUR  PSYM
    TOPOFA           510  IN__  NGEOMER  PGEOMER  N2NEUT_R PGRADLN  N2NEUT_R PGRADLT  N1NEUT_R PLSN     
                                N1NEUT_R PLST     
                          OUT__ E15NEUTR PAINTER  E12NEUTR PBASECO  E3NEUTI  PCFACE   E6NEUTR  PGESCLA  
                                E6NEUTR  PGESCLO  E6NEUTR  PGMAITR  E3NEUTI  PLONGCO  E6NEUTR  PPINTER  
    TOPOSE           514  IN__  NGEOMER  PGEOMER  N1NEUT_R PLEVSET  
                          OUT__ E36NEUI  PCNSETO  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  E06NEUR  PPINTTO  
                                     E22NEUTR PPMILTO
    TOU_INI_ELGA        99    IN__
                              OUT__  EDOMGGA  PDOMMAG  EGGEOM_R PGEOM_R  EGINST_R PINST_R  EGNEUT_F PNEUT_F
                                     EGNEUT_R PNEUT_R  ECONTPG  PSIEF_R  ZVARIPG  PVARI_R
    VERI_JACOBIEN       328   IN__   NGEOMER  PGEOMER
                              OUT__  ECODRET  PCODRET
    WEIBULL           -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    XCVBCA           532  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  E3NEUTI  PCFACE   E3NEUTR  PCOHES   
                                DDL_MECA PDEPL_P  CONTX_R  PDONCO   NGEOMER  PGEOMER  I1NEUT_I PGLISS   
                                I1NEUT_I PINDCOI  E3NEUTI  PLONGCO  N1NEUT_R PLST     CMATERC  PMATERC  
                                I1NEUT_I PMEMCON  E6NEUTR  PPINTER  
                          OUT__ E3NEUTR  PCOHESO  I1NEUT_I PINCOCA  I1NEUT_I PINDCOO  I1NEUT_I PINDMEM  
    XFEM_SMPLX_CALC  118  IN__  E1NEUTR  PGRANDF  N2NEUT_R PGRLS    N1NEUT_R PLSNO    N2NEUT_R PNIELNO  
                          OUT__ N1NEUT_R PALPHA   E1NEUTR  PDPHI    
    XFEM_SMPLX_INIT     118   IN__   NGEOMER  PGEOMER
                              OUT__  E1NEUTR  PMEAST   N2NEUT_R PNIELNO
    XFEM_XPG          46  IN__  E36NEUI  PCNSETO  NGEOMER  PGEOMER  E6NEUTI  PHEAVTO  E9NEUTI  PLONCHA  
                                E06NEUR  PPINTTO  E22NEUTR PPMILTO  
                              OUT__  XFGEOM_R PXFGEOM
    XREACL           548  IN__  E15NEUTR PAINTER  E12NEUTR PBASECO  E3NEUTI  PCFACE   DDL_MECA PDEPL_P  
                                CONTX_R  PDONCO   NGEOMER  PGEOMER  E3NEUTI  PLONGCO  N1NEUT_R PLST     
                                E6NEUTR  PPINTER  
                              OUT__  E1NEUTR  PSEUIL
