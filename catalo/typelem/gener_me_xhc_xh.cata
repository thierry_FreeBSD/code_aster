%& LIBRARY TYPELEM
% ======================================================================
% COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
%
GENER_ME_XHC_XH
TYPE_GENE__

%  CATALOGUES DES ELEMENTS X-FEM HEAVISIDE AVEC CONTACT GRANDS GLISSEMENTS


ENTETE__ ELEMENT__ ME3DH8HH8H_XH           MAILLE__ HE8HE8
   ELREFE__  HE8     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=H  XFEM_M=H
   ENS_NOEUD__  EN2   =  9  10  11  12  13  14  15  16
   ENS_NOEUD__  EN1   =  1   2   3   4   5   6   7   8
ENTETE__ ELEMENT__ ME3DH8HH8C_XH           MAILLE__ HE8HE8
   ELREFE__  HE8     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=H  XFEM_M=C
   ENS_NOEUD__  EN4   =  9  10  11  12  13  14  15  16
   ENS_NOEUD__  EN1   =  1   2   3   4   5   6   7   8
ENTETE__ ELEMENT__ ME3DH8CH8H_XH           MAILLE__ HE8HE8
   ELREFE__  HE8     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=C  XFEM_M=H
   ENS_NOEUD__  EN2   =  9  10  11  12  13  14  15  16
   ENS_NOEUD__  EN3   =  1   2   3   4   5   6   7   8
ENTETE__ ELEMENT__ ME3DH8CH8C_XH           MAILLE__ HE8HE8
   ELREFE__  HE8     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=C  XFEM_M=C
   ENS_NOEUD__  EN4   =  9  10  11  12  13  14  15  16
   ENS_NOEUD__  EN3   =  1   2   3   4   5   6   7   8
ENTETE__ ELEMENT__ ME3DH8T_XH              MAILLE__ HEXA8
   ELREFE__  HE8     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=T  XFEM_M=T
   ENS_NOEUD__  EN5   =  1   2   3   4   5   6   7   8


ENTETE__ ELEMENT__ ME3DP6HP6H_XH           MAILLE__ PE6PE6
   ELREFE__  PE6     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=H  XFEM_M=H
   ENS_NOEUD__  EN2   =  7   8   9  10  11  12
   ENS_NOEUD__  EN1   =  1   2   3   4   5   6
ENTETE__ ELEMENT__ ME3DP6HP6C_XH           MAILLE__ PE6PE6
   ELREFE__  PE6     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=H  XFEM_M=C
   ENS_NOEUD__  EN4   =  7   8   9  10  11  12
   ENS_NOEUD__  EN1   =  1   2   3   4   5   6
ENTETE__ ELEMENT__ ME3DP6CP6H_XH           MAILLE__ PE6PE6
   ELREFE__  PE6     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=C  XFEM_M=H
   ENS_NOEUD__  EN2   =  7   8   9  10  11  12
   ENS_NOEUD__  EN3   =  1   2   3   4   5   6
ENTETE__ ELEMENT__ ME3DP6CP6C_XH           MAILLE__ PE6PE6
   ELREFE__  PE6     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=C  XFEM_M=C
   ENS_NOEUD__  EN4   =  7   8   9  10  11  12
   ENS_NOEUD__  EN3   =  1   2   3   4   5   6
ENTETE__ ELEMENT__ ME3DP6T_XH              MAILLE__ PENTA6
   ELREFE__  PE6     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=T  XFEM_M=T
   ENS_NOEUD__  EN5   =  1   2   3   4   5   6


ENTETE__ ELEMENT__ ME3DT4HT4H_XH           MAILLE__ TE4TE4
   ELREFE__  TE4     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=H  XFEM_M=H
   ENS_NOEUD__  EN2   =  5   6   7   8
   ENS_NOEUD__  EN1   =  1   2   3   4
ENTETE__ ELEMENT__ ME3DT4HT4C_XH           MAILLE__ TE4TE4
   ELREFE__  TE4     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=H  XFEM_M=C
   ENS_NOEUD__  EN4   =  5   6   7   8
   ENS_NOEUD__  EN1   =  1   2   3   4
ENTETE__ ELEMENT__ ME3DT4CT4H_XH           MAILLE__ TE4TE4
   ELREFE__  TE4     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=C  XFEM_M=H
   ENS_NOEUD__  EN2   =  5   6   7   8
   ENS_NOEUD__  EN3   =  1   2   3   4
ENTETE__ ELEMENT__ ME3DT4CT4C_XH           MAILLE__ TE4TE4
   ELREFE__  TE4     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=C  XFEM_M=C
   ENS_NOEUD__  EN4   =  5   6   7   8
   ENS_NOEUD__  EN3   =  1   2   3   4
ENTETE__ ELEMENT__ ME3DT4T_XH              MAILLE__ TETRA4
   ELREFE__  TE4     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=T  XFEM_M=T
   ENS_NOEUD__  EN5   =  1   2   3   4
% debut des modifs ajout des elements quadratiques
% on ne garde que H-H C:heav+ctip T:ctip (el du fond)
ENTETE__ ELEMENT__ ME3DHVHHVH_XH           MAILLE__ H20H20
   ELREFE__  H20     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=H  XFEM_M=H
   ENS_NOEUD__  EN2   =  9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36  37  38  39  40
   ENS_NOEUD__  EN1   =  1   2   3   4   5   6   7   8
ENTETE__ ELEMENT__ ME3DPQHPQH_XH           MAILLE__ P15P15
   ELREFE__  P15     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=H  XFEM_M=H
   ENS_NOEUD__  EN2   =  7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  24  25  26  27  28  29  30
   ENS_NOEUD__  EN1   =  1   2   3   4   5   6
ENTETE__ ELEMENT__ ME3DTDHTDH_XH           MAILLE__ T10T10
   ELREFE__  T10     GAUSS__  NOEU=NOEU
   ELREFE__  TR3     GAUSS__  NOEU=NOEU
   ATTRIBUT__   XFEM_E=H  XFEM_M=H
   ENS_NOEUD__  EN2   =  5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20
   ENS_NOEUD__  EN1   =  1   2   3   4
%

MODE_LOCAL__

    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN1   (DX      DY      DZ      H1X      H1Y      H1Z
                                        LAGS_C   LAGS_F1  LAGS_F2  )
                                 EN2   (DX      DY      DZ      H1X      H1Y      H1Z   )
                                 EN3   (DX      DY      DZ      H1X      H1Y      H1Z
                                        E1X      E1Y      E1Z      LAGS_C   LAGS_F1  LAGS_F2)
                                 EN4   (DX      DY      DZ      H1X      H1Y      H1Z
                                        E1X      E1Y      E1Z      )
                                 EN5   (E1X      E1Y      E1Z      LAGS_C   LAGS_F1  LAGS_F2)

    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        Z        )

    CCONPT   = N120_R   ELEM__         (X[34])

    CCONPI   = NEUT_R   ELEM__         (X[18])

    CCONAI   = NEUT_R   ELEM__         (X[30])

    CCONCF   = NEUT_I   ELEM__         (X[15])

    I3NEUT_I = NEUT_I   ELEM__         (X[3] )

    STANO_I  = N120_I   ELEM__         (X[40])

VECTEUR__
    MVECTUR = VDEP_R DDL_MECA
MATRICE__
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    MMATUNS = MDNS_R DDL_MECA DDL_MECA

OPTION__
    CHAR_MECA_CONT   367  IN__  CCONAI   PCAR_AI  CCONCF   PCAR_CF  CCONPI   PCAR_PI  CCONPT   PCAR_PT  
                                DDL_MECA PDEPL_M  DDL_MECA PDEPL_P  NGEOMER  PGEOMER  STANO_I  PSTANO   
                          OUT__ MVECTUR  PVECTUR  
    CHAR_MECA_FROT   367  IN__  CCONAI   PCAR_AI  CCONCF   PCAR_CF  CCONPI   PCAR_PI  CCONPT   PCAR_PT  
                                DDL_MECA PDEPL_M  DDL_MECA PDEPL_P  NGEOMER  PGEOMER  STANO_I  PSTANO   
                          OUT__ MVECTUR  PVECTUR  
    RIGI_CONT        366  IN__  CCONAI   PCAR_AI  CCONCF   PCAR_CF  CCONPI   PCAR_PI  CCONPT   PCAR_PT  
                                DDL_MECA PDEPL_M  DDL_MECA PDEPL_P  NGEOMER  PGEOMER  STANO_I  PSTANO   
                          OUT__ MMATUNS  PMATUNS  MMATUUR  PMATUUR  
    RIGI_FROT        366  IN__  CCONAI   PCAR_AI  CCONCF   PCAR_CF  CCONPI   PCAR_PI  CCONPT   PCAR_PT  
                                DDL_MECA PDEPL_M  DDL_MECA PDEPL_P  NGEOMER  PGEOMER  STANO_I  PSTANO   
                          OUT__ MMATUNS  PMATUNS  MMATUUR  PMATUUR  
    SYME_MDNS_R      222  IN__  MMATUNS  PNOSYM   
                          OUT__ MMATUUR  PSYM     
    XCVBCA           363  IN__  CCONAI   PCAR_AI  CCONPT   PCAR_PT  DDL_MECA PDEPL_P  NGEOMER  PGEOMER  
                          OUT__ I3NEUT_I PINDCOO
