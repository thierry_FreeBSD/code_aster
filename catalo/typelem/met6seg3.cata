%& LIBRARY TYPELEM
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
MET6SEG3

% les catalogues MET3SEG3, MET3SEG4 et MET6SEG3 sont identiques mise a part :
% * l'entete de l'element
% * le mode local CCAORIE  (pour MET3SEG4)
% * les mode locaux NDEPLAC et DDL_MECA (pour MET6SEG3)


TYPE_ELEM__
ENTETE__ ELEMENT__ MET6SEG3         MAILLE__ SEG3
   ELREFE__  SE3       GAUSS__  RIGI=FPG3  MASS=FPG3 FPG1=FPG1 FPG_LISTE__  MATER = (RIGI FPG1)

MODE_LOCAL__
    CABSCUR  = ABSC_R   ELEM__         (ABSC1    ABSC2   ABSC3 )
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CCAGEPO  = CAGEPO   ELEM__         (R1       EP1      )
    CCAMASS  = CAMASS   ELEM__         (C        ALPHA    BETA     KAPPA    X        Y
                                        Z        )
    CCAORIE  = CAORIE   ELEM__         (ALPHA    BETA     GAMMA    ALPHA2   BETA2    GAMMA2
                                        ALPHA3   BETA3    GAMMA3   ICOUDE   DN1N2    RCOURB
                                        ANGCOU   ANGZZK   )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC   PERTURB
                                        TOLDEBO  ITEDEBO  TSSEUIL  TSAMPL   TSRETOUR )
    ECODRET  = CODE_I   ELEM__         (IRET     )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1
                                        XXXX2    )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    NDEPLAC  = DEPL_C   ELNO__ IDEN__  (DX       DY       DZ       DRX      DRY      DRZ
                                        UI2      VI2      WI2      UI3      VI3      WI3
                                        UI4      VI4      WI4      UI5      VI5      WI5
                                        UI6      VI6      WI6      UO2      VO2      WO2
                                        UO3      VO3      WO3      UO4      VO4      WO4
                                        UO5      VO5      WO5      UO6      VO6      WO6
                                        WO       WI1      WO1      )
    DDL_MECA = DEPL_R   ELNO__ IDEN__  (DX       DY       DZ       DRX      DRY      DRZ
                                        UI2      VI2      WI2      UI3      VI3      WI3
                                        UI4      VI4      WI4      UI5      VI5      WI5
                                        UI6      VI6      WI6      UO2      VO2      WO2
                                        UO3      VO3      WO3      UO4      VO4      WO4
                                        UO5      VO5      WO5      UO6      VO6      WO6
                                        WO       WI1      WO1      )
    ECOURAN  = NEUT_R   ELEM__         (X1       )
    EDEFONO  = EPSI_R   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDEFONC  = EPSI_C   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDEFOPG  = EPSI_R   ELGA__ RIGI    (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDEFOPC  = EPSI_C   ELGA__ RIGI    (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDFEQPG  = EPSI_R   ELGA__ RIGI    (INVA_2   PRIN_1   PRIN_2   PRIN_3   INVA_2SG VECT_1_X
                                        VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z VECT_3_X
                                        VECT_3_Y VECT_3_Z )
    EDFEQNO  = EPSI_R   ELNO__ IDEN__  (INVA_2   PRIN_1   PRIN_2   PRIN_3   INVA_2SG VECT_1_X
                                        VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z VECT_3_X
                                        VECT_3_Y VECT_3_Z )
    EDEFGNO  = EPSI_R   ELNO__ IDEN__  (EPXX     GAXY     GAXZ     GAT      KY       KZ       )
    EDEFGPG  = EPSI_R   ELGA__ RIGI    (EPXX     GAXY     GAXZ     GAT      KY       KZ       )
    CFORCEC  = FORC_C   ELEM__         (FX       FY       FZ       MX       MY       MZ
                                        REP      )
    CFORCEF  = FORC_F   ELEM__         (FX       FY       FZ       MX       MY       MZ
                                        REP      )
    CFORCER  = FORC_R   ELEM__         (FX       FY       FZ       MX       MY       MZ
                                        REP      )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        Z        )
    CGEOMER  = GEOM_R   ELEM__         (X        Y        Z        )
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        Z        W       )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    EMASSINE = MASS_R   ELEM__         (M        CDGX     CDGY     CDGZ     IXX      IYY
                                        IZZ      IXY      IXZ      IYZ      )
    ENBSP_I  = NBSP_I   ELEM__         (TUY_NCOU TUY_NSEC )
    ENOMCMP  = NEUT_K24 ELEM__         (Z1       Z2       )
    CNUMCOR  = NUMC_I   ELEM__         (NUMC     ORDO     ANGL     )
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    EREFCO   = PREC     ELEM__         (SIGM     )
    EPRESNO  = PRES_R   ELNO__ IDEN__  (PRES     )
    CPRESSF  = PRES_F   ELEM__         (PRES     )
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[30]    )
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[30]    )
    ENONLIN  = NEUT_I   ELEM__         (X1       )
    ECONTPG  = SIEF_R   ELGA__ RIGI    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTPC  = SIEF_C   ELGA__ RIGI    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTNC  = SIEF_C   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    EEFGENOR = SIEF_R   ELNO__ IDEN__  (N        VY       VZ       MT       MFY      MFZ      )
    EEFGENOC = SIEF_C   ELNO__ IDEN__  (N        VY       VZ       MT       MFY      MFZ      )
    EEFGEGAR = SIEF_R   ELGA__ RIGI    (N        VY       VZ       MT       MFY      MFZ      )
    EEFGEGAC = SIEF_C   ELGA__ RIGI    (N        VY       VZ       MT       MFY      MFZ      )
    ESIGMNOR = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECOEQPG  = SIEF_R   ELGA__ RIGI    (VMIS     TRESCA   PRIN_1   PRIN_2   PRIN_3   VMIS_SG
                                        VECT_1_X VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z
                                        VECT_3_X VECT_3_Y VECT_3_Z TRSIG    TRIAX )
    ECOEQNO  = SIEF_R   ELNO__ IDEN__  (VMIS     TRESCA   PRIN_1   PRIN_2   PRIN_3   VMIS_SG
                                        VECT_1_X VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z
                                        VECT_3_X VECT_3_Y VECT_3_Z TRSIG    TRIAX )
    EGAMIMA  = SPMX_R   ELGA__ RIGI    (VAL      NUCOU    NUSECT   NUFIBR   POSIC    POSIS    )
    ENOMIMA  = SPMX_R   ELNO__ IDEN__  (VAL      NUCOU    NUSECT   NUFIBR   POSIC    POSIS    )
    ZVARIPG  = VARI_R   ELGA__ RIGI    (VARI     )
    ZVARINO  = VARI_R   ELNO__ IDEN__  (VARI     )

    ENNEUT_F = NEUT_F   ELNO__ IDEN__  (X[30]    )
    ENNEUT_R = NEUT_R   ELNO__ IDEN__  (X[30]    )

VECTEUR__
    MVECTUC = VDEP_C NDEPLAC
    MVECTUR = VDEP_R DDL_MECA

MATRICE__
    MMATUUC = MDEP_C NDEPLAC NDEPLAC
    MMATUUR = MDEP_R DDL_MECA DDL_MECA

OPTION__
    AMOR_MECA         50  IN__  NGEOMER  PGEOMER  MMATUUR  PMASSEL  CMATERC  PMATERC  MMATUUR  PRIGIEL
                                ZVARCPG  PVARCPR
                          OUT__ MMATUUR  PMATUUR
    CARA_GEOM         -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_LIMITE       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_EPSI_R  -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_FC1D1D 583  IN__  CCAORIE  PCAORIE  CFORCEC  PFC1D1D  NGEOMER  PGEOMER
                          OUT__ MVECTUC  PVECTUC
    CHAR_MECA_FF1D1D 583  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  CFORCEF  PFF1D1D  NGEOMER  PGEOMER
                                ENBSP_I  PNBSP_I  CTEMPSR  PTEMPSR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_FR1D1D 583  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  CFORCER  PFR1D1D  NGEOMER  PGEOMER
                                ENBSP_I  PNBSP_I
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_FRELEC  -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_FRLAPL  -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_PESA_R 583  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                ENBSP_I  PNBSP_I  CPESANR  PPESANR  ZVARCPG  PVARCPR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_PRES_F 583  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  ENBSP_I  PNBSP_I
                                CPRESSF  PPRESSF  CTEMPSR  PTEMPSR  CABSCUR  PABSCUR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_PRES_R 583  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  ENBSP_I  PNBSP_I
                                EPRESNO  PPRESSR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_ROTA_R  -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_SF1D1D  -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_SR1D1D  -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_TEMP_R 589  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                ENBSP_I  PNBSP_I  CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                          OUT__ MVECTUR  PVECTUR
    COOR_ELGA        478  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  ENBSP_I  PNBSP_I
                          OUT__ EGGEOP_R PCOORPG
    DEGE_ELGA        584  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  DDL_MECA PDEPLAR  NGEOMER  PGEOMER
                                CMATERC  PMATERC  ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                          OUT__ EDEFGPG  PDEFOPG
    DEGE_ELNO        584  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  DDL_MECA PDEPLAR  NGEOMER  PGEOMER
                                CMATERC  PMATERC  ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                          OUT__ EDEFGNO  PDEFOGR
    ECIN_ELEM         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    EFGE_ELGA        587  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  ENBSP_I  PNBSP_I  ECONTPG  PSIEFR
                          OUT__ EEFGEGAC PEFGEC   EEFGEGAR PEFGER
    EFGE_ELNO        185  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECONTPG  PCONTRR
                                DDL_MECA PDEPLAR  NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I
                                ENONLIN  PNONLIN  CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                          OUT__ EEFGENOC PEFFORC  EEFGENOR PEFFORR
    ENEL_ELEM         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    ENER_TOTALE       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    EPEQ_ELGA        335  IN__  EDEFOPG  PDEFORR
                          OUT__ EDFEQPG  PDEFOEQ
    EPEQ_ELNO        335  IN__  EDEFONO  PDEFORR
                          OUT__ EDFEQNO  PDEFOEQ
    EPOT_ELEM         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    EPSI_ELGA        584  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  DDL_MECA PDEPLAR  ENBSP_I  PNBSP_I
                                ZVARCPG  PVARCPR
                          OUT__ EDEFOPC  PDEFOPC  EDEFOPG  PDEFOPG
    EPSI_ELNO          4  IN__  EDEFOPG  PDEFOPG
                          OUT__ EDEFONC  PDEFONC  EDEFONO  PDEFONO
    FORC_NODA        585  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR
                                NGEOMER  PGEOMER  ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR
                          OUT__ MVECTUR  PVECTUR
    FULL_MECA        586  IN__  CCAGEPO  PCAGEPO  CCAMASS  PCAMASS  CCAORIE  PCAORIE  CCARCRI  PCARCRI
                                CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  CMATERC  PMATERC
                                ENBSP_I  PNBSP_I  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                                ZVARIPG  PVARIMP  ZVARIPG  PVARIMR
                          OUT__ ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUUR  PMATUUR  ZVARIPG  PVARIPR
                                MVECTUR  PVECTUR
    FULL_MECA_ELAS    -1  IN__
                          OUT__ XXXXXX   XXXXXX
    INDIC_ENER        -1  IN__
                          OUT__ XXXXXX   XXXXXX
    INDIC_SEUIL       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    INIT_VARC         99  IN__
                          OUT__ ZVARCPG  PVARCPR
    MASS_FLUI_STRU    -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    MASS_INER         38  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                ZVARCPG  PVARCPR
                          OUT__ EMASSINE PMASSINE
    MASS_MECA        582  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR
                          OUT__ MMATUUR  PMATUUR
    MASS_MECA_DIAG    -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    MASS_MECA_EXPLI   -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    MINMAX_SP         99  IN__
                          OUT__ EGAMIMA  PGAMIMA  ENOMIMA  PNOMIMA
    M_GAMMA          582  IN__  DDL_MECA PACCELR  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER
                                CMATERC  PMATERC  ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR
                          OUT__ MVECTUR  PVECTUR
    NSPG_NBVA        496  IN__  CCOMPO2  PCOMPOR  ENBSP_I  PNBSP_I
                          OUT__ EDCEL_I  PDCEL_I
    PAS_COURANT      404  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC
                          OUT__ ECOURAN  PCOURAN
    RAPH_MECA        586  IN__  CCAGEPO  PCAGEPO  CCAMASS  PCAMASS  CCAORIE  PCAORIE  CCARCRI  PCARCRI
                                CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  CMATERC  PMATERC
                                ENBSP_I  PNBSP_I  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                                ZVARIPG  PVARIMP  ZVARIPG  PVARIMR
                          OUT__ ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    REFE_FORC_NODA   585  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  NGEOMER  PGEOMER
                                ENBSP_I  PNBSP_I  EREFCO   PREFCO
                          OUT__ MVECTUR  PVECTUR
    REPERE_LOCAL     135  IN__  CCAORIE  PCAORIE
                          OUT__ CGEOMER  PREPLO1  CGEOMER  PREPLO2  CGEOMER  PREPLO3
    RICE_TRACEY       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    RIGI_FLUI_STRU    -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA        582  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                ENBSP_I  PNBSP_I  CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR
                          OUT__ MMATUUR  PMATUUR
    RIGI_MECA_ELAS    -1  IN__  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA_GE      -1  IN__
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA_HYST    50  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  MMATUUR  PRIGIEL  ZVARCPG  PVARCPR
                          OUT__ MMATUUC  PMATUUC
    RIGI_MECA_RO      -1  IN__
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA_TANG   586  IN__  CCAGEPO  PCAGEPO  CCAMASS  PCAMASS  CCAORIE  PCAORIE  CCARCRI  PCARCRI
                                CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  CMATERC  PMATERC
                                ENBSP_I  PNBSP_I  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                                ZVARIPG  PVARIMP  ZVARIPG  PVARIMR
                          OUT__ MMATUUR  PMATUUR
    SIEF_ELGA        584  IN__  CCAGEPO  PCAGEPO  CCAORIE  PCAORIE  DDL_MECA PDEPLAR  NGEOMER  PGEOMER
                                CMATERC  PMATERC  ENBSP_I  PNBSP_I  CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR
                                ZVARCPG  PVARCRR
                          OUT__ ECONTPC  PCONTRC  ECONTPG  PCONTRR
    SIEF_ELNO          4  IN__  ECONTPG  PCONTRR  ZVARCPG  PVARCPR
                          OUT__ ECONTNC  PSIEFNOC ECONTNO  PSIEFNOR
    SIEQ_ELGA        335  IN__  ECONTPG  PCONTRR
                          OUT__ ECOEQPG  PCONTEQ
    SIEQ_ELNO        335  IN__  ECONTNO  PCONTRR
                          OUT__ ECOEQNO  PCONTEQ
    SIGM_ELGA        546  IN__  ECONTPG  PSIEFR
                          OUT__ ECONTPC  PSIGMC   ECONTPG  PSIGMR
    SIGM_ELNO          4  IN__  ECONTPG  PCONTRR
                          OUT__ ECONTNC  PSIEFNOC ECONTNO  PSIEFNOR
    SIPO_ELNO         -1  IN__
                          OUT__ XXXXXX   XXXXXX
    TOU_INI_ELEM      99  IN__
                          OUT__ CGEOMER  PGEOM_R  ENBSP_I  PNBSP_I
    TOU_INI_ELGA      99  IN__
                          OUT__ EGNEUT_F PNEUT_F  EGNEUT_R PNEUT_R  ECONTPG  PSIEF_R  ZVARIPG  PVARI_R
    TOU_INI_ELNO      99  IN__
                          OUT__ NGEOMER PGEOM_R  ENNEUT_F PNEUT_F  ENNEUT_R PNEUT_R  EPRESNO  PPRES_R  ECONTNO PSIEF_R
    VARI_ELNO          4  IN__  ZVARIPG  PVARIGR
                          OUT__ ZVARINO  PVARINR
    WEIBULL           -1  IN__
                          OUT__ XXXXXX   XXXXXX
