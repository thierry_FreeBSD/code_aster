%& LIBRARY TYPELEM
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_THCQ_2
TYPE_GENE__

ENTETE__ ELEMENT__ THCOQU4          MAILLE__ QUAD4
   ELREFE__  QU4     GAUSS__  RIGI=FPG4   MASS=FPG4    NOEU_S=NOEU_S FPG1=FPG1   FPG_LISTE__  MATER=(FPG1)
ENTETE__ ELEMENT__ THCOQU8          MAILLE__ QUAD8
   ELREFE__  QU8     GAUSS__  RIGI=FPG9   MASS=FPG9    NOEU_S=NOEU_S FPG1=FPG1   FPG_LISTE__  MATER=(FPG1)
ENTETE__ ELEMENT__ THCOQU9          MAILLE__ QUAD9
   ELREFE__  QU9     GAUSS__  RIGI=FPG9   MASS=FPG9    NOEU_S=NOEU_S FPG1=FPG1   FPG_LISTE__  MATER=(FPG1)
ENTETE__ ELEMENT__ THCOTR3          MAILLE__ TRIA3
   ELREFE__  TR3     GAUSS__  RIGI=FPG1   MASS=FPG3    NOEU_S=NOEU_S FPG1=FPG1   FPG_LISTE__  MATER=(FPG1)
ENTETE__ ELEMENT__ THCOTR6          MAILLE__ TRIA6
   ELREFE__  TR6     GAUSS__  RIGI=FPG3   MASS=FPG6    NOEU_S=NOEU_S FPG1=FPG1   FPG_LISTE__  MATER=(FPG1)
ENTETE__ ELEMENT__ THCOTR7          MAILLE__ TRIA7
   ELREFE__  TR7     GAUSS__  RIGI=FPG6   MASS=FPG6    NOEU_S=NOEU_S FPG1=FPG1   FPG_LISTE__  MATER=(FPG1)

MODE_LOCAL__
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CCACOQU  = CACOQU   ELEM__         (EP       ALPHA    BETA     )
    CCOEFHF  = COEH_F   ELEM__         (H_INF    H_SUP    )
    CCOEFHR  = COEH_R   ELEM__         (H_INF    H_SUP    )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    CFLUXNF  = FLUN_F   ELEM__         (FLUN_INF FLUN_SUP )
    CFLUXNR  = FLUN_R   ELEM__         (FLUN_INF FLUN_SUP )
    EFLUXPG  = FLUX_R   ELGA__ RIGI    (FLUX     FLUY     FLUZ     FLUX_INF FLUY_INF FLUZ_INF
                                        FLUX_SUP FLUY_SUP FLUZ_SUP )
    EFLUXNO  = FLUX_R   ELNO__ IDEN__  (FLUX     FLUY     FLUZ     FLUX_INF FLUY_INF FLUZ_INF
                                        FLUX_SUP FLUY_SUP FLUZ_SUP )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        Z        )
    EGGEOM_R = GEOM_R   ELGA__ RIGI    (X        Y        )
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        W        )
    ENGEOM_R = GEOM_R   ELNO__ IDEN__  (X        Y        Z        )
    CTEMPSR  = INST_R   ELEM__         (INST     DELTAT   THETA    )
    EGINST_R = INST_R   ELGA__ RIGI    (INST     )
    ENINST_R = INST_R   ELNO__ IDEN__  (INST     )
    ENBSP_I  = NBSP_I   ELEM__         (COQ_NCOU )
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[30]    )
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[30]    )
    CT_EXTF  = TEMP_F   ELEM__         (TEMP TEMP_INF TEMP_SUP )
    CT_EXTR  = TEMP_R   ELEM__         (TEMP TEMP_INF TEMP_SUP )
    DDL_THER = TEMP_R   ELNO__ IDEN__  (TEMP_MIL TEMP_INF TEMP_SUP )
    ENNEUT_F = NEUT_F   ELNO__ IDEN__  (X[30]    )
    ENNEUT_R = NEUT_R   ELNO__ IDEN__  (X[30]    )
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )

VECTEUR__
    MVECTTR = VTEM_R DDL_THER

MATRICE__
    MMATTTR = MTEM_R DDL_THER DDL_THER

OPTION__
    CARA_GEOM         -1  IN__  XXXXXX   XXXXXX   
                          OUT__ XXXXXX   XXXXXX   
    CHAR_LIMITE       -1  IN__  XXXXXX   XXXXXX   
                          OUT__ XXXXXX   XXXXXX   
    CHAR_THER_EVOL   110  IN__  CCACOQU  PCACOQU  CCOEFHF  PCOEFHF  CCOEFHR  PCOEFHR  NGEOMER  PGEOMER  
                                CMATERC  PMATERC  DDL_THER PTEMPER  CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  
                          OUT__ MVECTTR  PVECTTR  
    CHAR_THER_FLUN_F 105  IN__  CFLUXNF  PFLUXNF  NGEOMER  PGEOMER  CTEMPSR  PTEMPSR  
                          OUT__ MVECTTR  PVECTTR  
    CHAR_THER_FLUN_R 106  IN__  CFLUXNR  PFLUXNR  NGEOMER  PGEOMER  
                          OUT__ MVECTTR  PVECTTR  
    CHAR_THER_TEXT_F 107  IN__  CCOEFHF  PCOEFHF  NGEOMER  PGEOMER  DDL_THER PTEMPER  CTEMPSR  PTEMPSR  
                                CT_EXTF  PT_EXTF  
                          OUT__ MVECTTR  PVECTTR  
    CHAR_THER_TEXT_R 108  IN__  CCOEFHR  PCOEFHR  NGEOMER  PGEOMER  DDL_THER PTEMPER  CTEMPSR  PTEMPSR  
                                CT_EXTR  PT_EXTR  
                          OUT__ MVECTTR  PVECTTR  
    COOR_ELGA        479  IN__  NGEOMER  PGEOMER  
                          OUT__ EGGEOP_R PCOORPG  
    ECIN_ELEM         -1  IN__  XXXXXX   XXXXXX   
                          OUT__ XXXXXX   XXXXXX   
    ENEL_ELEM         -1  IN__  XXXXXX   XXXXXX   
                          OUT__ XXXXXX   XXXXXX   
    ENER_TOTALE       -1  IN__  XXXXXX   XXXXXX   
                          OUT__ XXXXXX   XXXXXX   
    EPOT_ELEM         -1  IN__  XXXXXX   XXXXXX   
                          OUT__ XXXXXX   XXXXXX   
    ERTH_ELEM         -1  IN__  XXXXXX   XXXXXX   
                          OUT__ XXXXXX   XXXXXX   
    ERTH_ELNO         -1  IN__  XXXXXX   XXXXXX   
                          OUT__ XXXXXX   XXXXXX   
    FLUX_ELGA        109  IN__  CCACOQU  PCACOQU  NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I  
                                DDL_THER PTEMPER  CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  
                          OUT__ EFLUXPG  PFLUXPG  
    FLUX_ELNO          4  IN__  EFLUXPG  PFLUXPG  
                          OUT__ EFLUXNO  PFLUXNO  
    INDIC_ENER        -1  IN__  XXXXXX   XXXXXX   
                          OUT__ XXXXXX   XXXXXX   
    INDIC_SEUIL       -1  IN__  XXXXXX   XXXXXX   
                          OUT__ XXXXXX   XXXXXX   
    INIT_VARC         99  IN__  
                          OUT__ ZVARCPG  PVARCPR  
    MASS_INER         -1  IN__  XXXXXX   XXXXXX   
                          OUT__ XXXXXX   XXXXXX   
    MASS_THER        102  IN__  CCACOQU  PCACOQU  NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR  
                                ZVARCPG  PVARCPR  
                          OUT__ MMATTTR  PMATTTR  
    NSPG_NBVA        496  IN__  CCOMPO2  PCOMPOR  
                          OUT__ EDCEL_I  PDCEL_I  
    RIGI_THER        101  IN__  CCACOQU  PCACOQU  NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR  
                                ZVARCPG  PVARCPR  
                          OUT__ MMATTTR  PMATTTR  
    RIGI_THER_COEH_F 103  IN__  CCOEFHF  PCOEFHF  NGEOMER  PGEOMER  CTEMPSR  PTEMPSR  
                          OUT__ MMATTTR  PMATTTR  
    RIGI_THER_COEH_R 104  IN__  CCOEFHR  PCOEFHR  NGEOMER  PGEOMER  CTEMPSR  PTEMPSR  
                          OUT__ MMATTTR  PMATTTR  
    TOU_INI_ELEM      99  IN__  
                          OUT__ ENBSP_I  PNBSP_I  
    TOU_INI_ELGA      99  IN__  
                          OUT__ EGGEOM_R PGEOM_R  EGINST_R PINST_R  EGNEUT_F PNEUT_F  EGNEUT_R PNEUT_R  
    TOU_INI_ELNO      99  IN__  
                          OUT__ ENGEOM_R PGEOM_R  ENINST_R PINST_R  ENNEUT_F PNEUT_F  ENNEUT_R PNEUT_R  
    WEIBULL           -1  IN__  XXXXXX   XXXXXX   
                          OUT__ XXXXXX   XXXXXX
