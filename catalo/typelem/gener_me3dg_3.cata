%& LIBRARY TYPELEM
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_ME3DG_3
TYPE_GENE__

ENTETE__ ELEMENT__ MGCA_HEXA20      MAILLE__ HEXA20
   ELREFE__  H20       GAUSS__  RIGI=FPG8 FPG1=FPG1  MASS=FPG27   FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  HE8       GAUSS__  RIGI=FPG8  MASS=FPG27
   ELREFE__  QU8       GAUSS__  RIGI=FPG9  MASS=FPG9
   ENS_NOEUD__  EN2     =     9  10  11  12  13  14  15  16  17  18  19  20
   ENS_NOEUD__  EN1     =     1  2  3  4  5  6  7  8
ENTETE__ ELEMENT__ MGCA_TETRA10     MAILLE__ TETRA10
   ELREFE__  T10       GAUSS__  RIGI=FPG4 FPG1=FPG1  MASS=FPG15   FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  TE4       GAUSS__  RIGI=FPG4  MASS=FPG15
   ELREFE__  TR6       GAUSS__  RIGI=FPG6  MASS=FPG6
   ENS_NOEUD__  EN2     =     5  6  7  8  9  10
   ENS_NOEUD__  EN1     =     1  2  3  4
ENTETE__ ELEMENT__ MGCA_PENTA15     MAILLE__ PENTA15
   ELREFE__  P15       GAUSS__  RIGI=FPG6 FPG1=FPG1  MASS=FPG21   FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  PE6       GAUSS__  RIGI=FPG6  MASS=FPG6
   ELREFE__  QU8       GAUSS__  RIGI=FPG9  MASS=FPG9
   ELREFE__  TR6       GAUSS__  RIGI=FPG6  MASS=FPG6
   ENS_NOEUD__  EN2     =     7  8  9  10  11  12  13  14  15
   ENS_NOEUD__  EN1     =     1  2  3  4  5  6
ENTETE__ ELEMENT__ MGCA_PYRAM13     MAILLE__ PYRAM13
   ELREFE__  P13       GAUSS__  RIGI=FPG5 FPG1=FPG1  MASS=FPG27   FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  PY5       GAUSS__  RIGI=FPG5  MASS=FPG5
   ELREFE__  QU8       GAUSS__  RIGI=FPG9  MASS=FPG9
   ELREFE__  TR6       GAUSS__  RIGI=FPG6  MASS=FPG6
   ENS_NOEUD__  EN2     =     6  7  8  9  10  11  12  13
   ENS_NOEUD__  EN1     =     1  2  3  4  5

MODE_LOCAL__
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CCAMASS  = CAMASS   ELEM__         (C        ALPHA    BETA     KAPPA    X        Y
                                        Z        )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC   PERTURB
                                        TOLDEBO  ITEDEBO  TSSEUIL TSAMPL TSRETOUR)
    ECODRET  = CODE_I   ELEM__         (IRET     )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1
                                        XXXX2    KIT[9]   )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN1   (DX       DY       DZ       EPXX     EPYY     EPZZ
                                        EPXY     EPXZ     EPYZ     )
                                 EN2   (DX       DY       DZ       )
    NDEPLAR  = DEPL_R   ELNO__ IDEN__  (DX       DY       DZ       )
    EDOMGGA  = DOMA_R   ELGA__ RIGI    (DOMA     )
    EDOMGNO  = DOMA_R   ELNO__ IDEN__  (DOMA     )
    EENERR   = ENER_R   ELEM__         (TOTALE   )
    EENERPG  = ENER_R   ELGA__ RIGI    (TOTALE   )
    EENERNO  = ENER_R   ELNO__ IDEN__  (TOTALE   )
    CEPSINF  = EPSI_F   ELEM__         (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    CEPSINR  = EPSI_R   ELEM__         (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDFEQPG  = EPSI_R   ELGA__ RIGI    (INVA_2   PRIN_1   PRIN_2   PRIN_3   INVA_2SG VECT_1_X
                                        VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z VECT_3_X
                                        VECT_3_Y VECT_3_Z )
    EDFEQNO  = EPSI_R   ELNO__ IDEN__  (INVA_2   PRIN_1   PRIN_2   PRIN_3   INVA_2SG VECT_1_X
                                        VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z VECT_3_X
                                        VECT_3_Y VECT_3_Z )
    EDEFOPG  = EPSI_R   ELGA__ RIGI    (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDEFOPC  = EPSI_C   ELGA__ RIGI    (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDEFONO  = EPSI_R   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDEFONC  = EPSI_C   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDFVCPG  = EPSI_R   ELGA__ RIGI    (EPTHER_L EPTHER_T EPTHER_N EPSECH   EPHYDR  )
    EDFVCNO  = EPSI_R   ELNO__ IDEN__  (EPTHER_L EPTHER_T EPTHER_N EPSECH   EPHYDR  )
    ENORME   = NEUT_R   ELEM__         (X1       )
    EMNEUT_R = NEUT_R   ELEM__         (X[30]    )
    CFORCEF  = FORC_F   ELEM__         (FX       FY       FZ       )
    NFORCER  = FORC_R   ELNO__ IDEN__  (FX       FY       FZ       )
    EFORCER  = FORC_R   ELGA__ RIGI    (FX       FY       FZ       )
    COMEG2R  = OME2_R   ELEM__         (OMEG2    )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        Z        )
    CGEOMER  = GEOM_R   ELEM__         (X        Y        Z        )
    EGGEOM_R = GEOM_R   ELGA__ RIGI    (X        Y        Z        )
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        Z        W        )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    EGINST_R = INST_R   ELGA__ RIGI    (INST     )
    ENINST_R = INST_R   ELNO__ IDEN__  (INST     )
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[30]    )
    ECOURAN  = NEUT_R   ELEM__         (X1       )
    CSOUSOP  = NEUT_K24 ELEM__         (Z1       )
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[30]    )
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    CTYPEPI  = PILO_K   ELEM__         (TYPE     )
    CBORNPI  = PILO_R   ELEM__         (A0       A1       )
    CCDTAU   = PILO_R   ELEM__         (A0       )
    ECOPILO  = PILO_R   ELGA__ RIGI    (A0       A1       A2       A3       ETA      )
    ECONTPG  = SIEF_R   ELGA__ RIGI    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTPC  = SIEF_C   ELGA__ RIGI    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTNC  = SIEF_C   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECOEQPG  = SIEF_R   ELGA__ RIGI    (VMIS     TRESCA   PRIN_1   PRIN_2   PRIN_3   VMIS_SG
                                        VECT_1_X VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z
                                        VECT_3_X VECT_3_Y VECT_3_Z TRSIG    TRIAX    )
    ECOEQNO  = SIEF_R   ELNO__ IDEN__  (VMIS     TRESCA   PRIN_1   PRIN_2   PRIN_3   VMIS_SG
                                        VECT_1_X VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z
                                        VECT_3_X VECT_3_Y VECT_3_Z TRSIG    TRIAX    )
    EDERAPG  = DERA_R   ELGA__ RIGI    (DCHA_V   DCHA_T   RADI_V   ERR_RADI   )
    EDERANO  = DERA_R   ELNO__ IDEN__  (DCHA_V   DCHA_T   RADI_V   ERR_RADI   )
    ZVARIPG  = VARI_R   ELGA__ RIGI    (VARI     )
    ZVARINO  = VARI_R   ELNO__ IDEN__  (VARI     )
    EVOISIN  = VOISIN   ELEM__         (V0       V1       V2       V3       V4       V5
                                        V6       T0       T1       T2       T3       T4
                                        T5       T6       )
    CITERAT  = NEUT_R   ELEM__         (X1       )

    ENNEUT_F = NEUT_F   ELNO__ IDEN__  (X[30]    )
    ENNEUT_R = NEUT_R   ELNO__ IDEN__  (X[30]    )

VECTEUR__
    MVECTUR = VDEP_R DDL_MECA
    MVECTDR = VDEP_R NDEPLAR

MATRICE__
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    MMATUNS = MDNS_R DDL_MECA DDL_MECA

OPTION__
    ADD_SIGM         581  IN__  ECONTPG  PEPCON1  ECONTPG  PEPCON2  
                          OUT__ ECONTPG  PEPCON3  
    AMOR_MECA         50  IN__  NGEOMER  PGEOMER  MMATUUR  PMASSEL  CMATERC  PMATERC  MMATUNS  PRIGINS  
                                ZVARCPG  PVARCPR  
                          OUT__ MMATUNS  PMATUNS  
    CARA_GEOM         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_LIMITE       -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_EPSA_R 426  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_EPSI_F  49  IN__  CCAMASS  PCAMASS  CEPSINF  PEPSINF  NGEOMER  PGEOMER  CMATERC  PMATERC  
                                CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_EPSI_R  49  IN__  CCAMASS  PCAMASS  CEPSINR  PEPSINR  NGEOMER  PGEOMER  CMATERC  PMATERC  
                                ZVARCPG  PVARCPR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_FF3D3D  17  IN__  CFORCEF  PFF3D3D  NGEOMER  PGEOMER  CTEMPSR  PTEMPSR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_FR3D3D  16  IN__  NFORCER  PFR3D3D  NGEOMER  PGEOMER  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_HYDR_R 492  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR  
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_PESA_R  15  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR  ZVARCPG  PVARCPR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_ROTA_R  -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_SECH_R 492  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR  
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_TEMP_R  13  IN__  CCAMASS  PCAMASS  CCOMPOR  PCOMPOR  NGEOMER  PGEOMER  CMATERC  PMATERC  
                                CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                          OUT__ MVECTDR  PVECTUR  
    COOR_ELGA        488  IN__  NGEOMER  PGEOMER  
                          OUT__ EGGEOP_R PCOORPG  
    ECIN_ELEM         12  IN__  NDEPLAR  PDEPLAR  NGEOMER  PGEOMER  CMATERC  PMATERC  COMEG2R  POMEGA2  
                                ZVARCPG  PVARCPR  NDEPLAR  PVITESR  
                          OUT__ EENERR   PENERCR  
    ENEL_ELEM        491  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTPR  NDEPLAR  PDEPLR   NGEOMER  PGEOMER  
                                CMATERC  PMATERC  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARIPG  PVARIPR  
                          OUT__ EENERR   PENERD1  
    ENEL_ELGA        576  IN__  CCAMASS  PCAMASS  CCOMPOR  PCOMPOR  ECONTPG  PCONTRR  DDL_MECA PDEPLAR  
                                NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  ZVARIPG  PVARIGR  
                          OUT__ EENERPG  PENERDR  
    ENEL_ELNO          4  IN__  EENERPG  PENERPG  
                          OUT__ EENERNO  PENERNO  
    ENER_TOTALE      491  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  ECONTPG  PCONTPR  NDEPLAR  PDEPLM   
                                DDL_MECA PDEPLR   NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  ZVARIPG  PVARIPR  
                          OUT__ EENERR   PENERD1  
    EPEQ_ELGA        335  IN__  EDEFOPG  PDEFORR  
                          OUT__ EDFEQPG  PDEFOEQ  
    EPEQ_ELNO        335  IN__  EDEFONO  PDEFORR  
                          OUT__ EDFEQNO  PDEFOEQ  
    EPME_ELGA         25  IN__  DDL_MECA PDEPLAR  NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR  
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                          OUT__ EDEFOPG  PDEFOPG  
    EPME_ELNO          4  IN__  EDEFOPG  PDEFOPG  
                          OUT__ EDEFONO  PDEFONO  
    EPMG_ELGA         25  IN__  DDL_MECA PDEPLAR  NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR  
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                          OUT__ EDEFOPG  PDEFOPG  
    EPMG_ELNO          4  IN__  EDEFOPG  PDEFOPG  
                          OUT__ EDEFONO  PDEFONO  
    EPMQ_ELGA        335  IN__  EDEFOPG  PDEFORR  
                          OUT__ EDFEQPG  PDEFOEQ  
    EPMQ_ELNO        335  IN__  EDEFONO  PDEFORR  
                          OUT__ EDFEQNO  PDEFOEQ  
    EPOT_ELEM        218  IN__  CCAMASS  PCAMASS  NDEPLAR  PDEPLAR  NGEOMER  PGEOMER  CMATERC  PMATERC  
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                          OUT__ EENERR   PENERDR  
    EPSG_ELGA         25  IN__  DDL_MECA PDEPLAR  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  
                          OUT__ EDEFOPG  PDEFOPG  
    EPSG_ELNO          4  IN__  EDEFOPG  PDEFOPG  
                          OUT__ EDEFONO  PDEFONO  
    EPSI_ELGA         25  IN__  NDEPLAR  PDEPLAR  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  
                          OUT__ EDEFOPC  PDEFOPC  EDEFOPG  PDEFOPG  
    EPSI_ELNO          4  IN__  EDEFOPG  PDEFOPG  
                          OUT__ EDEFONC  PDEFONC  EDEFONO  PDEFONO  
    EPSP_ELGA        333  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTRR  NDEPLAR  PDEPLAR  NGEOMER  PGEOMER  
                                CMATERC  PMATERC  CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                                ZVARIPG  PVARIGR  
                          OUT__ EDEFOPG  PDEFOPG  
    EPSP_ELNO          4  IN__  EDEFOPG  PDEFOPG  
                          OUT__ EDEFONO  PDEFONO  
    EPVC_ELGA        529  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                          OUT__ EDFVCPG  PDEFOPG  
    EPVC_ELNO          4  IN__  EDFVCPG  PDEFOPG  
                          OUT__ EDFVCNO  PDEFONO  
    ERME_ELEM         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    ERME_ELNO         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    FORC_NODA        437  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  NGEOMER  PGEOMER  
                                ZVARCPG  PVARCPR  
                          OUT__ MVECTUR  PVECTUR  
    FULL_MECA        113  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                CITERAT  PITERAT  CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  ZVARIPG  PVARIMP  ZVARIPG  PVARIMR  
                          OUT__ ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUNS  PMATUNS  ZVARIPG  PVARIPR  
                                MVECTUR  PVECTUR  
    FULL_MECA_ELAS   113  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                                ZVARIPG  PVARIMP  ZVARIPG  PVARIMR  
                          OUT__ ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUNS  PMATUNS  ZVARIPG  PVARIPR  
                                MVECTUR  PVECTUR  
    INDIC_ENER       491  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTPR  NDEPLAR  PDEPLR   NGEOMER  PGEOMER  
                                CMATERC  PMATERC  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARIPG  PVARIPR  
                          OUT__ EENERR   PENERD1  EENERR   PENERD2  
    INDIC_SEUIL      491  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTPR  NDEPLAR  PDEPLR   NGEOMER  PGEOMER  
                                CMATERC  PMATERC  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARIPG  PVARIPR  
                          OUT__ EENERR   PENERD1  EENERR   PENERD2  
    INIT_MAIL_VOIS    99  IN__  
                          OUT__ EVOISIN  PVOISIN  
    INIT_VARC         99  IN__  
                          OUT__ ZVARCPG  PVARCPR  
    MASS_INER         -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    MASS_MECA        113  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  
                          OUT__ MMATUUR  PMATUUR  
    MASS_MECA_DIAG   113  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  
                          OUT__ MMATUUR  PMATUUR  
    MASS_MECA_EXPLI  113  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  
                          OUT__ MMATUUR  PMATUUR  
    M_GAMMA           -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    NORME_L2          -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    NSPG_NBVA        496  IN__  CCOMPO2  PCOMPOR  
                          OUT__ EDCEL_I  PDCEL_I  
    PAS_COURANT      404  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  
                          OUT__ ECOURAN  PCOURAN  
    PILO_PRED_DEFO   543  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  NDEPLAR  PDDEPLR  NDEPLAR  PDEPL0R  
                                NDEPLAR  PDEPL1R  NDEPLAR  PDEPLMR  NGEOMER  PGEOMER  CMATERC  PMATERC  
                                CTYPEPI  PTYPEPI  ZVARIPG  PVARIMR  
                          OUT__ ECOPILO  PCOPILO  
    PILO_PRED_ELAS   544  IN__  CBORNPI  PBORNPI  CCDTAU   PCDTAU   CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  
                                DDL_MECA PDDEPLR  DDL_MECA PDEPL0R  DDL_MECA PDEPL1R  DDL_MECA PDEPLMR  
                                NGEOMER  PGEOMER  CMATERC  PMATERC  CTYPEPI  PTYPEPI  ZVARIPG  PVARIMR  
                          OUT__ ECOPILO  PCOPILO  
    RAPH_MECA        113  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                CITERAT  PITERAT  CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  ZVARIPG  PVARIMP  ZVARIPG  PVARIMR  
                          OUT__ ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR  
    REFE_FORC_NODA    -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    REPERE_LOCAL     133  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER  
                          OUT__ CGEOMER  PREPLO1  CGEOMER  PREPLO2  CGEOMER  PREPLO3  
    RICE_TRACEY       -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    RIGI_MECA_ELAS   113  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                                ZVARIPG  PVARIMR  
                          OUT__ MMATUNS  PMATUNS  
    RIGI_MECA_GE      -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    RIGI_MECA_HYST    -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    RIGI_MECA_RO      -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    RIGI_MECA_TANG   113  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                CITERAT  PITERAT  CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  ZVARIPG  PVARIMR  
                          OUT__ MMATUNS  PMATUNS  
    SIEF_ELGA         -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    SIEF_ELNO          4  IN__  ECONTPG  PCONTRR  ZVARCPG  PVARCPR  
                          OUT__ ECONTNC  PSIEFNOC ECONTNO  PSIEFNOR 
    SIEQ_ELGA        335  IN__  ECONTPG  PCONTRR  
                          OUT__ ECOEQPG  PCONTEQ  
    SIEQ_ELNO        335  IN__  ECONTNO  PCONTRR  
                          OUT__ ECOEQNO  PCONTEQ  
    SIGM_ELGA        546  IN__  ECONTPG  PSIEFR   
                          OUT__ ECONTPC  PSIGMC   ECONTPG  PSIGMR   
    SIGM_ELNO          4  IN__  ECONTPG  PCONTRR  
                          OUT__ ECONTNC  PSIEFNOC ECONTNO  PSIEFNOR 
    SYME_MDNS_R      222  IN__  MMATUNS  PNOSYM   
                          OUT__ MMATUUR  PSYM     
    TOU_INI_ELGA      99  IN__  
                          OUT__ EDOMGGA  PDOMMAG  EGGEOM_R PGEOM_R  EGINST_R PINST_R  EGNEUT_F PNEUT_F  
                                EGNEUT_R PNEUT_R  ECONTPG  PSIEF_R  ZVARIPG  PVARI_R  
    TOU_INI_ELNO      99  IN__  
                          OUT__ EDOMGNO  PDOMMAG  EDEFONO  PEPSI_R  NGEOMER  PGEOM_R  ENINST_R PINST_R  
                                ENNEUT_F PNEUT_F  ENNEUT_R PNEUT_R  ECONTNO  PSIEF_R  ZVARINO  PVARI_R  
    VARI_ELNO          4  IN__  ZVARIPG  PVARIGR  
                          OUT__ ZVARINO  PVARINR  
    VERI_JACOBIEN    328  IN__  NGEOMER  PGEOMER  
                          OUT__ ECODRET  PCODRET  
    WEIBULL           -1  IN__  
                          OUT__ XXXXXX   XXXXXX
