%& LIBRARY TYPELEM
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
% person_in_charge: sebastien.fayolle at edf.fr
GENER_ME3DO3
TYPE_GENE__

ENTETE__ ELEMENT__ MINCOS_HEXA8       MAILLE__ HEXA8
   ELREFE__  HE8       GAUSS__  RIGI=FPG8  MASS=FPG8  NOEU=NOEU   FPG1=FPG1  FPG_LISTE__  MATER=(RIGI MASS NOEU FPG1)
   ELREFE__  HE8       GAUSS__  RIGI=FPG8
   ELREFE__  QU4       GAUSS__  RIGI=FPG4  MASS=FPG4  NOEU=NOEU
   ENS_NOEUD__  EN1     =     1  2  3  4  5  6  7  8
ENTETE__ ELEMENT__ MINCOS_TETRA4     MAILLE__ TETRA4
   ELREFE__  TE4       GAUSS__  RIGI=FPG4  MASS=FPG4  NOEU=NOEU   FPG1=FPG1  FPG_LISTE__  MATER=(RIGI MASS NOEU FPG1)
   ELREFE__  TE4       GAUSS__  RIGI=FPG4
   ELREFE__  TR3       GAUSS__  RIGI=FPG3  MASS=FPG3  NOEU=NOEU
   ENS_NOEUD__  EN1     =     1  2  3  4
ENTETE__ ELEMENT__ MINCOS_PENTA6      MAILLE__ PENTA6
   ELREFE__  PE6       GAUSS__  RIGI=FPG6  MASS=FPG6  NOEU=NOEU   FPG1=FPG1  FPG_LISTE__  MATER=(RIGI MASS NOEU FPG1)
   ELREFE__  PE6       GAUSS__  RIGI=FPG6
   ELREFE__  QU4       GAUSS__  RIGI=FPG4  MASS=FPG4  NOEU=NOEU
   ELREFE__  TR3       GAUSS__  RIGI=FPG3  MASS=FPG3  NOEU=NOEU
   ENS_NOEUD__  EN1     =     1  2  3  4  5  6
ENTETE__ ELEMENT__ MINCOS_PYRAM5      MAILLE__ PYRAM5
   ELREFE__  PY5       GAUSS__  RIGI=FPG5  MASS=FPG5  NOEU=NOEU   FPG1=FPG1  FPG_LISTE__  MATER=(RIGI MASS NOEU FPG1)
   ELREFE__  PY5       GAUSS__  RIGI=FPG5
   ELREFE__  QU4       GAUSS__  RIGI=FPG4  MASS=FPG4  NOEU=NOEU
   ELREFE__  TR3       GAUSS__  RIGI=FPG3  MASS=FPG3  NOEU=NOEU
   ENS_NOEUD__  EN1     =     1  2  3  4  5

MODE_LOCAL__
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CCAMASS  = CAMASS   ELEM__         (C        ALPHA    )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC
                                        PERTURB  TOLDEBO  ITEDEBO  TSSEUIL  TSAMPL   TSRETOUR )
    ECHALIM  = CHLI_R   ELEM__         (CHLI1    CHLI2    CHLI3    )
    ECODRET  = CODE_I   ELEM__         (IRET     )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1    )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    EGTHETA  = G        ELEM__         (GTHETA   )
    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN1   (DX       DY       DZ       PRES     PIX      PIY      PIZ     )
    NDEPLAR  = DEPL_R   ELNO__ IDEN__  (DX       DY       DZ       )
    EDEFOPG  = EPSI_R   ELGA__ RIGI    (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDEFOPC  = EPSI_C   ELGA__ RIGI    (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDEFONO  = EPSI_R   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDEFONC  = EPSI_C   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    CEPSINF  = EPSI_F   ELEM__         (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    CEPSINR  = EPSI_R   ELEM__         (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDFEQPG  = EPSI_R   ELGA__ RIGI    (INVA_2   PRIN_1   PRIN_2   PRIN_3   INVA_2SG VECT_1_X
                                        VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z VECT_3_X
                                        VECT_3_Y VECT_3_Z )
    EDFEQNO  = EPSI_R   ELNO__ IDEN__  (INVA_2   PRIN_1   PRIN_2   PRIN_3   INVA_2SG VECT_1_X
                                        VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z VECT_3_X
                                        VECT_3_Y VECT_3_Z )
    EERREUR  = ERRE_R   ELEM__         (ERREST   NUEST    SIGCAL   TERMRE   TERMR2   TERMNO
                                        TERMN2   TERMSA   TERMS2   TAILLE   )
    EERRENO  = ERRE_R   ELNO__ IDEN__  (ERREST   NUEST    SIGCAL   TERMRE   TERMR2   TERMNO
                                        TERMN2   TERMSA   TERMS2   TAILLE   )
    CFORCEF  = FORC_F   ELEM__         (FX       FY       FZ       )
    NFORCER  = FORC_R   ELNO__ IDEN__  (FX       FY       FZ       )
    EFORCER  = FORC_R   ELGA__ RIGI    (FX       FY       FZ       )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        Z        )
    CGEOMER  = GEOM_R   ELEM__         (X        Y        Z        )
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        Z        W       )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    CREFERI  = NEUT_I   ELEM__         (X[12]    )
    ECOURAN  = NEUT_R   ELEM__         (X1       )
    CCONSTR  = NEUT_R   ELEM__         (X1       )
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    CROTATR  = ROTA_R   ELEM__         (OME      AR       BR       CR       X        Y
                                        Z        )
    ESIGMPG  = SIEF_R   ELGA__ RIGI    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ESIGMPC  = SIEF_C   ELGA__ RIGI    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ESIGMNO  = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ESIGMNC  = SIEF_C   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTPG  = SIEF_R   ELGA__ RIGI    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     SIP      )
    ECONTPC  = SIEF_C   ELGA__ RIGI    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     SIP      )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     SIP      )
    ECONTNC  = SIEF_C   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     SIP      )
    ECOEQPG  = SIEF_R   ELGA__ RIGI    (VMIS     TRESCA   PRIN_1   PRIN_2   PRIN_3   VMIS_SG
                                        VECT_1_X VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z
                                        VECT_3_X VECT_3_Y VECT_3_Z TRSIG    TRIAX    )
    ECOEQNO  = SIEF_R   ELNO__ IDEN__  (VMIS     TRESCA   PRIN_1   PRIN_2   PRIN_3   VMIS_SG
                                        VECT_1_X VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z
                                        VECT_3_X VECT_3_Y VECT_3_Z TRSIG    TRIAX    )
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    ZVARIPG  = VARI_R   ELGA__ RIGI    (VARI     )
    ZVARINO  = VARI_R   ELNO__ IDEN__  (VARI     )
    EVOISIN  = VOISIN   ELEM__         (V0       V1       V2       V3       V4       V5       V6
                                        T0       T1       T2       T3       T4       T5       T6       )
    ESOURCR  = SOUR_R   ELGA__ RIGI    (SOUR)
    ENORME   = NEUT_R   ELEM__         (X1       )
    EMNEUT_R = NEUT_R   ELEM__         (X[30]    )
    EMNEUT_I = NEUT_I   ELEM__         (X[1]     )
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[30]    )
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[30]    )
    ENNEUT_F = NEUT_F   ELNO__ IDEN__  (X[30]    )
    ENNEUT_R = NEUT_R   ELNO__ IDEN__  (X[30]    )
    N9NEUT_R = NEUT_R   ELNO__ IDEN__  (X[9]     )
    G27NEUTR = NEUT_R   ELGA__ RIGI    (X[27]    )
    N1NEUT_R = NEUT_R   ELNO__ IDEN__  (X1       )
    EKTHETA  = G        ELEM__         (GTHETA   FIC1     FIC2     FIC3
                                        K1       K2       K3       BETA     )
    CFREQR   = FREQ_R   ELEM__         (FREQ     )

VECTEUR__
    MVECTUR = VDEP_R DDL_MECA
    VVECTUR = VDEP_R NDEPLAR

MATRICE__
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    VMATUUR = MDEP_R NDEPLAR  NDEPLAR

OPTION__
    CALC_G            27  IN__  NDEPLAR  PACCELE  CCOMPOR  PCOMPOR  ESIGMPG  PCONTGR  ESIGMPG  PCONTRR
                                EDEFONO  PDEFOPL  NDEPLAR  PDEPINR  NDEPLAR  PDEPLAR  CEPSINR  PEPSINR
                                NFORCER  PFRVOLU  NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR
                                CROTATR  PROTATR  ESIGMNO  PSIGINR  NDEPLAR  PTHETAR  ZVARCPG  PVARCPR
                                ZVARCPG  PVARCRR  ZVARINO  PVARIPR  NDEPLAR  PVITESS
                          OUT__ EGTHETA  PGTHETA
    CALC_G_F          27  IN__  NDEPLAR  PACCELE  CCOMPOR  PCOMPOR  ESIGMPG  PCONTGR  ESIGMPG  PCONTRR
                                EDEFONO  PDEFOPL  NDEPLAR  PDEPINR  NDEPLAR  PDEPLAR  CEPSINF  PEPSINF
                                CFORCEF  PFFVOLU  NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR
                                CROTATR  PROTATR  ESIGMNO  PSIGINR  CTEMPSR  PTEMPSR  NDEPLAR  PTHETAR
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARINO  PVARIPR  NDEPLAR  PVITESS
                          OUT__ EGTHETA  PGTHETA
    CALC_GTP          27  IN__  NDEPLAR  PACCELE  CCOMPOR  PCOMPOR  ESIGMPG  PCONTGR  ESIGMPG  PCONTRR
                                EDEFONO  PDEFOPL  NDEPLAR  PDEPINR  NDEPLAR  PDEPLAR  CEPSINR  PEPSINR
                                NFORCER  PFRVOLU  NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR
                                CROTATR  PROTATR  ESIGMNO  PSIGINR  NDEPLAR  PTHETAR  ZVARCPG  PVARCPR
                                ZVARCPG  PVARCRR  ZVARINO  PVARIPR  NDEPLAR  PVITESS
                          OUT__ EGTHETA  PGTHETA
    CALC_GTP_F        27  IN__  NDEPLAR  PACCELE  CCOMPOR  PCOMPOR  ESIGMPG  PCONTGR  ESIGMPG  PCONTRR
                                EDEFONO  PDEFOPL  NDEPLAR  PDEPINR  NDEPLAR  PDEPLAR  CEPSINF  PEPSINF
                                CFORCEF  PFFVOLU  NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR
                                CROTATR  PROTATR  ESIGMNO  PSIGINR  CTEMPSR  PTEMPSR  NDEPLAR  PTHETAR
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARINO  PVARIPR  NDEPLAR  PVITESS
                          OUT__ EGTHETA  PGTHETA                          
    CALC_G_GLOB       27  IN__  NDEPLAR  PACCELE  CCOMPOR  PCOMPOR  ESIGMPG  PCONTGR  ESIGMPG  PCONTRR
                                EDEFONO  PDEFOPL  NDEPLAR  PDEPINR  NDEPLAR  PDEPLAR  CEPSINR  PEPSINR
                                NFORCER  PFRVOLU  NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR
                                CROTATR  PROTATR  ESIGMNO  PSIGINR  NDEPLAR  PTHETAR  ZVARCPG  PVARCPR
                                ZVARCPG  PVARCRR  ZVARINO  PVARIPR  NDEPLAR  PVITESS
                          OUT__ EGTHETA  PGTHETA
    CALC_G_GLOB_F     27  IN__  NDEPLAR  PACCELE  CCOMPOR  PCOMPOR  ESIGMPG  PCONTGR  ESIGMPG  PCONTRR
                                EDEFONO  PDEFOPL  NDEPLAR  PDEPINR  NDEPLAR  PDEPLAR  CEPSINF  PEPSINF
                                CFORCEF  PFFVOLU  NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR
                                CROTATR  PROTATR  ESIGMNO  PSIGINR  CTEMPSR  PTEMPSR  NDEPLAR  PTHETAR
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARINO  PVARIPR  NDEPLAR  PVITESS
                          OUT__ EGTHETA  PGTHETA
    CALC_K_G         295  IN__  N9NEUT_R PBASLOR  CCOMPOR  PCOMPOR  G27NEUTR PCOURB   NDEPLAR  PDEPINR
                                NDEPLAR  PDEPLAR  CEPSINR  PEPSINR  NFORCER  PFRVOLU  NGEOMER  PGEOMER
                                N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  CPESANR  PPESANR
                                CFREQR   PPULPRO  CROTATR  PROTATR  ESIGMNO  PSIGINR  NDEPLAR  PTHETAR
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                          OUT__ EKTHETA  PGTHETA
    CALC_K_G_F       295  IN__  N9NEUT_R PBASLOR  CCOMPOR  PCOMPOR  G27NEUTR PCOURB   NDEPLAR  PDEPINR
                                NDEPLAR  PDEPLAR  CEPSINF  PEPSINF  CFORCEF  PFFVOLU  NGEOMER  PGEOMER
                                N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  CPESANR  PPESANR
                                CFREQR   PPULPRO  CROTATR  PROTATR  ESIGMNO  PSIGINR  CTEMPSR  PTEMPSR
                                NDEPLAR  PTHETAR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                          OUT__ EKTHETA  PGTHETA
    CARA_GEOM         -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_LIMITE      483  IN__  NDEPLAR  PDEPLAR  NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR
                                ZVARCPG  PVARCPR
                          OUT__ ECHALIM  PECHLI
    CHAR_MECA_FF3D3D  17  IN__  CFORCEF  PFF3D3D  NGEOMER  PGEOMER  CTEMPSR  PTEMPSR
                          OUT__ VVECTUR  PVECTUR
    CHAR_MECA_FR3D3D  16  IN__  NFORCER  PFR3D3D  NGEOMER  PGEOMER
                          OUT__ VVECTUR  PVECTUR
    CHAR_MECA_PESA_R  15  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR  ZVARCPG  PVARCPR
                          OUT__ VVECTUR  PVECTUR
    CHAR_MECA_ROTA_R  -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_TEMP_R  -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    COOR_ELGA        488  IN__  NGEOMER  PGEOMER
                          OUT__ EGGEOP_R PCOORPG
    ECIN_ELEM         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    ENEL_ELEM         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    ENER_TOTALE       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    EPEQ_ELGA        335  IN__  EDEFOPG  PDEFORR
                          OUT__ EDFEQPG  PDEFOEQ
    EPEQ_ELNO        335  IN__  EDEFONO  PDEFORR
                          OUT__ EDFEQNO  PDEFOEQ
    EPOT_ELEM         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    EPSI_ELGA        453  IN__  NDEPLAR  PDEPLAR  NGEOMER  PGEOMER  ZVARCPG  PVARCPR
                          OUT__ EDEFOPC  PDEFOPC  EDEFOPG  PDEFOPG
    EPSI_ELNO          4  IN__  EDEFOPG  PDEFOPG
                          OUT__ EDEFONC  PDEFONC  EDEFONO  PDEFONO
    ERME_ELEM        375  IN__  ESIGMNO  PCONTNO  CFORCEF  PFFVOLU  CREFERI  PFORCE   EFORCER  PFRVOLU
                                NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR  CREFERI  PPRESS
                                CROTATR  PROTATR  CTEMPSR  PTEMPSR  EVOISIN  PVOISIN
                          OUT__ EERREUR  PERREUR
    ERME_ELNO        379  IN__  EERREUR  PERREUR
                          OUT__ EERRENO  PERRENO
    FORC_NODA        596  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                          OUT__ MVECTUR  PVECTUR
    FULL_MECA        595  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                                ZVARIPG  PVARIMP  ZVARIPG  PVARIMR
                          OUT__ ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUUR  PMATUUR  ZVARIPG  PVARIPR
                                MVECTUR  PVECTUR
    FULL_MECA_ELAS   595  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                                ZVARIPG  PVARIMP  ZVARIPG  PVARIMR
                          OUT__ ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUUR  PMATUUR  ZVARIPG  PVARIPR
                                MVECTUR  PVECTUR
    GRAD_NEUT9_R     398  IN__  NGEOMER  PGEOMER  N9NEUT_R PNEUTER
                          OUT__ G27NEUTR PGNEUTR
    G_BILI            95  IN__  NDEPLAR  PDEPLAU  NDEPLAR  PDEPLAV  NGEOMER  PGEOMER  CMATERC  PMATERC
                                NDEPLAR  PTHETAR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                                CEPSINR  UEPSINR  CPESANR  UPESANR  NFORCER  UPFRVOL  CROTATR  UROTATR
                                CEPSINR  VEPSINR  CPESANR  VPESANR  NFORCER  VPFRVOL  CROTATR  VROTATR
                          OUT__ EGTHETA  PGTHETA
    G_BILI_F          95  IN__  NDEPLAR  PDEPLAU  NDEPLAR  PDEPLAV  NGEOMER  PGEOMER  CMATERC  PMATERC
                                NDEPLAR  PTHETAR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                                CEPSINF  UEPSINF  CPESANR  UPESANR  CFORCEF  UPFFVOL  CROTATR  UROTATR
                                CTEMPSR  UTEMPSR  CEPSINF  VEPSINF  CPESANR  VPESANR  CFORCEF  VPFFVOL
                                CROTATR  VROTATR  CTEMPSR  VTEMPSR
                          OUT__ EGTHETA  PGTHETA
    INDIC_ENER        -1  IN__
                          OUT__ XXXXXX   XXXXXX
    INDIC_SEUIL       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    INIT_MAIL_VOIS    99  IN__
                          OUT__ EVOISIN  PVOISIN
    INIT_VARC         99  IN__
                          OUT__ ZVARCPG  PVARCPR
    MASS_INER         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    MASS_MECA         12  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR
                          OUT__ VMATUUR  PMATUUR
    MASS_MECA_DIAG    -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    MASS_MECA_EXPLI   -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    NORME_L2         563  IN__  EMNEUT_I PCALCI   EGNEUT_R PCHAMPG  EMNEUT_R PCOEFR   EGGEOP_R PCOORPG
                          OUT__ ENORME   PNORME
    NSPG_NBVA        496  IN__  CCOMPO2  PCOMPOR
                          OUT__ EDCEL_I  PDCEL_I
    PAS_COURANT      404  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC
                          OUT__ ECOURAN  PCOURAN
    QIRE_ELEM        368  IN__  CCONSTR  PCONSTR  ESIGMNO  PCONTNOD ESIGMNO  PCONTNOP CFORCEF  PFFVOLUD
                                CFORCEF  PFFVOLUP CREFERI  PFORCED  CREFERI  PFORCEP  EFORCER  PFRVOLUD
                                EFORCER  PFRVOLUP NGEOMER  PGEOMER  CPESANR  PPESANRD CPESANR  PPESANRP
                                CREFERI  PPRESSD  CREFERI  PPRESSP  CROTATR  PROTATRD CROTATR  PROTATRP
                                CTEMPSR  PTEMPSR  EVOISIN  PVOISIN
                          OUT__ EERREUR  PERREUR
    QIRE_ELNO        379  IN__  EERREUR  PERREUR
                          OUT__ EERRENO  PERRENO
    RAPH_MECA        595  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                                ZVARIPG  PVARIMP  ZVARIPG  PVARIMR
                          OUT__ ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    REFE_FORC_NODA    -1  IN__
                          OUT__ XXXXXX   XXXXXX
    REPERE_LOCAL     133  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER
                          OUT__ CGEOMER  PREPLO1  CGEOMER  PREPLO2  CGEOMER  PREPLO3
    RICE_TRACEY       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA_ELAS   595  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                                ZVARIPG  PVARIMR
                          OUT__ MMATUUR  PMATUUR
    RIGI_MECA_GE      -1  IN__
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA_RO      -1  IN__
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA_TANG   595  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                                ZVARIPG  PVARIMR
                          OUT__ MMATUUR  PMATUUR
    SIEF_ELGA         97  IN__  CCAMASS  PCAMASS  NDEPLAR  PDEPLAR  NGEOMER  PGEOMER  CMATERC  PMATERC
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                          OUT__ ECONTPC  PCONTRC  ECONTPG  PCONTRR
    SIEF_ELNO          4  IN__  ECONTPG  PCONTRR  ZVARCPG  PVARCPR
                          OUT__ ECONTNC  PSIEFNOC ECONTNO  PSIEFNOR
    SIEQ_ELGA        335  IN__  ESIGMPG  PCONTRR
                          OUT__ ECOEQPG  PCONTEQ
    SIEQ_ELNO        335  IN__  ESIGMNO  PCONTRR
                          OUT__ ECOEQNO  PCONTEQ
    SIGM_ELGA        546  IN__  ESIGMPG  PSIEFR
                          OUT__ ESIGMPC  PSIGMC   ESIGMPG  PSIGMR
    SIGM_ELNO          4  IN__  ESIGMPG  PCONTRR
                          OUT__ ESIGMNC  PSIEFNOC ESIGMNO  PSIEFNOR
    TOU_INI_ELGA      99  IN__
                          OUT__ EGNEUT_F PNEUT_F  EGNEUT_R PNEUT_R  ECONTPG  PSIEF_R  ESOURCR  PSOUR_R
                                ZVARIPG  PVARI_R
    TOU_INI_ELNO      99  IN__
                          OUT__ NGEOMER PGEOM_R  ENNEUT_F PNEUT_F  ENNEUT_R PNEUT_R  ECONTNO  PSIEF_R  ZVARINO  PVARI_R
    VARI_ELNO          4  IN__  ZVARIPG  PVARIGR
                          OUT__ ZVARINO  PVARINR
    VERI_JACOBIEN    328  IN__  NGEOMER  PGEOMER
                          OUT__ ECODRET  PCODRET
    WEIBULL           -1  IN__
                          OUT__ XXXXXX   XXXXXX
