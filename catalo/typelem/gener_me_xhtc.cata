%& LIBRARY TYPELEM
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_ME_XHTC
TYPE_GENE__

%  CATALOGUES DES ELEMENTS X-FEM HEAVISIDE-CRACKTIP AVEC CONTACT (QUE LINEAIRES)

ENTETE__ ELEMENT__ MECA_XHTC_HEXA8        MAILLE__ HEXA8
   ELREFE__  HE8       GAUSS__  RIGI=FPG8    NOEU=NOEU   XFEM=XFEM480  FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG15   NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6   FPG7=FPG7   XCON=FPG12 GAUSS=FPG12 SIMP=SIMP
   ATTRIBUT__  XFEM=XHTC XLAG=NOEUD
   ENS_NOEUD__  EN1     =     1   2   3   4   5   6   7   8
ENTETE__ ELEMENT__ MECA_XHTC_PENTA6       MAILLE__ PENTA6
   ELREFE__  PE6       GAUSS__  RIGI=FPG6    NOEU=NOEU   XFEM=XFEM240  FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG15   NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6   FPG7=FPG7   XCON=FPG12 GAUSS=FPG12 SIMP=SIMP
   ATTRIBUT__  XFEM=XHTC XLAG=NOEUD
   ENS_NOEUD__  EN1     =     1   2   3   4   5   6
ENTETE__ ELEMENT__ MECA_XHTC_PYRAM5      MAILLE__ PYRAM5
   ELREFE__  PY5       GAUSS__  RIGI=FPG5    NOEU=NOEU   XFEM=XFEM180  FPG1=FPG1  FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG15   NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6    FPG7=FPG7  XCON=FPG12 SIMP=SIMP GAUSS=FPG12
   ATTRIBUT__  XFEM=XHTC XLAG=NOEUD
   ENS_NOEUD__  EN1     =     1   2   3   4   5
ENTETE__ ELEMENT__ MECA_XHTC_TETRA4       MAILLE__ TETRA4
   ELREFE__  TE4       GAUSS__  RIGI=FPG1    NOEU=NOEU   XINT=FPG15  FPG1=FPG1  XFEM=XFEM90   FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6  FPG7=FPG7    XCON=FPG12 GAUSS=FPG12 SIMP=SIMP
   ATTRIBUT__  XFEM=XHTC XLAG=NOEUD
   ENS_NOEUD__  EN1     =     1   2   3   4
ENTETE__ ELEMENT__ MECA_XHTC_HE20      MAILLE__ HEXA20
   ELREFE__  H20       GAUSS__  RIGI=FPG27   NOEU=NOEU   XFEM=XFEM480  FPG1=FPG1 FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG15   NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6   FPG7=FPG7   XCON=FPG12 SIMP=SIMP GAUSS=FPG12
   ATTRIBUT__  XFEM=XHTC XLAG=NOEUD
   ENS_NOEUD__  EN1     =     1   2   3   4   5   6   7   8
   ENS_NOEUD__  EN3     =     9  10  11  12  13  14  15  16  17  18  19  20
ENTETE__ ELEMENT__ MECA_XHTC_PE15     MAILLE__ PENTA15
   ELREFE__  P15       GAUSS__  RIGI=FPG21   NOEU=NOEU   XFEM=XFEM240 FPG1=FPG1 FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG15   NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6   FPG7=FPG7   XCON=FPG12 SIMP=SIMP GAUSS=FPG12
   ATTRIBUT__  XFEM=XHTC XLAG=NOEUD
   ENS_NOEUD__  EN1     =     1   2   3   4   5   6
   ENS_NOEUD__  EN3     =     7   8   9  10  11  12  13  14  15
ENTETE__ ELEMENT__ MECA_XHTC_PY13     MAILLE__ PYRAM13
   ELREFE__  P13       GAUSS__  RIGI=FPG27   NOEU=NOEU   XFEM=XFEM180 FPG1=FPG1 FPG_LISTE__ MATER=(RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG15   NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6    FPG7=FPG7  XCON=FPG12 SIMP=SIMP GAUSS=FPG12
   ATTRIBUT__  XFEM=XHTC XLAG=NOEUD
   ENS_NOEUD__  EN1     =     1   2   3   4   5
   ENS_NOEUD__  EN3     =     6   7   8   9  10  11  12  13
ENTETE__ ELEMENT__ MECA_XHTC_TE10     MAILLE__ TETRA10
   ELREFE__  T10       GAUSS__  RIGI=FPG5    NOEU=NOEU   XFEM=XFEM90 FPG1=FPG1 FPG_LISTE__  MATER = (RIGI XFEM NOEU FPG1)
   ELREFE__  TE4       GAUSS__  XINT=FPG15   NOEU=NOEU
   ELREFE__  TR3       GAUSS__  FPG4=FPG4    NOEU=NOEU   FPG6=FPG6  FPG7=FPG7    XCON=FPG12 SIMP=SIMP GAUSS=FPG12
   ATTRIBUT__  XFEM=XHTC XLAG=NOEUD
   ENS_NOEUD__  EN1     =     1   2   3   4
   ENS_NOEUD__  EN3     =     5   6   7   8   9  10


MODE_LOCAL__
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CCAMASS  = CAMASS   ELEM__         (C        ALPHA    BETA     KAPPA    X        Y
                                        Z        )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC   PERTURB
                                        TOLDEBO  ITEDEBO  TSSEUIL TSAMPL TSRETOUR)
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1
                                        XXXX2    KIT1     KIT2     KIT3     KIT4     KIT5
                                        KIT6     KIT7     KIT8     KIT9     )
    CEPSINR  = EPSI_R   ELEM__         (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CONTX_R  = XCONTAC  ELEM__         (RHON     MU       RHOTK    INTEG   COECH    COSTCO   COSTFR
                                        COPECO   COPEFR)
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    CPRESSF  = PRES_F   ELEM__         (PRES     )
    CROTATR  = ROTA_R   ELEM__         (OME      AR       BR       CR       X        Y
                                        Z        )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN1   (DX      DY      DZ      H1X      H1Y      H1Z
                                        E1X      E1Y      E1Z      E2X      E2Y      E2Z
                                        E3X      E3Y      E3Z      E4X      E4Y      E4Z
                                        LAGS_C   LAGS_F1  LAGS_F2  )
                                 EN3   (DX      DY      DZ      H1X      H1Y      H1Z
                                        E1X      E1Y      E1Z      E2X      E2Y      E2Z
                                        E3X      E3Y      E3Z      E4X      E4Y      E4Z      )
    DDL_MECC = DEPL_R   ELNO__ DIFF__
                                 EN1   (DX       DY       DZ       )
                                 EN3   (DX       DY       DZ       )
    E1NEUTR  = NEUT_R   ELEM__         (X1       )
    E1NEUTI  = NEUT_I   ELEM__         (X1       )
    E3NEUTI  = N120_I   ELEM__         (X[3]     )
    E10NEUTI = N120_I   ELEM__         (X[10]    )
    E15NEUTI = N120_I   ELEM__         (X[15]    )
    E21NEUTR = N120_R   ELEM__         (X[21]    )
    E30NEUTR = NEUT_R   ELEM__         (X[30]    )
    E32NEUTI = N128_I   ELEM__         (X[32]    )
    E33NEUTR = N132_R   ELEM__         (X[33]    )
    E35NEUTR = N120_R   ELEM__         (X[35]    )
    E63NEUTR = N120_R   ELEM__         (X[63]    )
    E195NEUT = N480_R   ELEM__         (X[195]   )
    E320NEUI = N512_I   ELEM__         (X[320]   )
    ECODRET  = CODE_I   ELEM__         (IRET     )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTNC  = SIEF_C   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTPG  = SIEF_R   ELGA__ XFEM    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    ECONTPC  = SIEF_C   ELGA__ XFEM    (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    EDEFONO  = EPSI_R   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     EPXZ     EPYZ     )
    EDOMGGA  = DOMA_R   ELGA__ RIGI    (DOMA     )
    EFACY_R  = FACY_R   ELGA__ RIGI    (DTAUM1   VNM1X    VNM1Y    VNM1Z    SINMAX1  SINMOY1
                                        EPNMAX1  EPNMOY1  SIGEQ1   NBRUP1   ENDO1    DTAUM2
                                        VNM2X    VNM2Y    VNM2Z    SINMAX2  SINMOY2  EPNMAX2
                                        EPNMOY2  SIGEQ2   NBRUP2   ENDO2    )
    EGGEOM_R = GEOM_R   ELGA__ XFEM    (X        Y        Z        )
    EGINST_R = INST_R   ELGA__ RIGI    (INST     )
    EGNEUT_F = NEUT_F   ELGA__ XFEM    (X[30]    )
    EGNEUT_R = NEUT_R   ELGA__ XFEM    (X[30]    )
    EKTHETA  = G        ELEM__         (GTHETA   FIC1      FIC2      FIC3
                                        K1       K2        K3        BETA   )
    EGTHETA  = G        ELEM__         (GTHETA   )
    EPRESNO  = PRES_R   ELNO__ IDEN__  (PRES     )
    EREFCO   = PREC     ELEM__         (SIGM     )
    G27NEUTR = NEUT_R   ELGA__ RIGI    (X[27]    )
    N1NEUT_R = NEUT_R   ELNO__ IDEN__  (X1       )
    N3NEUT_R = NEUT_R   ELNO__ IDEN__  (X[3]     )
    N9NEUT_R = NEUT_R   ELNO__ IDEN__  (X[9]     )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        Z        )
    CGEOMER  = GEOM_R   ELEM__         (X        Y        Z        )
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        Z        W       )
    ENORME   = NEUT_R   ELEM__         (X1       )
    EMNEUT_R = NEUT_R   ELEM__         (X[30]     )
    CFORCEF  = FORC_F   ELEM__         (FX       FY       FZ       )
    NFORCER  = FORC_R   ELNO__ IDEN__  (FX       FY       FZ       )
    EFORCER  = FORC_R   ELGA__ XFEM    (FX       FY       FZ       )
    STANO_I  = N120_I   ELNO__ IDEN__  (X1       )
    XFGEOM_R = GEOM_R   ELGA__ XFEM    (X        Y        Z        )
    ZVARIPG  = VARI_R   ELGA__ XFEM    (VARI     )
    E1NEUTK  = NEUT_K8  ELEM__         (Z1       )
    CFREQR   = FREQ_R   ELEM__         (FREQ     )

VECTEUR__
    MVECTUR = VDEP_R DDL_MECA

MATRICE__
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    MMATUNS = MDNS_R DDL_MECA DDL_MECA

OPTION__
    CALC_G           288  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  N9NEUT_R PBASLOR  E15NEUTI PCFACE   
                                E320NEUI PCNSETO  CCOMPOR  PCOMPOR  DDL_MECA PDEPLAR  NFORCER  PFRVOLU  
                                NGEOMER  PGEOMER  E32NEUTI PHEAVTO  E10NEUTI PLONCHA  E3NEUTI  PLONGCO  
                                N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  CPESANR  PPESANR  
                                E21NEUTR PPINTER  E33NEUTR PPINTTO  EPRESNO  PPRESSR  CROTATR  PROTATR  
                                DDL_MECC PTHETAR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                                E195NEUT PPMILTO
                          OUT__ EGTHETA  PGTHETA  
    CALC_G_F         288  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  N9NEUT_R PBASLOR  E15NEUTI PCFACE   
                                E320NEUI PCNSETO  CCOMPOR  PCOMPOR  G27NEUTR PCOURB   DDL_MECA PDEPLAR  
                                CFORCEF  PFFVOLU  NGEOMER  PGEOMER  E32NEUTI PHEAVTO  E10NEUTI PLONCHA  
                                E3NEUTI  PLONGCO  N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  
                                CPESANR  PPESANR  E21NEUTR PPINTER  E33NEUTR PPINTTO  CPRESSF  PPRESSF  
                                CROTATR  PROTATR  CTEMPSR  PTEMPSR  DDL_MECC PTHETAR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  
                                E195NEUT PPMILTO
                          OUT__ EGTHETA  PGTHETA
    CALC_GTP         288  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  N9NEUT_R PBASLOR  E15NEUTI PCFACE   
                                E320NEUI PCNSETO  CCOMPOR  PCOMPOR  DDL_MECA PDEPLAR  NFORCER  PFRVOLU  
                                NGEOMER  PGEOMER  E32NEUTI PHEAVTO  E10NEUTI PLONCHA  E3NEUTI  PLONGCO  
                                N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  CPESANR  PPESANR  
                                E21NEUTR PPINTER  E33NEUTR PPINTTO  EPRESNO  PPRESSR  CROTATR  PROTATR  
                                DDL_MECC PTHETAR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                                E195NEUT PPMILTO
                          OUT__ EGTHETA  PGTHETA  
    CALC_GTP_F       288  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  N9NEUT_R PBASLOR  E15NEUTI PCFACE   
                                E320NEUI PCNSETO  CCOMPOR  PCOMPOR  G27NEUTR PCOURB   DDL_MECA PDEPLAR  
                                CFORCEF  PFFVOLU  NGEOMER  PGEOMER  E32NEUTI PHEAVTO  E10NEUTI PLONCHA  
                                E3NEUTI  PLONGCO  N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  
                                CPESANR  PPESANR  E21NEUTR PPINTER  E33NEUTR PPINTTO  CPRESSF  PPRESSF  
                                CROTATR  PROTATR  CTEMPSR  PTEMPSR  DDL_MECC PTHETAR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  
                                E195NEUT PPMILTO
                          OUT__ EGTHETA  PGTHETA                            
    CALC_K_G         297  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  N9NEUT_R PBASLOR  E15NEUTI PCFACE   
                                E320NEUI PCNSETO  CCOMPOR  PCOMPOR  G27NEUTR PCOURB   DDL_MECA PDEPLAR  
                                NFORCER  PFRVOLU  NGEOMER  PGEOMER  E32NEUTI PHEAVTO  E10NEUTI PLONCHA  
                                E3NEUTI  PLONGCO  N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  
                                CPESANR  PPESANR  E21NEUTR PPINTER  E33NEUTR PPINTTO  EPRESNO  PPRESSR  
                                CFREQR   PPULPRO  CROTATR  PROTATR  DDL_MECC PTHETAR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  
                                E195NEUT PPMILTO
                          OUT__ EKTHETA  PGTHETA  
    CALC_K_G_F       297  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  N9NEUT_R PBASLOR  E15NEUTI PCFACE   
                                E320NEUI PCNSETO  CCOMPOR  PCOMPOR  G27NEUTR PCOURB   DDL_MECA PDEPLAR  
                                CFORCEF  PFFVOLU  NGEOMER  PGEOMER  E32NEUTI PHEAVTO  E10NEUTI PLONCHA  
                                E3NEUTI  PLONGCO  N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  
                                CPESANR  PPESANR  E21NEUTR PPINTER  E33NEUTR PPINTTO  CPRESSF  PPRESSF  
                                CFREQR   PPULPRO  CROTATR  PROTATR  CTEMPSR  PTEMPSR  DDL_MECC PTHETAR  
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                                E195NEUT PPMILTO
                          OUT__ EKTHETA  PGTHETA  
    CARA_GEOM         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CFL_XFEM         118  IN__  NGEOMER  PGEOMER  
                          OUT__ E1NEUTR  PLONCAR  
    CHAR_LIMITE       -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_CONT   534  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  E15NEUTI PCFACE   DDL_MECA PDEPL_M  
                                DDL_MECA PDEPL_P  CONTX_R  PDONCO   NGEOMER  PGEOMER  E1NEUTI  PINDCOI  
                                E3NEUTI  PLONGCO  N1NEUT_R PLST     E21NEUTR PPINTER  E1NEUTR  PSEUIL   
                                STANO_I  PSTANO   
                          OUT__ MVECTUR  PVECTUR  
    CHAR_MECA_EPSA_R  -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_FF3D3D 440  IN__  CFORCEF  PFF3D3D  NGEOMER  PGEOMER  CTEMPSR  PTEMPSR  
                          OUT__ MVECTUR  PVECTUR  
    CHAR_MECA_FR3D3D 440  IN__  E320NEUI PCNSETO  NFORCER  PFR3D3D  NGEOMER  PGEOMER  E32NEUTI PHEAVTO  
                                E10NEUTI PLONCHA  N1NEUT_R PLSN     N1NEUT_R PLST     E33NEUTR PPINTTO  
                                STANO_I  PSTANO   
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_FROT   534  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  E15NEUTI PCFACE   DDL_MECA PDEPL_M  
                                DDL_MECA PDEPL_P  CONTX_R  PDONCO   NGEOMER  PGEOMER  E1NEUTI  PINDCOI  
                                E3NEUTI  PLONGCO  N1NEUT_R PLST     E21NEUTR PPINTER  E1NEUTR  PSEUIL   
                                STANO_I  PSTANO   
                              OUT__  MVECTUR  PVECTUR
    CHAR_MECA_HYDR_R  -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_META_Z  -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_PESA_R 441  IN__  E320NEUI PCNSETO  NGEOMER  PGEOMER  E32NEUTI PHEAVTO  E10NEUTI PLONCHA  
                                N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  CPESANR  PPESANR  
                                E33NEUTR PPINTTO  STANO_I  PSTANO   ZVARCPG  PVARCPR  
                          OUT__ MVECTUR  PVECTUR  
    CHAR_MECA_PRES_F  37  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  E15NEUTI PCFACE   NGEOMER  PGEOMER  
                                E3NEUTI  PLONGCO  N1NEUT_R PLST     E21NEUTR PPINTER  CPRESSF  PPRESSF  
                                STANO_I  PSTANO   CTEMPSR  PTEMPSR  
                          OUT__ MVECTUR  PVECTUR  
    CHAR_MECA_PRES_R  37  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  E15NEUTI PCFACE   NGEOMER  PGEOMER  
                                E3NEUTI  PLONGCO  N1NEUT_R PLST     E21NEUTR PPINTER  EPRESNO  PPRESSR  
                                STANO_I  PSTANO   
                          OUT__ MVECTUR  PVECTUR  
    CHAR_MECA_PTOT_R  -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_ROTA_R 441  IN__  E320NEUI PCNSETO  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER  
                                E32NEUTI PHEAVTO  E10NEUTI PLONCHA  N1NEUT_R PLSN     N1NEUT_R PLST     
                                CMATERC  PMATERC  E33NEUTR PPINTTO  CROTATR  PROTATR  STANO_I  PSTANO   
                          OUT__ MVECTUR  PVECTUR  
    CHAR_MECA_SECH_R  -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_TEMP_R  -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    COOR_ELGA           488   IN__   NGEOMER  PGEOMER
                              OUT__  EGGEOP_R PCOORPG
    ECIN_ELEM         -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    ENEL_ELEM         -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    ENER_TOTALE       -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    EPOT_ELEM         -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    FORC_NODA        542  IN__  N9NEUT_R PBASLOR  E320NEUI PCNSETO  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  
                                DDL_MECA PDEPLMR  NGEOMER  PGEOMER  E32NEUTI PHEAVTO  E10NEUTI PLONCHA  
                                N1NEUT_R PLSN     N1NEUT_R PLST     E33NEUTR PPINTTO  STANO_I  PSTANO   
                                ZVARCPG  PVARCPR  
                              OUT__  MVECTUR  PVECTUR
    FULL_MECA        539  IN__  N9NEUT_R PBASLOR  CCAMASS  PCAMASS  CCARCRI  PCARCRI  E320NEUI PCNSETO  
                                CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  
                                NGEOMER  PGEOMER  E32NEUTI PHEAVTO  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                E10NEUTI PLONCHA  N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  
                                E33NEUTR PPINTTO  STANO_I  PSTANO   ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  ZVARIPG  PVARIMP  ZVARIPG  PVARIMR  
                                E195NEUT PPMILTO
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUUR  PMATUUR  ZVARIPG  PVARIPR
                                     MVECTUR  PVECTUR
    GEOM_FAC         519  IN__  E1NEUTK  NOMFIS   DDL_MECA PDEPLA   E21NEUTR PGESCLO  E3NEUTI  PLONGCO  
                                N1NEUT_R PLST     E21NEUTR PPINTER  
                          OUT__ E63NEUTR PBASESC  E63NEUTR PBASMAI  E21NEUTR PNEWGEM  E21NEUTR PNEWGES  
    GRAD_NEUT9_R     398  IN__  NGEOMER  PGEOMER  N9NEUT_R PNEUTER  
                          OUT__ G27NEUTR PGNEUTR  
    GRAD_NEUT_R         24    IN__   NGEOMER  PGEOMER  N1NEUT_R PNEUTER
                              OUT__  N3NEUT_R PGNEUTR
    INDIC_ENER        -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    INDIC_SEUIL       -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    INIT_VARC           99    IN__
                              OUT__  ZVARCPG  PVARCPR
    MASS_INER         -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    MOY_NOEU_S          118   IN__   N1NEUT_R PNEUTR
                              OUT__  E1NEUTR  PMOYEL
    NORME_L2          -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    NSPG_NBVA           496   IN__   CCOMPO2  PCOMPOR
                              OUT__  EDCEL_I  PDCEL_I
    RAPH_MECA        539  IN__  N9NEUT_R PBASLOR  CCAMASS  PCAMASS  CCARCRI  PCARCRI  E320NEUI PCNSETO  
                                CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  
                                NGEOMER  PGEOMER  E32NEUTI PHEAVTO  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                E10NEUTI PLONCHA  N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  
                                E33NEUTR PPINTTO  STANO_I  PSTANO   ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  ZVARIPG  PVARIMP  ZVARIPG  PVARIMR 
                                E195NEUT PPMILTO
                              OUT__  ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    REFE_FORC_NODA      542   IN__   NGEOMER  PGEOMER  EREFCO   PREFCO
                              OUT__  MVECTUR  PVECTUR
    REPERE_LOCAL     133  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER  
                          OUT__ CGEOMER  PREPLO1  CGEOMER  PREPLO2  CGEOMER  PREPLO3  
    RICE_TRACEY       -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    RIGI_CONT        533  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  E15NEUTI PCFACE   DDL_MECA PDEPL_M  
                                DDL_MECA PDEPL_P  CONTX_R  PDONCO   NGEOMER  PGEOMER  E1NEUTI  PINDCOI  
                                E3NEUTI  PLONGCO  N1NEUT_R PLSN     N1NEUT_R PLST     E21NEUTR PPINTER  
                                     E1NEUTR  PSEUIL   STANO_I  PSTANO
                          OUT__ MMATUNS  PMATUNS  MMATUUR  PMATUUR  
    RIGI_FROT        533  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  E15NEUTI PCFACE   DDL_MECA PDEPL_M  
                                DDL_MECA PDEPL_P  CONTX_R  PDONCO   NGEOMER  PGEOMER  E1NEUTI  PINDCOI  
                                E3NEUTI  PLONGCO  N1NEUT_R PLSN     N1NEUT_R PLST     E21NEUTR PPINTER  
                                     E1NEUTR  PSEUIL   STANO_I  PSTANO
                          OUT__ MMATUNS  PMATUNS  MMATUUR  PMATUUR  
    RIGI_MECA         11  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  
                              OUT__  MMATUUR  PMATUUR
    RIGI_MECA_TANG   539  IN__  N9NEUT_R PBASLOR  CCAMASS  PCAMASS  CCARCRI  PCARCRI  E320NEUI PCNSETO  
                                CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  
                                NGEOMER  PGEOMER  E32NEUTI PHEAVTO  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                E10NEUTI PLONCHA  N1NEUT_R PLSN     N1NEUT_R PLST     CMATERC  PMATERC  
                                E33NEUTR PPINTTO  STANO_I  PSTANO   ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  ZVARIPG  PVARIMR  
                                E195NEUT PPMILTO
                              OUT__  MMATUUR  PMATUUR
    SIEQ_ELGA         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    SIEQ_ELNO         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    SIGM_ELGA           546   IN__   ECONTPG  PSIEFR
                          OUT__ ECONTPC  PSIGMC   ECONTPG  PSIGMR   
    SIGM_ELNO         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    TOPOFA           510  IN__  NGEOMER  PGEOMER  N3NEUT_R PGRADLN  N3NEUT_R PGRADLT  N1NEUT_R PLSN     
                                N1NEUT_R PLST     
                          OUT__ E35NEUTR PAINTER  E63NEUTR PBASECO  E15NEUTI PCFACE   E21NEUTR PGESCLA  
                                E21NEUTR PGESCLO  E21NEUTR PGMAITR  E3NEUTI  PLONGCO  E21NEUTR PPINTER  
    TOPOSE              514   IN__   NGEOMER  PGEOMER  N1NEUT_R PLEVSET
                          OUT__ E320NEUI PCNSETO  E32NEUTI PHEAVTO  E10NEUTI PLONCHA  E33NEUTR PPINTTO
                                E195NEUT PPMILTO
    TOU_INI_ELGA        99    IN__
                              OUT__  EDOMGGA  PDOMMAG  EFACY_R  PFACY_R  EGGEOM_R PGEOM_R  EGINST_R PINST_R
                                     EGNEUT_F PNEUT_F  EGNEUT_R PNEUT_R  ECONTPG  PSIEF_R  ZVARIPG  PVARI_R
    VERI_JACOBIEN       328   IN__   NGEOMER  PGEOMER
                              OUT__  ECODRET  PCODRET
    WEIBULL           -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    XCVBCA           532  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  E15NEUTI PCFACE   DDL_MECA PDEPL_P  
                                CONTX_R  PDONCO   NGEOMER  PGEOMER  E1NEUTI  PGLISS   E1NEUTI  PINDCOI  
                                E3NEUTI  PLONGCO  N1NEUT_R PLST     E1NEUTI  PMEMCON  E21NEUTR PPINTER  
                              OUT__  E1NEUTI  PINCOCA  E1NEUTI  PINDCOO  E1NEUTI  PINDMEM
    XFEM_SMPLX_CALC  118  IN__  E1NEUTR  PGRANDF  N3NEUT_R PGRLS    N1NEUT_R PLSNO    N3NEUT_R PNIELNO  
                          OUT__ N1NEUT_R PALPHA   E1NEUTR  PDPHI    
    XFEM_SMPLX_INIT     118   IN__   NGEOMER  PGEOMER
                              OUT__  E1NEUTR  PMEAST   N3NEUT_R PNIELNO
    XFEM_XPG          46  IN__  E320NEUI PCNSETO  NGEOMER  PGEOMER  E32NEUTI PHEAVTO  E10NEUTI PLONCHA  
                                E33NEUTR PPINTTO  
                                E195NEUT PPMILTO
                              OUT__  XFGEOM_R PXFGEOM
    XREACL           548  IN__  E35NEUTR PAINTER  E63NEUTR PBASECO  E15NEUTI PCFACE   DDL_MECA PDEPL_P  
                                CONTX_R  PDONCO   NGEOMER  PGEOMER  E3NEUTI  PLONGCO  E21NEUTR PPINTER  
                              OUT__  E1NEUTR  PSEUIL
