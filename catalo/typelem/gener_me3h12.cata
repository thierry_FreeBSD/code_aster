%& LIBRARY TYPELEM
% person_in_charge: sylvie.granet at edf.fr
% ======================================================================
% COPYRIGHT (C) 1991 - 2012  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_ME3H12
TYPE_GENE__

ENTETE__ ELEMENT__ HHM_FACE8        MAILLE__ QUAD8
   ELREFE__  QU8       GAUSS__  RIGI=FPG9
   ELREFE__  QU4       GAUSS__  RIGI=FPG9
   ENS_NOEUD__  EN2     =     5  6  7  8
   ENS_NOEUD__  EN1     =     1  2  3  4
ENTETE__ ELEMENT__ HHM_FACE6        MAILLE__ TRIA6
   ELREFE__  TR6       GAUSS__  RIGI=FPG6
   ELREFE__  TR3       GAUSS__  RIGI=FPG6
   ENS_NOEUD__  EN2     =     4  5  6
   ENS_NOEUD__  EN1     =     1  2  3

MODE_LOCAL__
    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN1   (DX       DY       DZ       PRE1     PRE2     )
                                 EN2   (DX       DY       DZ       )
    EFORCNO  = FORC_R   ELNO__ IDEN__  (FX       FY       FZ       )
    CFLUXF   = FTHM_F   ELEM__         (PFLU1    PFLU2    )
    EFLUXE   = FTHM_R   ELGA__ RIGI    (PFLU1    PFLU2    )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        Z        )
    NSIEF_R  = SIEF_R   ELNO__ DIFF__
     EN1   (FH11X FH11Y FH11Z FH12X FH12Y FH12Z
            FH21X FH21Y FH21Z       )
     EN2   (      )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     SIXZ     SIYZ     )
    EPJSIGM  = SIEF_R   ELEM__         (SIG_NX   SIG_NY   SIG_NZ   SIG_N
                                        SIG_TX   SIG_TY   SIG_TZ
                                        SIG_T1X  SIG_T1Y  SIG_T1Z  SIG_T1
                                        SIG_T2X  SIG_T2Y  SIG_T2Z  SIG_T2   )
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        Z        W       )
    CTEMPSR  = INST_R   ELEM__         (INST     DELTAT   THETA    )
    CPRESSF  = PRES_F   ELEM__         (PRES     )
    EPRESNO  = PRES_R   ELNO__ IDEN__  (PRES     )
    EFLHN    = FLHN_R   ELGA__ RIGI    (FH11     FH12     FH21     FH22     )

VECTEUR__
    MVECTUR = VDEP_R DDL_MECA

MATRICE__

OPTION__
    CHAR_MECA_FLUX_F 466  IN__  CFLUXF   PFLUXF   NGEOMER  PGEOMER  CTEMPSR  PTEMPSR  
                          OUT__ MVECTUR  PVECTUR  
    CHAR_MECA_FLUX_R 466  IN__  EFLUXE   PFLUXR   NGEOMER  PGEOMER  CTEMPSR  PTEMPSR  
                          OUT__ MVECTUR  PVECTUR  
    CHAR_MECA_FR2D3D 466  IN__  EFORCNO  PFR2D3D  NGEOMER  PGEOMER  
                          OUT__ MVECTUR  PVECTUR  
    CHAR_MECA_PRES_F 466  IN__  NGEOMER  PGEOMER  CPRESSF  PPRESSF  CTEMPSR  PTEMPSR  
                          OUT__ MVECTUR  PVECTUR  
    CHAR_MECA_PRES_R 466  IN__  NGEOMER  PGEOMER  EPRESNO  PPRESSR  
                          OUT__ MVECTUR  PVECTUR  
    COOR_ELGA        488  IN__  NGEOMER  PGEOMER  
                          OUT__ EGGEOP_R PCOORPG  
    FLHN_ELGA        493  IN__  NSIEF_R  PCONTR   NGEOMER  PGEOMER  
                          OUT__ EFLHN    PFLHN    
    RIGI_MECA         -1  IN__  XXXXXX   XXXXXX   
                          OUT__ XXXXXX   XXXXXX   
    SIRO_ELEM        411  IN__  NGEOMER  PGEOMER  ECONTNO  PSIG3D   
                          OUT__ EPJSIGM  PPJSIGM
