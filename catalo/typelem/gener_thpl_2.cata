%& LIBRARY TYPELEM
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_THPL_2
TYPE_GENE__

ENTETE__ ELEMENT__ THPLQU4          MAILLE__ QUAD4
   ELREFE__  QU4     GAUSS__  RIGI=FPG4   MASS=FPG4 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE2     GAUSS__  RIGI=FPG2
ENTETE__ ELEMENT__ THPLQU8          MAILLE__ QUAD8
   ELREFE__  QU8     GAUSS__  RIGI=FPG9   MASS=FPG9 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE3     GAUSS__  RIGI=FPG4
   OPTION__   GRAD_NEUT_R             -1
ENTETE__ ELEMENT__ THPLQU9          MAILLE__ QUAD9
   ELREFE__  QU9     GAUSS__  RIGI=FPG9   MASS=FPG9 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE3     GAUSS__  RIGI=FPG4
   OPTION__   GRAD_NEUT_R             -1
ENTETE__ ELEMENT__ THPLTR3          MAILLE__ TRIA3
   ELREFE__  TR3     GAUSS__  RIGI=FPG3   MASS=FPG3 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE2     GAUSS__  RIGI=FPG2
ENTETE__ ELEMENT__ THPLTR6          MAILLE__ TRIA6
   ELREFE__  TR6     GAUSS__  RIGI=FPG6   MASS=FPG6 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE3     GAUSS__  RIGI=FPG4
   OPTION__   GRAD_NEUT_R             -1

ENTETE__ ELEMENT__ THAXQU4          MAILLE__ QUAD4
   ELREFE__  QU4     GAUSS__  RIGI=FPG4   MASS=FPG4 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE2     GAUSS__  RIGI=FPG2
   % les elements axisymetriques ne devraient jamais etre utilises pour CARA_XXXX
   % Ce serait mieux d'inventer un autre "code" (-3 par exemple).
   OPTION__   CARA_CISA               -1
   OPTION__   CARA_GAUCHI             -1
   OPTION__   CARA_TORSION            -1
ENTETE__ ELEMENT__ THAXQU8          MAILLE__ QUAD8
   ELREFE__  QU8     GAUSS__  RIGI=FPG9   MASS=FPG9 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE3     GAUSS__  RIGI=FPG4
   OPTION__   CARA_CISA               -1
   OPTION__   CARA_GAUCHI             -1
   OPTION__   CARA_TORSION            -1
   OPTION__   GRAD_NEUT_R             -1
ENTETE__ ELEMENT__ THAXQU9          MAILLE__ QUAD9
   ELREFE__  QU9     GAUSS__  RIGI=FPG9   MASS=FPG9 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE3     GAUSS__  RIGI=FPG4
   OPTION__   CARA_CISA               -1
   OPTION__   CARA_GAUCHI             -1
   OPTION__   CARA_TORSION            -1
   OPTION__   GRAD_NEUT_R             -1
ENTETE__ ELEMENT__ THAXTR3          MAILLE__ TRIA3
   ELREFE__  TR3     GAUSS__  RIGI=FPG3   MASS=FPG3  FPG1=FPG1    NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE2     GAUSS__  RIGI=FPG2
   OPTION__   CARA_CISA               -1
   OPTION__   CARA_GAUCHI             -1
   OPTION__   CARA_TORSION            -1
ENTETE__ ELEMENT__ THAXTR6          MAILLE__ TRIA6
   ELREFE__  TR6     GAUSS__  RIGI=FPG6   MASS=FPG6 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE3     GAUSS__  RIGI=FPG4
   OPTION__   CARA_CISA               -1
   OPTION__   CARA_GAUCHI             -1
   OPTION__   CARA_TORSION            -1
   OPTION__   GRAD_NEUT_R             -1
ENTETE__ ELEMENT__ THAXQL4          MAILLE__ QUAD4
   ELREFE__  QU4     GAUSS__  RIGI=FPG4   MASS=FPG4 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE2     GAUSS__  RIGI=FPG2
   OPTION__   CARA_CISA               -1
   OPTION__   CARA_GAUCHI             -1
   OPTION__   CARA_TORSION            -1
   OPTION__   GRAD_NEUT_R             -1
ENTETE__ ELEMENT__ THAXQL9          MAILLE__ QUAD9
   ELREFE__  QU9     GAUSS__  RIGI=FPG9   MASS=FPG9 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  QU4     GAUSS__  RIGI=FPG4   MASS=FPG4                 NOEU=NOEU
   ELREFE__  SE2     GAUSS__  RIGI=FPG2
   OPTION__   CHAR_THER_TNL           -1
   OPTION__   DURT_ELNO               -1
   OPTION__   ETHE_ELEM               -1
   OPTION__   META_ELNO               -1
   OPTION__   META_INIT_ELNO          -1
   OPTION__   RIGI_THER_CONV_T        -1
   OPTION__   RIGI_THER_TRANS         -1
   OPTION__   SOUR_ELGA               -1
   OPTION__   CARA_CISA               -1
   OPTION__   CARA_GAUCHI             -1
   OPTION__   CARA_TORSION            -1
   OPTION__   GRAD_NEUT_R             -1
ENTETE__ ELEMENT__ THAXTL3          MAILLE__ TRIA3
   ELREFE__  TR3     GAUSS__  RIGI=FPG3   MASS=FPG3 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE2     GAUSS__  RIGI=FPG2
   OPTION__   CARA_CISA               -1
   OPTION__   CARA_GAUCHI             -1
   OPTION__   CARA_TORSION            -1
   OPTION__   GRAD_NEUT_R             -1
ENTETE__ ELEMENT__ THAXTL6          MAILLE__ TRIA6
   ELREFE__  TR6     GAUSS__  RIGI=FPG3   MASS=FPG3 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  TR3     GAUSS__  RIGI=FPG3   MASS=FPG3                 NOEU=NOEU
   ELREFE__  SE3     GAUSS__  RIGI=FPG4
   OPTION__   CHAR_THER_TNL           -1
   OPTION__   DURT_ELNO               -1
   OPTION__   ETHE_ELEM               -1
   OPTION__   META_ELNO               -1
   OPTION__   META_INIT_ELNO          -1
   OPTION__   RIGI_THER_CONV_T        -1
   OPTION__   RIGI_THER_TRANS         -1
   OPTION__   SOUR_ELGA               -1
   OPTION__   CARA_CISA               -1
   OPTION__   CARA_GAUCHI             -1
   OPTION__   CARA_TORSION            -1
   OPTION__   CHAR_THER_SOURNL        -1
   OPTION__   RESI_THER_SOURNL        -1
   OPTION__   MTAN_THER_SOURNL        -1
   OPTION__   GRAD_NEUT_R             -1
ENTETE__ ELEMENT__ THPLQL4          MAILLE__ QUAD4
   ELREFE__  QU4     GAUSS__  RIGI=FPG4   MASS=FPG4 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE2     GAUSS__  RIGI=FPG2
   OPTION__   CARA_CISA               -1
   OPTION__   CARA_GAUCHI             -1
   OPTION__   CARA_TORSION            -1
   OPTION__   GRAD_NEUT_R             -1
ENTETE__ ELEMENT__ THPLQL9          MAILLE__ QUAD9
   ELREFE__  QU9     GAUSS__  RIGI=FPG9   MASS=FPG9 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  QU4     GAUSS__  RIGI=FPG4   MASS=FPG4     NOEU=NOEU
   ELREFE__  SE2     GAUSS__  RIGI=FPG2
   OPTION__   CHAR_THER_TNL           -1
   OPTION__   DURT_ELNO               -1
   OPTION__   ETHE_ELEM               -1
   OPTION__   META_ELNO               -1
   OPTION__   META_INIT_ELNO          -1
   OPTION__   RIGI_THER_CONV_T        -1
   OPTION__   RIGI_THER_TRANS         -1
   OPTION__   SOUR_ELGA               -1
   OPTION__   CARA_CISA               -1
   OPTION__   CARA_GAUCHI             -1
   OPTION__   CARA_TORSION            -1
   OPTION__   CHAR_THER_SOURNL        -1
   OPTION__   RESI_THER_SOURNL        -1
   OPTION__   MTAN_THER_SOURNL        -1
   OPTION__   GRAD_NEUT_R             -1
ENTETE__ ELEMENT__ THPLTL3          MAILLE__ TRIA3
   ELREFE__  TR3     GAUSS__  RIGI=FPG3   MASS=FPG3 FPG1=FPG1     NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  SE2     GAUSS__  RIGI=FPG2
   OPTION__   CARA_CISA               -1
   OPTION__   CARA_GAUCHI             -1
   OPTION__   CARA_TORSION            -1
   OPTION__   GRAD_NEUT_R             -1
ENTETE__ ELEMENT__ THPLTL6          MAILLE__ TRIA6
   ELREFE__  TR6     GAUSS__  RIGI=FPG3   MASS=FPG3  FPG1=FPG1    NOEU=NOEU FPG_LISTE__ MATER = (FPG1)
   ELREFE__  TR3     GAUSS__  RIGI=FPG3   MASS=FPG3     NOEU=NOEU
   ELREFE__  SE3     GAUSS__  RIGI=FPG4
   OPTION__   CHAR_THER_TNL           -1
   OPTION__   DURT_ELNO               -1
   OPTION__   ETHE_ELEM               -1
   OPTION__   META_ELNO               -1
   OPTION__   META_INIT_ELNO          -1
   OPTION__   RIGI_THER_CONV_T        -1
   OPTION__   RIGI_THER_TRANS         -1
   OPTION__   SOUR_ELGA               -1
   OPTION__   CARA_CISA               -1
   OPTION__   CARA_GAUCHI             -1
   OPTION__   CARA_TORSION            -1
   OPTION__   CHAR_THER_SOURNL        -1
   OPTION__   RESI_THER_SOURNL        -1
   OPTION__   MTAN_THER_SOURNL        -1
   OPTION__   GRAD_NEUT_R             -1

MODE_LOCAL__
    ECODRET  = CODE_I   ELEM__         (IRET     )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CFTRC    = ADRSJEVN ELEM__         (I1       I2       )
    CCAMASS  = CAMASS   ELEM__         (C        ALPHA    BETA     KAPPA    X        Y        Z)
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    NVITESR  = DEPL_R   ELNO__ IDEN__  (DX       DY       )
    NDEPLAR  = DEPL_R   ELNO__ IDEN__  (DX       DY       )
    EDURTNO  = DURT_R   ELNO__ IDEN__  (HV       )
    EENERR   = ENER_R   ELEM__         (TOTALE   )
    EERREURT = ERRE_R   ELEM__         (ERTABS   ERTREL   TERMNO   TERMVO   TERMV2   TERMV1
                                        TERMSA   TERMS2   TERMS1   TERMFL   TERMF2   TERMF1
                                        TERMEC   TERME2   TERME1   )
    EERRENOT = ERRE_R   ELNO__ IDEN__  (ERTABS   ERTREL   TERMNO   TERMVO   TERMV2   TERMV1
                                        TERMSA   TERMS2   TERMS1   TERMFL   TERMF2   TERMF1
                                        TERMEC   TERME2   TERME1   )
    CGRAINF  = FLUX_F   ELEM__         (FLUX     FLUY     )
    CGRAINR  = FLUX_R   ELEM__         (FLUX     FLUY     )
    EFLUXPG  = FLUX_R   ELGA__ RIGI    (FLUX     FLUY     )
    EFLUXPN  = FLUX_R   ELNO__ IDEN__  (FLUX     FLUY     )
    EFLUXNO  = FLUX_R   ELNO__ IDEN__  (FLUX     FLUY     )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        )
    CGEOMER  = GEOM_R   ELEM__         (X        Y        )
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        W        )
    ENORME   = NEUT_R   ELEM__         (X1       )
    EGGEOM_R = GEOM_R   ELGA__ RIGI    (X        Y        )
    ENGEOM_R = GEOM_R   ELNO__ IDEN__  (X        Y        )
    EHYDRNO  = HYDR_R   ELNO__ IDEN__  (HYDR     )
    CTEMPSR  = INST_R   ELEM__         (INST     DELTAT   THETA    KHI      R        RHO      )
    ENINST_R = INST_R   ELNO__ IDEN__  (INST     )
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[30]    )
    CDECENT  = NEUT_K24 ELEM__         (Z1       )
    CREFERK  = NEUT_K24 ELEM__         (Z[19]    )
    ECASECT  = NEUT_R   ELEM__         (X[9]     )
    EGNEUT1R = NEUT_R   ELGA__ RIGI    (X1       )
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[30]    )
    EMNEUT_R = NEUT_R   ELEM__         (X[30]    )
    EMNEUT_I = NEUT_I   ELEM__         (X[1]     )
    N1NEUT_R = NEUT_R   ELNO__ IDEN__  (X1       )
    N2NEUT_R = NEUT_R   ELNO__ IDEN__  (X1       X2       )
    CSOURCF  = SOUR_F   ELEM__         (SOUR     )
    CSOURCR  = SOUR_R   ELEM__         (SOUR     )
    ESOURCR  = SOUR_R   ELGA__ RIGI    (SOUR     )
    DDL_THER = TEMP_R   ELNO__ IDEN__  (TEMP     )
    ETEMPPG  = TEMP_R   ELGA__ RIGI    (TEMP     )
    ETEMPNO  = TEMP_R   ELNO__ IDEN__  (TEMP     )
    CPHASIN_ = VAR2_R   ELEM__         (V[5]    )
    EPHASNO_ = VARI_R   ELNO__ IDEN__  (VARI     )
    EVOISIN  = VOISIN   ELEM__         (V0       V1       V2       V3       V4       V5
                                        V6       T0       T1       T2       T3       T4
                                        T5       T6       )
    ENNEUT_F = NEUT_F   ELNO__ IDEN__  (X[30]    )
    ENNEUT_R = NEUT_R   ELNO__ IDEN__  (X[30]    )
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )

VECTEUR__
    MVECTTR = VTEM_R DDL_THER

MATRICE__
    MMATTTR = MTEM_R DDL_THER DDL_THER
    MMATTSR = MTNS_R DDL_THER DDL_THER

OPTION__
    CARA_CISA        509  IN__  NGEOMER  PGEOMER  DDL_THER PTEMPE1  DDL_THER PTEMPE2
                          OUT__ ECASECT  PCASECT
    CARA_GAUCHI      509  IN__  NGEOMER  PGEOMER  DDL_THER PTEMPER
                          OUT__ ECASECT  PCASECT
    CARA_GEOM         -1  IN__  XXXXXX   XXXXXX
                          OUT__ XXXXXX   XXXXXX
    CARA_TORSION     509  IN__  NGEOMER  PGEOMER  DDL_THER PTEMPER
                          OUT__ ECASECT  PCASECT
    CHAR_LIMITE       -1  IN__  XXXXXX   XXXXXX
                          OUT__ XXXXXX   XXXXXX
    CHAR_THER_EVOL    78  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER  CMATERC  PMATERC  DDL_THER PTEMPER
                                CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR
                          OUT__ MVECTTR  PVECTTR
    CHAR_THER_EVOLNI 244  IN__  CCOMPOR  PCOMPOR  NGEOMER  PGEOMER  EHYDRNO  PHYDRPM  CMATERC  PMATERC
                                DDL_THER PTEMPER  CTEMPSR  PTEMPSR  DDL_THER PTMPCHF  DDL_THER PTMPCHI
                                ZVARCPG  PVARCPR
                          OUT__ MVECTTR  PVECTTI  MVECTTR  PVECTTR
    CHAR_THER_GRAI_F 219  IN__  NGEOMER  PGEOMER  CGRAINF  PGRAINF  CMATERC  PMATERC  CTEMPSR  PTEMPSR
                                ZVARCPG  PVARCPR
                          OUT__ MVECTTR  PVECTTR
    CHAR_THER_GRAI_R 219  IN__  NGEOMER  PGEOMER  CGRAINR  PGRAINR  CMATERC  PMATERC  ZVARCPG  PVARCPR
                          OUT__ MVECTTR  PVECTTR
    CHAR_THER_SOURNL 354  IN__  NGEOMER  PGEOMER  CSOURCF  PSOURNL  DDL_THER PTEMPER  CTEMPSR  PTEMPSR
                          OUT__ MVECTTR  PVECTTR
    CHAR_THER_SOUR_F  80  IN__  NGEOMER  PGEOMER  CSOURCF  PSOURCF  CTEMPSR  PTEMPSR
                          OUT__ MVECTTR  PVECTTR
    CHAR_THER_SOUR_R  79  IN__  NGEOMER  PGEOMER  ESOURCR  PSOURCR
                          OUT__ MVECTTR  PVECTTR
    CHAR_THER_TNL    505  IN__  NGEOMER  PGEOMER  EGNEUT1R PLAGRM   CMATERC  PMATERC  DDL_THER PTEMPEI
                                DDL_THER PTEMPER  CTEMPSR  PTEMPSR  NVITESR  PVITESR
                          OUT__ EGNEUT1R PLAGRP   MVECTTR  PRESIDU  MVECTTR  PVECTTR
    COOR_ELGA        479  IN__  NGEOMER  PGEOMER
                          OUT__ EGGEOP_R PCOORPG
    DURT_ELNO        551  IN__  CMATERC  PMATERC  EPHASNO_ PPHASIN
                          OUT__ EDURTNO  PDURT_R
    ECIN_ELEM         -1  IN__  XXXXXX   XXXXXX
                          OUT__ XXXXXX   XXXXXX
    ENEL_ELEM         -1  IN__  XXXXXX   XXXXXX
                          OUT__ XXXXXX   XXXXXX
    ENER_TOTALE       -1  IN__  XXXXXX   XXXXXX
                          OUT__ XXXXXX   XXXXXX
    EPOT_ELEM         -1  IN__  XXXXXX   XXXXXX
                          OUT__ XXXXXX   XXXXXX
    ERTH_ELEM          3  IN__  CREFERK  PCHARG   EFLUXNO  PFLUX_M  EFLUXNO  PFLUX_P  NGEOMER  PGEOMER
                                CMATERC  PMATERC  CSOURCF  PSOURCF  ESOURCR  PSOURCR  DDL_THER PTEMP_M
                                DDL_THER PTEMP_P  EVOISIN  PVOISIN
                          OUT__ EERREURT PERREUR
    ERTH_ELNO        379  IN__  EERREURT PERREUR
                          OUT__ EERRENOT PERRENO
    ETHE_ELEM        220  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  DDL_THER PTEMPER  CTEMPSR  PTEMPSR
                                ZVARCPG  PVARCPR  CCAMASS  PCAMASS
                          OUT__ EENERR   PENERDR
    FLUX_ELGA         69  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER  CMATERC  PMATERC  DDL_THER PTEMPER
                                CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR
                          OUT__ EFLUXPG  PFLUXPG
    FLUX_ELNO          4  IN__  EFLUXPG  PFLUXPG
                          OUT__ EFLUXNO  PFLUXNO
    GRAD_NEUT_R       24  IN__  NGEOMER  PGEOMER  N1NEUT_R PNEUTER
                          OUT__ N2NEUT_R PGNEUTR
    INDIC_ENER        -1  IN__  XXXXXX   XXXXXX
                          OUT__ XXXXXX   XXXXXX
    INDIC_SEUIL       -1  IN__  XXXXXX   XXXXXX
                          OUT__ XXXXXX   XXXXXX
    INIT_MAIL_VOIS    99  IN__
                          OUT__ EVOISIN  PVOISIN
    INIT_VARC         99  IN__
                          OUT__ ZVARCPG  PVARCPR
    MASS_INER         -1  IN__  XXXXXX   XXXXXX
                          OUT__ XXXXXX   XXXXXX
    MASS_THER         77  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR
                          OUT__ MMATTTR  PMATTTR
    META_ELNO         67  IN__  CCOMPOR  PCOMPOR  CFTRC    PFTRC    CMATERC  PMATERC  EPHASNO_ PPHASIN
                                DDL_THER PTEMPAR  DDL_THER PTEMPER  DDL_THER PTEMPIR  CTEMPSR  PTEMPSR
                          OUT__ EPHASNO_ PPHASNOU
    META_INIT_ELNO   320  IN__  CCOMPOR  PCOMPOR  CMATERC  PMATERC  CPHASIN_ PPHASIN  DDL_THER PTEMPER
                          OUT__ EPHASNO_ PPHASNOU
    MTAN_RIGI_MASS   242  IN__  CCOMPOR  PCOMPOR  NGEOMER  PGEOMER  CMATERC  PMATERC  DDL_THER PTEMPEI
                                CTEMPSR  PTEMPSR  DDL_THER PTMPCHF  DDL_THER PTMPCHI  ZVARCPG  PVARCPR
                          OUT__ MMATTTR  PMATTTR
    MTAN_THER_SOURNL 354  IN__  NGEOMER  PGEOMER  CSOURCF  PSOURNL  DDL_THER PTEMPEI  CTEMPSR  PTEMPSR
                          OUT__ MMATTTR  PMATTTR
    NORME_L2         563  IN__  EMNEUT_I PCALCI   EGNEUT_R PCHAMPG  EMNEUT_R PCOEFR   EGGEOP_R PCOORPG
                          OUT__ ENORME   PNORME
    NSPG_NBVA        496  IN__  CCOMPO2  PCOMPOR
                          OUT__ EDCEL_I  PDCEL_I
    REPERE_LOCAL     133  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER
                          OUT__ CGEOMER  PREPLO1  CGEOMER  PREPLO2
    RESI_RIGI_MASS   243  IN__  CCOMPOR  PCOMPOR  NGEOMER  PGEOMER  EHYDRNO  PHYDRPM  CMATERC  PMATERC
                                DDL_THER PTEMPEI  DDL_THER PTEMPER  CTEMPSR  PTEMPSR  DDL_THER PTMPCHF
                                DDL_THER PTMPCHI  ZVARCPG  PVARCPR
                          OUT__ EHYDRNO  PHYDRPP  MVECTTR  PRESIDU
    RESI_THER_SOURNL 354  IN__  NGEOMER  PGEOMER  CSOURCF  PSOURNL  DDL_THER PTEMPEI  CTEMPSR  PTEMPSR
                          OUT__ MVECTTR  PRESIDU
    RICE_TRACEY       -1  IN__  XXXXXX   XXXXXX
                          OUT__ XXXXXX   XXXXXX
    RIGI_THER         76  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR
                                ZVARCPG  PVARCPR
                          OUT__ MMATTTR  PMATTTR
    RIGI_THER_CONV_T 502  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  CDECENT  PNEUK24  DDL_THER PTEMPEI
                                CTEMPSR  PTEMPSR  NVITESR  PVITESR
                          OUT__ MMATTSR  PMATTTR
    RIGI_THER_TRANS  501  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  DDL_THER PTEMPEI  DDL_THER PTEMPER
                                CTEMPSR  PTEMPSR
                          OUT__ MMATTTR  PMATTTR
    SOUR_ELGA        318  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  DDL_THER PTEMPER  CTEMPSR  PTEMPSR
                                ZVARCPG  PVARCPR
                          OUT__ ESOURCR  PSOUR_R
    SYME_MTNS_R      222  IN__  MMATTSR  PNOSYM
                          OUT__ MMATTTR  PSYM
    TOU_INI_ELEM      99  IN__
                          OUT__ CSOURCR  PSOUR_R
    TOU_INI_ELGA      99  IN__
                          OUT__ EFLUXPG  PFLUX_R  EGGEOM_R PGEOM_R  EGNEUT_F PNEUT_F  EGNEUT_R PNEUT_R
                                ESOURCR  PSOUR_R  ETEMPPG  PTEMP_R
    TOU_INI_ELNO      99  IN__
                          OUT__ EFLUXNO  PFLUX_R  ENGEOM_R PGEOM_R  EHYDRNO  PHYDRPM  ENINST_R PINST_R
                                ENNEUT_F PNEUT_F  ENNEUT_R PNEUT_R  EPHASNO_ PVARI_R
    VERI_JACOBIEN    328  IN__  NGEOMER  PGEOMER
                          OUT__ ECODRET  PCODRET
    WEIBULL           -1  IN__  XXXXXX   XXXXXX
                          OUT__ XXXXXX   XXXXXX
