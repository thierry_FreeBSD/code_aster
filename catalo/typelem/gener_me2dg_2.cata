%& LIBRARY TYPELEM
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_ME2DG_2
TYPE_GENE__

ENTETE__ ELEMENT__ MGDPQU8          MAILLE__ QUAD8
   ELREFE__  QU8       GAUSS__  RIGI=FPG9 FPG1=FPG1  MASS=FPG9     NOEU=NOEU   FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  QU4       GAUSS__  RIGI=FPG9  MASS=FPG9    NOEU=NOEU
   ENS_NOEUD__  EN2     =     5  6  7  8
   ENS_NOEUD__  EN1     =     1  2  3  4
ENTETE__ ELEMENT__ MGCPQU8          MAILLE__ QUAD8
   ELREFE__  QU8       GAUSS__  RIGI=FPG9 FPG1=FPG1  MASS=FPG9     NOEU=NOEU   FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  QU4       GAUSS__  RIGI=FPG9  MASS=FPG9    NOEU=NOEU
   ENS_NOEUD__  EN2     =     5  6  7  8
   ENS_NOEUD__  EN1     =     1  2  3  4
ENTETE__ ELEMENT__ MGDPTR6          MAILLE__ TRIA6
   ELREFE__  TR6       GAUSS__  RIGI=FPG3 FPG1=FPG1  MASS=FPG3     NOEU=NOEU   FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  TR3       GAUSS__  RIGI=FPG3  MASS=FPG3    NOEU=NOEU
   ENS_NOEUD__  EN2     =     4  5  6
   ENS_NOEUD__  EN1     =     1  2  3
ENTETE__ ELEMENT__ MGCPTR6          MAILLE__ TRIA6
   ELREFE__  TR6       GAUSS__  RIGI=FPG3 FPG1=FPG1  MASS=FPG3     NOEU=NOEU   FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  TR3       GAUSS__  RIGI=FPG3  MASS=FPG3    NOEU=NOEU
   ENS_NOEUD__  EN2     =     4  5  6
   ENS_NOEUD__  EN1     =     1  2  3

MODE_LOCAL__
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CCAMASS  = CAMASS   ELEM__         (C        ALPHA    )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC   PERTURB
                                        TOLDEBO  ITEDEBO  TSSEUIL TSAMPL TSRETOUR)
    ECODRET  = CODE_I   ELEM__         (IRET     )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1
                                        XXXX2    KIT[9])
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    NDEPLAR  = DEPL_R   ELNO__ IDEN__  (DX       DY       )
    DDL_MECA = DEPL_R   ELNO__ DIFF__
                                 EN1   (DX       DY       EPXX     EPYY     EPZZ     EPXY     )
                                 EN2   (DX       DY       )
    EDEULNO  = DEPL_R   ELNO__ IDEN__  (DX       DY       )
    EDOMGGA  = DOMA_R   ELGA__ RIGI    (DOMA     )
    EDOMGNO  = DOMA_R   ELNO__ IDEN__  (DOMA     )
    EENERR   = ENER_R   ELEM__         (TOTALE   )
    EENERPG  = ENER_R   ELGA__ RIGI    (TOTALE   )
    EENERNO  = ENER_R   ELNO__ IDEN__  (TOTALE   )
    CEPSINF  = EPSI_F   ELEM__         (EPXX     EPYY     EPZZ     EPXY     )
    CEPSINR  = EPSI_R   ELEM__         (EPXX     EPYY     EPZZ     EPXY     )
    EDEFOPG  = EPSI_R   ELGA__ RIGI    (EPXX     EPYY     EPZZ     EPXY     )
    EDEFOPC  = EPSI_C   ELGA__ RIGI    (EPXX     EPYY     EPZZ     EPXY     )
    EDFEQPG  = EPSI_R   ELGA__ RIGI    (INVA_2   PRIN_1   PRIN_2   PRIN_3   INVA_2SG VECT_1_X
                                        VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z VECT_3_X
                                        VECT_3_Y VECT_3_Z )
    EDFEQNO  = EPSI_R   ELNO__ IDEN__  (INVA_2   PRIN_1   PRIN_2   PRIN_3   INVA_2SG VECT_1_X
                                        VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z VECT_3_X
                                        VECT_3_Y VECT_3_Z )
    EDEFONO  = EPSI_R   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     )
    EDEFONC  = EPSI_C   ELNO__ IDEN__  (EPXX     EPYY     EPZZ     EPXY     )
    EDFVCPG  = EPSI_R   ELGA__ RIGI    (EPTHER_L EPTHER_T EPTHER_N EPSECH   EPHYDR  EPPTOT)
    EDFVCNO  = EPSI_R   ELNO__ IDEN__  (EPTHER_L EPTHER_T EPTHER_N EPSECH   EPHYDR  EPPTOT)
    CFORCEF  = FORC_F   ELEM__         (FX       FY       )
    NFORCER  = FORC_R   ELNO__ IDEN__  (FX       FY       )
    EFORCER  = FORC_R   ELGA__ RIGI    (FX       FY       )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        )
    CGEOMER  = GEOM_R   ELEM__         (X        Y        )
    EGGEOM_R = GEOM_R   ELGA__ RIGI    (X        Y        )
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        W        )
    ENORME   = NEUT_R   ELEM__         (X1       )
    EMNEUT_R = NEUT_R   ELEM__         (X[30]    )
    ENGEOM_R = GEOM_R   ELNO__ IDEN__  (X        Y        )
    EGRDUPG  = G_DEPL_R ELGA__ RIGI    (D_DX_X   D_DX_Y   D_DY_X   D_DY_Y   )
    EGRDUNO  = G_DEPL_R ELNO__ IDEN__  (D_DX_X   D_DX_Y   D_DY_X   D_DY_Y   )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    EGINST_R = INST_R   ELGA__ RIGI    (INST     )
    ENINST_R = INST_R   ELNO__ IDEN__  (INST     )
    ECARAGE  = MASS_R   ELEM__         (M        CDGX     CDGY     CDGZ     IXX      IYY
                                        IZZ      IXY      IXZ      IYZ      IXR2     IYR2     )
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[30])
    CSOUSOP  = NEUT_K24 ELEM__         (Z1       )
    ECOURAN  = NEUT_R   ELEM__         (X1       )
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[30]    )
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    CTYPEPI  = PILO_K   ELEM__         (TYPE     )
    CBORNPI  = PILO_R   ELEM__         (A0       A1       )
    CCDTAU   = PILO_R   ELEM__         (A0       )
    ECOPILO  = PILO_R   ELGA__ RIGI    (A0       A1       A2       A3       ETA      )
    ECONTPG  = SIEF_R   ELGA__ RIGI    (SIXX     SIYY     SIZZ     SIXY     )
    ECONTPC  = SIEF_C   ELGA__ RIGI    (SIXX     SIYY     SIZZ     SIXY     )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     )
    ECONTNC  = SIEF_C   ELNO__ IDEN__  (SIXX     SIYY     SIZZ     SIXY     )
    ECOEQPG  = SIEF_R   ELGA__ RIGI    (VMIS     TRESCA   PRIN_1   PRIN_2   PRIN_3   VMIS_SG
                                        VECT_1_X VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z
                                        VECT_3_X VECT_3_Y VECT_3_Z TRSIG    TRIAX    )
    ECOEQNO  = SIEF_R   ELNO__ IDEN__  (VMIS     TRESCA   PRIN_1   PRIN_2   PRIN_3   VMIS_SG
                                        VECT_1_X VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z
                                        VECT_3_X VECT_3_Y VECT_3_Z TRSIG    TRIAX    )
    EDERAPG  = DERA_R   ELGA__ RIGI    (DCHA_V   DCHA_T   RADI_V   ERR_RADI   )
    EDERANO  = DERA_R   ELNO__ IDEN__  (DCHA_V   DCHA_T   RADI_V   ERR_RADI   )
    ZVARIPG  = VARI_R   ELGA__ RIGI    (VARI     )
    ZVARINO  = VARI_R   ELNO__ IDEN__  (VARI     )
    EVOISIN  = VOISIN   ELEM__         (V0       V1       V2       V3       V4       V5
                                        V6       T0       T1       T2       T3       T4
                                        T5       T6       )
    CITERAT  = NEUT_R   ELEM__         (X1       )
    ENNEUT_F = NEUT_F   ELNO__ IDEN__  (X[30]    )
    ENNEUT_R = NEUT_R   ELNO__ IDEN__  (X[30]    )

VECTEUR__
    MVECTUR = VDEP_R DDL_MECA
    MVECTDR = VDEP_R NDEPLAR

MATRICE__
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    MMATUNS = MDNS_R DDL_MECA DDL_MECA

OPTION__
    ADD_SIGM         581  IN__  ECONTPG  PEPCON1  ECONTPG  PEPCON2  
                          OUT__ ECONTPG  PEPCON3  
    AMOR_MECA         50  IN__  NGEOMER  PGEOMER  MMATUUR  PMASSEL  CMATERC  PMATERC  MMATUNS  PRIGINS  
                                ZVARCPG  PVARCPR  
                          OUT__ MMATUNS  PMATUNS  
    CALC_ESTI_ERRE    -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CALC_NOEU_BORD   290  IN__  NGEOMER  PGEOMER  
                          OUT__ MVECTDR  PVECTUR  
    CARA_GEOM        285  IN__  NGEOMER  PGEOMER  
                          OUT__ ECARAGE  PCARAGE  
    CHAR_LIMITE       -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_EPSA_R 421  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_EPSI_F 284  IN__  CCAMASS  PCAMASS  CEPSINF  PEPSINF  NGEOMER  PGEOMER  CMATERC  PMATERC  
                                CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_EPSI_R 284  IN__  CCAMASS  PCAMASS  CEPSINR  PEPSINR  NGEOMER  PGEOMER  CMATERC  PMATERC  
                                ZVARCPG  PVARCPR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_FF2D2D  94  IN__  CFORCEF  PFF2D2D  NGEOMER  PGEOMER  CTEMPSR  PTEMPSR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_FR2D2D  93  IN__  NFORCER  PFR2D2D  NGEOMER  PGEOMER  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_HYDR_R 492  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR  
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_PESA_R  85  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR  ZVARCPG  PVARCPR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_ROTA_R  -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    CHAR_MECA_SECH_R 492  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR  
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                          OUT__ MVECTDR  PVECTUR  
    CHAR_MECA_TEMP_R  83  IN__  CCAMASS  PCAMASS  CCOMPOR  PCOMPOR  NGEOMER  PGEOMER  CMATERC  PMATERC  
                                CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                          OUT__ MVECTDR  PVECTUR  
    COOR_ELGA        479  IN__  NGEOMER  PGEOMER  
                          OUT__ EGGEOP_R PCOORPG  
    ECIN_ELEM         -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    ENEL_ELEM        490  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTPR  NDEPLAR  PDEPLR   NGEOMER  PGEOMER  
                                CMATERC  PMATERC  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARIPG  PVARIPR  
                          OUT__ EENERR   PENERD1  
    ENEL_ELGA        575  IN__  CCAMASS  PCAMASS  CCOMPOR  PCOMPOR  ECONTPG  PCONTRR  DDL_MECA PDEPLAR  
                                NGEOMER  PGEOMER  CMATERC  PMATERC  CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  ZVARIPG  PVARIGR  
                          OUT__ EENERPG  PENERDR  
    ENEL_ELNO          4  IN__  EENERPG  PENERPG  
                          OUT__ EENERNO  PENERNO  
    ENER_TOTALE      490  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  ECONTPG  PCONTPR  NDEPLAR  PDEPLM   
                                NDEPLAR  PDEPLR   NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  ZVARIPG  PVARIPR  
                          OUT__ EENERR   PENERD1  
    EPEQ_ELGA        335  IN__  EDEFOPG  PDEFORR  
                          OUT__ EDFEQPG  PDEFOEQ  
    EPEQ_ELNO        335  IN__  EDEFONO  PDEFORR  
                          OUT__ EDFEQNO  PDEFOEQ  
    EPME_ELGA         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    EPME_ELNO         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    EPMG_ELGA         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    EPMG_ELNO         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    EPMQ_ELGA        335  IN__  EDEFOPG  PDEFORR  
                          OUT__ EDFEQPG  PDEFOEQ  
    EPMQ_ELNO        335  IN__  EDEFONO  PDEFORR  
                          OUT__ EDFEQNO  PDEFOEQ  
    EPOT_ELEM        286  IN__  CCAMASS  PCAMASS  NDEPLAR  PDEPLAR  NGEOMER  PGEOMER  CMATERC  PMATERC  
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                          OUT__ EENERR   PENERDR  
    EPSG_ELGA         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    EPSG_ELNO         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    EPSI_ELGA         87  IN__  CCOMPOR  PCOMPOR  NDEPLAR  PDEPLAR  NGEOMER  PGEOMER  CMATERC  PMATERC  
                                CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                          OUT__ EDEFOPC  PDEFOPC  EDEFOPG  PDEFOPG  
    EPSI_ELNO          4  IN__  EDEFOPG  PDEFOPG  
                          OUT__ EDEFONC  PDEFONC  EDEFONO  PDEFONO  
    EPSP_ELGA        334  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTRR  NDEPLAR  PDEPLAR  NGEOMER  PGEOMER  
                                CMATERC  PMATERC  CTEMPSR  PTEMPSR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                                ZVARIPG  PVARIGR  
                          OUT__ EDEFOPG  PDEFOPG  
    EPSP_ELNO          4  IN__  EDEFOPG  PDEFOPG  
                          OUT__ EDEFONO  PDEFONO  
    EPVC_ELGA        529  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                          OUT__ EDFVCPG  PDEFOPG  
    EPVC_ELNO          4  IN__  EDFVCPG  PDEFOPG  
                          OUT__ EDFVCNO  PDEFONO  
    ERME_ELEM         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    ERME_ELNO         -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    FORC_NODA          7  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  NGEOMER  PGEOMER  
                                ZVARCPG  PVARCPR  
                          OUT__ MVECTUR  PVECTUR  
    FULL_MECA        113  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                CITERAT  PITERAT  CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  ZVARIPG  PVARIMP  ZVARIPG  PVARIMR  
                          OUT__ ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUNS  PMATUNS  ZVARIPG  PVARIPR  
                                MVECTUR  PVECTUR  
    FULL_MECA_ELAS   113  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                                ZVARIPG  PVARIMP  ZVARIPG  PVARIMR  
                          OUT__ ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUNS  PMATUNS  ZVARIPG  PVARIPR  
                                MVECTUR  PVECTUR  
    INDIC_ENER       490  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTPR  NDEPLAR  PDEPLR   NGEOMER  PGEOMER  
                                CMATERC  PMATERC  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARIPG  PVARIPR  
                          OUT__ EENERR   PENERD1  EENERR   PENERD2  
    INDIC_SEUIL      490  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTPR  NDEPLAR  PDEPLR   NGEOMER  PGEOMER  
                                CMATERC  PMATERC  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARIPG  PVARIPR  
                          OUT__ EENERR   PENERD1  EENERR   PENERD2  
    INIT_MAIL_VOIS    99  IN__  
                          OUT__ EVOISIN  PVOISIN  
    INIT_VARC         99  IN__  
                          OUT__ ZVARCPG  PVARCPR  
    MASS_INER         -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    MASS_MECA        113  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  
                          OUT__ MMATUUR  PMATUUR  
    MASS_MECA_DIAG   113  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  
                          OUT__ MMATUUR  PMATUUR  
    MASS_MECA_EXPLI  113  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  ZVARCPG  PVARCPR  
                          OUT__ MMATUUR  PMATUUR  
    MASS_ZZ1          -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    M_GAMMA           -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    NORME_L2          -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    NSPG_NBVA        496  IN__  CCOMPO2  PCOMPOR  
                          OUT__ EDCEL_I  PDCEL_I  
    PAS_COURANT      405  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  
                          OUT__ ECOURAN  PCOURAN  
    PILO_PRED_DEFO   543  IN__  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  NDEPLAR  PDDEPLR  NDEPLAR  PDEPL0R  
                                NDEPLAR  PDEPL1R  NDEPLAR  PDEPLMR  NGEOMER  PGEOMER  CMATERC  PMATERC  
                                CTYPEPI  PTYPEPI  ZVARIPG  PVARIMR  
                          OUT__ ECOPILO  PCOPILO  
    PILO_PRED_ELAS   544  IN__  CBORNPI  PBORNPI  CCDTAU   PCDTAU   CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  
                                DDL_MECA PDDEPLR  DDL_MECA PDEPL0R  DDL_MECA PDEPL1R  DDL_MECA PDEPLMR  
                                NGEOMER  PGEOMER  CMATERC  PMATERC  CTYPEPI  PTYPEPI  ZVARIPG  PVARIMR  
                          OUT__ ECOPILO  PCOPILO  
    RAPH_MECA        113  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                CITERAT  PITERAT  CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  ZVARIPG  PVARIMP  ZVARIPG  PVARIMR  
                          OUT__ ECODRET  PCODRET  ECONTPG  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR  
    REFE_FORC_NODA    -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    REPERE_LOCAL     133  IN__  CCAMASS  PCAMASS  NGEOMER  PGEOMER  
                          OUT__ CGEOMER  PREPLO1  CGEOMER  PREPLO2  
    RICE_TRACEY       -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    RIGI_MECA_ELAS   113  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  
                                ZVARIPG  PVARIMR  
                          OUT__ MMATUNS  PMATUNS  
    RIGI_MECA_GE      -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    RIGI_MECA_HYST    -1  IN__  ZVARCPG  PVARCPR  
                          OUT__ XXXXXX   XXXXXX   
    RIGI_MECA_RO      -1  IN__  
                          OUT__ XXXXXX   XXXXXX   
    RIGI_MECA_TANG   113  IN__  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR  DDL_MECA PDEPLMR  
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR  
                                CITERAT  PITERAT  CMATERC  PMATERC  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  
                                ZVARCPG  PVARCRR  ZVARIPG  PVARIMR  
                          OUT__ MMATUNS  PMATUNS  
    SIEF_ELNO          4  IN__  ECONTPG  PCONTRR  ZVARCPG  PVARCPR  
                          OUT__ ECONTNC  PSIEFNOC ECONTNO  PSIEFNOR 
    SIEQ_ELGA        335  IN__  ECONTPG  PCONTRR  
                          OUT__ ECOEQPG  PCONTEQ  
    SIEQ_ELNO        335  IN__  ECONTNO  PCONTRR  
                          OUT__ ECOEQNO  PCONTEQ  
    SIGM_ELGA        546  IN__  ECONTPG  PSIEFR   
                          OUT__ ECONTPC  PSIGMC   ECONTPG  PSIGMR   
    SIGM_ELNO          4  IN__  ECONTPG  PCONTRR  
                          OUT__ ECONTNC  PSIEFNOC ECONTNO  PSIEFNOR 
    SYME_MDNS_R      222  IN__  MMATUNS  PNOSYM   
                          OUT__ MMATUUR  PSYM     
    TOU_INI_ELGA      99  IN__  
                          OUT__ EDOMGGA  PDOMMAG  EGGEOM_R PGEOM_R  EGINST_R PINST_R  EGNEUT_F PNEUT_F  
                                EGNEUT_R PNEUT_R  ECONTPG  PSIEF_R  ZVARIPG  PVARI_R  
    TOU_INI_ELNO      99  IN__  
                          OUT__ EDOMGNO  PDOMMAG  EDEFONO  PEPSI_R  ENGEOM_R PGEOM_R  ENINST_R PINST_R  
                                ENNEUT_F PNEUT_F  ENNEUT_R PNEUT_R  ECONTNO  PSIEF_R  ZVARINO  PVARI_R  
    VARI_ELNO          4  IN__  ZVARIPG  PVARIGR  
                          OUT__ ZVARINO  PVARINR  
    VERI_JACOBIEN    328  IN__  NGEOMER  PGEOMER  
                          OUT__ ECODRET  PCODRET  
    WEIBULL           -1  IN__  
                          OUT__ XXXXXX   XXXXXX
