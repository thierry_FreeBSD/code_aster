%& LIBRARY TYPELEM
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_MEPMF2
TYPE_GENE__

ENTETE__ ELEMENT__ MECA_POU_D_TGM   MAILLE__ SEG2
   ELREFE__  SE2    GAUSS__    RIGI=FPG3  FPG1=FPG1  FPG_LISTE__  MATER = (RIGI FPG1)

MODE_LOCAL__
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CABSCUR  = ABSC_R   ELEM__         (ABSC1    ABSC2    )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    ECAFIEL  = CAFI_R   ELEM__         (XG       YG       AIRE     )
    CCAGEPO  = CAGEPO   ELEM__         (R1       EP1      )
    CCAGNPO  = CAGNPO   ELEM__         (A1       IY1      IZ1      AY1      AZ1      EY1
                                        EZ1      JX1      RY1      RZ1      RT1      JG1
                                        IY2      IZ2      AY2      AZ2      EY2      EZ2
                                        JX2      RY2      RZ2      RT2      TVAR     )
    CCAGNP1  = CAGNPO   ELEM__         (A1       IY1      IZ1      AY1      AZ1      EY1
                                        EZ1      JX1      JG1      IYR21    IZR21    )
    CCAGNP2  = CAGNPO   ELEM__         (A1       IY1      IZ1      AY1      AZ1      EY1
                                        EZ1      JX1      RY1      RZ1      RT1      JG1
                                        IYR21    IZR21    IY2      IZ2      AY2      AZ2
                                        EY2      EZ2      JX2      RY2      RZ2      RT2
                                        TVAR     )
    CCAORIE  = CAORIE   ELEM__         (ALPHA    BETA     GAMMA    )
    CCARCRI  = CARCRI   ELEM__         (ITECREL  MACOMP   RESCREL  THETA    ITEDEC   INTLOC   PERTURB
                                        TOLDEBO  ITEDEBO)
    ECODRET  = CODE_I   ELEM__         (IRET     )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   XXXX1
                                        XXXX2    KIT1     KIT2     KIT3     KIT4     KIT5
                                        KIT6     KIT7     KIT8     KIT9     )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    NDEPLAC  = DEPL_C   ELNO__ IDEN__  (DX       DY       DZ       DRX      DRY      DRZ
                                        GRX      )
    DDL_MECA = DEPL_R   ELNO__ IDEN__  (DX       DY       DZ       DRX      DRY      DRZ
                                        GRX      )
    ECOURAN  = NEUT_R   ELEM__         (X1       )
    CITERAT  = NEUT_R   ELEM__         (X1       )
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[30]    )
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[30]    )
    EENEUT_F = NEUT_F   ELNO__ IDEN__  (X[30]    )
    EENEUT_R = NEUT_R   ELNO__ IDEN__  (X[30]    )
    EMNEUT_R = NEUT_R   ELGA__ MATER   (X[1]     )
    ETEMPMA  = TEMP_R   ELGA__ MATER   (TEMP     )
    EHYDRMA  = HYDR_R   ELGA__ MATER   (HYDR     )
    ENONLIN  = NEUT_I   ELEM__         (X1)
    NVITER   = DEPL_R   ELNO__ IDEN__  (DX       DY       DZ       )
    EENEDNO  = ENER_R   ELEM__         (TOTALE   TRAC_COM TORSION  FLEX_Y   FLEX_Z   )
    CEPSINR  = EPSI_R   ELEM__         (EPX      KY       KZ       )
    EDEFGNO  = EPSI_R   ELNO__ IDEN__  (EPXX     GAXY     GAXZ     GAT      KY       KZ
                                        GAX      )
    EDEFGPG  = EPSI_R   ELGA__ RIGI    (EPXX     GAXY     GAXZ     GAT      KY       KZ
                                        GAX      )
    CFLAPLA  = FLAPLA   ELEM__         (NOMAIL   NOGEOM   )
    CFRELEC  = FELECR   ELEM__         (X1       Y1       Z1       X2       Y2       Z2
                                        CODE     )
    CFORCEC  = FORC_C   ELEM__         (FX       FY       FZ       MX       MY       MZ
                                        REP      )
    CFORCEF  = FORC_F   ELEM__         (FX       FY       FZ       MX       MY       MZ
                                        REP      )
    CFORCER  = FORC_R   ELEM__         (FX       FY       FZ       MX       MY       MZ
                                        REP      )
    EGGEOM_R = GEOM_R   ELGA__ RIGI    (X        Y        Z        )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        Z        )
    CGEOMER  = GEOM_R   ELEM__         (X        Y        Z        )
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        Z        W       )
    EGGEMA_R = GEOM_R   ELGA__ MATER   (X        Y        Z        )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    CLISTMA  = LISTMA   ELEM__         (LISTMA   TRANS    )
    EMASSINE = MASS_R   ELEM__         (M        CDGX     CDGY     CDGZ     IXX      IYY
                                        IZZ      IXY      IXZ      IYZ      )
    ENBSP_I  = NBSP_I   ELEM__         (NBFIBR   NBGRFI   NUG1     NUG2     NUG3     NUG4
                                        NUG5     NUG6     NUG7     NUG8     NUG9     NUG10    )
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    CROTATR  = ROTA_R   ELEM__         (OME    AR     BR     CR     X      Y     Z  )
    EEFGENO  = SIEF_R   ELNO__ IDEN__  (N        VY       VZ       MT       MFY      MFZ
                                        BX       )
    EEFGENC  = SIEF_C   ELNO__ IDEN__  (N        VY       VZ       MT       MFY      MFZ
                                        BX       )
    ECONTPG  = SIEF_R   ELGA__ RIGI    (SIXX     )
    ECONTPC  = SIEF_C   ELGA__ RIGI    (SIXX     )
    ECONTNO  = SIEF_R   ELNO__ IDEN__  (SIXX     )
    ECONTNC  = SIEF_C   ELNO__ IDEN__  (SIXX     )
    ECOEQPG  = SIEF_R   ELGA__ RIGI    (VMIS     TRESCA   PRIN_1   PRIN_2   PRIN_3   VMIS_SG
                                        VECT_1_X VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z
                                        VECT_3_X VECT_3_Y VECT_3_Z TRSIG    TRIAX    )
    ECOEQNO  = SIEF_R   ELNO__ IDEN__  (VMIS     TRESCA   PRIN_1   PRIN_2   PRIN_3   VMIS_SG
                                        VECT_1_X VECT_1_Y VECT_1_Z VECT_2_X VECT_2_Y VECT_2_Z
                                        VECT_3_X VECT_3_Y VECT_3_Z TRSIG    TRIAX    )
    ZVARIPG  = VARI_R   ELGA__ RIGI    (VARI     )
    ZVARINO  = VARI_R   ELNO__ IDEN__  (VARI     )
    CVENTCX  = VENTCX_F ELEM__         (FCXP     )
    EREFCO   = PREC     ELEM__         (EFFORT   MOMENT   )
    EGINST_R = INST_R   ELGA__ RIGI    (INST     )
    EEINST_R = INST_R   ELNO__ IDEN__  (INST     )
    ESTRAUX  = STRX_R   ELGA__ RIGI    (N        VY       VZ       MT       MFY      MFZ
                                        BX       EPXX     GAXY     GAXZ     GAT      KY
                                        KZ       GAX      DXINT    ALPHA    BETA     GAMMA   )
    EGAMIMA  = SPMX_R   ELGA__ RIGI    (VAL      NUCOU    NUSECT   NUFIBR   POSIC   POSIS    )
    ENOMIMA  = SPMX_R   ELNO__ IDEN__  (VAL      NUCOU    NUSECT   NUFIBR   POSIC   POSIS    )
    ESIMXNO  = SIEFMX_R ELNO__ IDEN__  (SIXXMIN     SIXXMAX)
    ESIMXNC  = SIEFMX_C ELNO__ IDEN__  (SIXXMIN     SIXXMAX)

VECTEUR__
    MVECTUC = VDEP_C NDEPLAC
    MVECTUR = VDEP_R DDL_MECA

MATRICE__
    MMATUUC = MDEP_C NDEPLAC NDEPLAC
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    MMATUNS = MDNS_R DDL_MECA DDL_MECA

OPTION__
    ADD_SIGM         581  IN__  ECONTPG  PEPCON1  ECONTPG  PEPCON2
                          OUT__ ECONTPG  PEPCON3
    AMOR_MECA         50  IN__  NGEOMER  PGEOMER  MMATUUR  PMASSEL  CMATERC  PMATERC  MMATUUR  PRIGIEL
                                ZVARCPG  PVARCPR
                          OUT__ MMATUUR  PMATUUR
    CARA_GEOM         -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_LIMITE       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_EPSI_R  20  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CEPSINR  PEPSINR  ECAFIEL  PFIBRES
                                NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR
                                CCOMPOR  PCOMPOR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_FC1D1D 150  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CFORCEC  PFC1D1D  NGEOMER  PGEOMER
                          OUT__ MVECTUC  PVECTUC
    CHAR_MECA_FF1D1D 150  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CFORCEF  PFF1D1D  NGEOMER  PGEOMER
                                CTEMPSR  PTEMPSR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_FR1D1D 150  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CFORCER  PFR1D1D  NGEOMER  PGEOMER
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_FRELEC 145  IN__  CFRELEC  PFRELEC  NGEOMER  PGEOMER
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_FRLAPL 148  IN__  CFLAPLA  PFLAPLA  NGEOMER  PGEOMER  CLISTMA  PLISTMA
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_HYDR_R 312  IN__  CMATERC  PMATERC  ZVARCPG  PVARCPR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_PESA_R 150  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECAFIEL  PFIBRES
                                NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I  CPESANR  PPESANR
                                ZVARCPG  PVARCPR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_ROTA_R 150  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECAFIEL  PFIBRES
                                NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I  CROTATR  PROTATR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_SECH_R 312  IN__  CMATERC  PMATERC  ZVARCPG  PVARCPR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_SF1D1D 150  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                CFORCEF  PFF1D1D  NGEOMER  PGEOMER  CTEMPSR  PTEMPSR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_SR1D1D 150  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                NGEOMER  PGEOMER  CVENTCX  PVENTCX  NVITER   PVITER
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_TEMP_R 150  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECAFIEL  PFIBRES
                                NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR
                                ZVARCPG  PVARCRR
                          OUT__ MVECTUR  PVECTUR
    COOR_ELGA        478  IN__  CCAORIE  PCAORIE  ECAFIEL  PFIBRES  NGEOMER  PGEOMER  ENBSP_I  PNBSP_I
                          OUT__ EGGEOP_R PCOORPG
    COOR_ELGA_MATER  463  IN__  CCAORIE  PCAORIE  ECAFIEL  PFIBRES  NGEOMER  PGEOMER  ENBSP_I  PNBSP_I
                          OUT__ EGGEMA_R PCOOPGM
    DEGE_ELGA        -1  IN__
                          OUT__ XXXXXX   XXXXXX
    DEGE_ELNO        158  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  DDL_MECA PDEPLAR  NGEOMER  PGEOMER
                                CMATERC  PMATERC  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                          OUT__ EDEFGNO  PDEFOGR
    ECIN_ELEM         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    EFGE_ELGA         -1  IN__
                          OUT__ XXXXXX   XXXXXX
    EFGE_ELNO        185  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECONTPG  PCONTRR
                                DDL_MECA PDEPLAR  ECAFIEL  PFIBRES  NGEOMER  PGEOMER  CMATERC  PMATERC
                                ENBSP_I  PNBSP_I  ENONLIN  PNONLIN  ESTRAUX  PSTRXRR  ZVARCPG  PVARCPR
                          OUT__ EEFGENC  PEFFORC  EEFGENO  PEFFORR
    ENEL_ELEM         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    ENER_TOTALE       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    EPEQ_ELGA         -1  IN__
                          OUT__ XXXXXX   XXXXXX
    EPEQ_ELNO         -1  IN__
                          OUT__ XXXXXX   XXXXXX
    EPOT_ELEM        151  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  DDL_MECA PDEPLAR
                                ECAFIEL  PFIBRES  NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                          OUT__ EENEDNO  PENERDR
    EPSI_ELGA         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    EPSI_ELNO         -1  IN__
                          OUT__ XXXXXX   XXXXXX
    FORC_NODA        517  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECONTPG  PCONTMR
                                DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  ECAFIEL  PFIBRES  NGEOMER  PGEOMER
                                CMATERC  PMATERC  ENBSP_I  PNBSP_I  ESTRAUX  PSTRXMR  ZVARCPG  PVARCPR
                          OUT__ MVECTUR  PVECTUR
    FULL_MECA        516  IN__  CCAGNP2  PCAGNPO  CCAORIE  PCAORIE  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR
                                ECONTPG  PCONTMR  DDL_MECA PDDEPLA  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                ECAFIEL  PFIBRES  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                CITERAT  PITERAT  CMATERC  PMATERC  ENBSP_I  PNBSP_I  ESTRAUX  PSTRXMP
                                ESTRAUX  PSTRXMR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                                ZVARIPG  PVARIMP  ZVARIPG  PVARIMR
                          OUT__ ECODRET  PCODRET  ECONTPG  PCONTPR  MMATUUR  PMATUUR  ESTRAUX  PSTRXPR
                                ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    FULL_MECA_ELAS    -1  IN__
                          OUT__ XXXXXX   XXXXXX
    INDIC_ENER        -1  IN__
                          OUT__ XXXXXX   XXXXXX
    INDIC_SEUIL       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    INIT_VARC         99  IN__
                          OUT__ ZVARCPG  PVARCPR
    INI_SP_MATER      99  IN__
                          OUT__ EHYDRMA  PHYDMAT  EMNEUT_R PNEUMAT  ETEMPMA  PTEMMAT
    INI_STRX          23  IN__  CCAORIE  PCAORIE
                          OUT__ ESTRAUX  PSTRX_R
    MASS_FLUI_STRU   141  IN__  CABSCUR  PABSCUR  CCAGEPO  PCAGEPO  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE
                                CCOMPOR  PCOMPOR  ECAFIEL  PFIBRES  NGEOMER  PGEOMER  CMATERC  PMATERC
                                ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR
                          OUT__ MMATUUR  PMATUUR
    MASS_INER         38  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECAFIEL  PFIBRES
                                NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR
                                CCAGEPO  PCAGEPO  CABSCUR  PABSCUR
                          OUT__ EMASSINE PMASSINE
    MASS_MECA        141  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECAFIEL  PFIBRES
                                NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR
                          OUT__ MMATUUR  PMATUUR
    MASS_MECA_DIAG   141  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECAFIEL  PFIBRES
                                NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR
                          OUT__ MMATUUR  PMATUUR
    MASS_MECA_EXPLI  141  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECAFIEL  PFIBRES
                                NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR
                          OUT__ MMATUUR  PMATUUR
    MECA_GYRO        259  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECAFIEL  PFIBRES
                                NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I
                          OUT__ MMATUNS  PMATUNS
    MINMAX_SP         99  IN__
                          OUT__ EGAMIMA  PGAMIMA  ENOMIMA  PNOMIMA
    M_GAMMA          141  IN__  DDL_MECA PACCELR  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER
                                CMATERC  PMATERC  ZVARCPG  PVARCPR
                          OUT__ MVECTUR  PVECTUR
    NSPG_NBVA        496  IN__  CCOMPO2  PCOMPOR  ENBSP_I  PNBSP_I
                          OUT__ EDCEL_I  PDCEL_I
    PAS_COURANT      404  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC
                          OUT__ ECOURAN  PCOURAN
    RAPH_MECA        516  IN__  CCAGNP2  PCAGNPO  CCAORIE  PCAORIE  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR
                                ECONTPG  PCONTMR  DDL_MECA PDDEPLA  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                ECAFIEL  PFIBRES  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                CITERAT  PITERAT  CMATERC  PMATERC  ENBSP_I  PNBSP_I  ESTRAUX  PSTRXMP
                                ESTRAUX  PSTRXMR  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                                ZVARIPG  PVARIMP  ZVARIPG  PVARIMR
                          OUT__ ECODRET  PCODRET  ECONTPG  PCONTPR  ESTRAUX  PSTRXPR  ZVARIPG  PVARIPR
                                MVECTUR  PVECTUR
    REFE_FORC_NODA   517  IN__  EREFCO   PREFCO
                          OUT__ MVECTUR  PVECTUR
    REPERE_LOCAL     135  IN__  CCAORIE  PCAORIE
                          OUT__ CGEOMER  PREPLO1  CGEOMER  PREPLO2  CGEOMER  PREPLO3
    RICE_TRACEY       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    RIGI_FLUI_STRU   140  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECAFIEL  PFIBRES
                                NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR
                          OUT__ MMATUUR  PMATUUR
    RIGI_MECA        140  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECAFIEL  PFIBRES
                                NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I  ZVARCPG  PVARCPR
                          OUT__ MMATUUR  PMATUUR
    RIGI_MECA_ELAS    -1  IN__  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA_GE     143  IN__  CCAGNP1  PCAGNPO  CCAORIE  PCAORIE  ECONTPG  PEFFORR  ECAFIEL  PFIBRES
                                NGEOMER  PGEOMER  ENBSP_I  PNBSP_I  ESTRAUX  PSTRXRR
                          OUT__ MMATUUR  PMATUUR
    RIGI_MECA_HYST    50  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC  MMATUUR  PRIGIEL  ZVARCPG  PVARCPR
                          OUT__ MMATUUC  PMATUUC
    RIGI_MECA_RO     235  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  CCOMPOR  PCOMPOR  ECAFIEL  PFIBRES
                                NGEOMER  PGEOMER  CMATERC  PMATERC  ENBSP_I  PNBSP_I  CROTATR  PROTATR
                                ZVARCPG  PVARCPR
                          OUT__ MMATUUR  PMATUUR
    RIGI_MECA_TANG   516  IN__  CCAGNP2  PCAGNPO  CCAORIE  PCAORIE  CCARCRI  PCARCRI  CCOMPOR  PCOMPOR
                                ECONTPG  PCONTMR  DDL_MECA PDDEPLA  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                ECAFIEL  PFIBRES  NGEOMER  PGEOMER  CTEMPSR  PINSTMR  CTEMPSR  PINSTPR
                                CITERAT  PITERAT  CMATERC  PMATERC  ENBSP_I  PNBSP_I  ESTRAUX  PSTRXMR
                                ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARIPG  PVARIMR
                          OUT__ MMATUUR  PMATUUR
    SIEF_ELGA         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    SIEF_ELNO          4  IN__  ECONTPG  PCONTRR  ZVARCPG  PVARCPR
                          OUT__ ECONTNC  PSIEFNOC ECONTNO  PSIEFNOR
    SIEQ_ELGA        335  IN__  ECONTPG  PCONTRR
                          OUT__ ECOEQPG  PCONTEQ
    SIEQ_ELNO        335  IN__  ECONTNO  PCONTRR
                          OUT__ ECOEQNO  PCONTEQ
    SIGM_ELGA        546  IN__  ECONTPG  PSIEFR
                          OUT__ ECONTPC  PSIGMC   ECONTPG  PSIGMR
    SIGM_ELNO          4  IN__  ECONTPG  PCONTRR
                          OUT__ ECONTNC  PSIEFNOC ECONTNO  PSIEFNOR
    SIPM_ELNO        149  IN__  ENBSP_I  PNBSP_I  ECONTNO  PSIEFNOR ZVARCPG  PVARCPR
                          OUT__ ESIMXNC  PSIMXRC  ESIMXNO  PSIMXRR
    SIPO_ELNO         -1  IN__
                          OUT__ XXXXXX   XXXXXX
    TOU_INI_ELEM      99  IN__
                          OUT__ ECAFIEL  PCAFI_R  CGEOMER  PGEOM_R  ENBSP_I  PNBSP_I
    TOU_INI_ELGA      99  IN__
                          OUT__ EGGEOM_R PGEOM_R  EGINST_R PINST_R  EGNEUT_F PNEUT_F  EGNEUT_R PNEUT_R
                                ECONTPG  PSIEF_R  ZVARIPG  PVARI_R
    TOU_INI_ELNO      99  IN__
                          OUT__ NGEOMER  PGEOM_R  EEINST_R PINST_R  EENEUT_F PNEUT_F  EENEUT_R PNEUT_R
                                EEFGENO  PSIEF_R  ZVARINO  PVARI_R
    VARI_ELNO         -1  IN__
                          OUT__ XXXXXX   XXXXXX
    WEIBULL           -1  IN__
                          OUT__ XXXXXX   XXXXXX
