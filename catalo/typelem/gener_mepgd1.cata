%& LIBRARY TYPELEM
% ======================================================================
% COPYRIGHT (C) 1991 - 2013  EDF R&D                  WWW.CODE-ASTER.ORG
% THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
% IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
% THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
% (AT YOUR OPTION) ANY LATER VERSION.
%
% THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
% WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
% MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
% GENERAL PUBLIC LICENSE FOR MORE DETAILS.
%
% YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
% ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
%    1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
% ======================================================================
GENER_MEPGD1
TYPE_GENE__

ENTETE__ ELEMENT__ MECA_POU_D_T_GD  MAILLE__ SEG2
   ELREFE__  SE2       GAUSS__  RIGI=FPG1  FPG1=FPG1  FPG_LISTE__  MATER = (RIGI FPG1)
   ELREFE__  CABPOU

MODE_LOCAL__
    ZVARCPG  = VARI_R   ELGA__ MATER   (VARI     )
    CMATERC  = ADRSJEVE ELEM__         (I1       )
    CCAGNPO  = CAGNPO   ELEM__         (A1       IY1      IZ1      AY1      AZ1      EY1
                                        EZ1      JX1      RY1      RZ1      RT1      A2
                                        IY2      IZ2      AY2      AZ2      EY2      EZ2
                                        JX2      RY2      RZ2      RT2      TVAR     )
    CCAORIE  = CAORIE   ELEM__         (ALPHA    BETA     GAMMA    )
    ECODRET  = CODE_I   ELEM__         (IRET     )
    CCOMPOR  = COMPOR   ELEM__         (RELCOM   NBVARI   DEFORM   INCELA   C_PLAN   )
    CCOMPO2  = COMPOR   ELEM__         (NBVARI   )
    EDCEL_I  = DCEL_I   ELEM__         (NPG_DYN  NCMP_DYN )
    DDL_MECA = DEPL_R   ELNO__ IDEN__  (DX       DY       DZ       DRX      DRY      DRZ      )
    ECOURAN  = NEUT_R   ELEM__         (X1       )
    NVITER   = DEPL_R   ELNO__ IDEN__  (DX       DY       DZ       )
    CFLAPLA  = FLAPLA   ELEM__         (NOMAIL   NOGEOM   )
    CFORCEF  = FORC_F   ELEM__         (FX       FY       FZ       MX       MY       MZ
                                        REP      )
    CFORCER  = FORC_R   ELEM__         (FX       FY       FZ       MX       MY       MZ
                                        REP      )
    NGEOMER  = GEOM_R   ELNO__ IDEN__  (X        Y        Z        )
    CGEOMER  = GEOM_R   ELEM__         (X        Y        Z        )
    EGGEOP_R = GEOM_R   ELGA__ RIGI    (X        Y        Z        W       )
    CTEMPSR  = INST_R   ELEM__         (INST     )
    CINSTMR  = INST_R   ELEM__         (INST     )
    CINSTPR  = INST_R   ELEM__         (INST     )
    CLISTMA  = LISTMA   ELEM__         (LISTMA   TRANS    )
    CPESANR  = PESA_R   ELEM__         (G        AG       BG       CG       )
    CROTATR  = ROTA_R   ELEM__         (OME    AR     BR     CR     X      Y     Z  )
    EEFGEGA  = SIEF_R   ELGA__ RIGI    (N        VY       VZ       MT       MFY      MFZ      )
    EEFGEGC  = SIEF_C   ELGA__ RIGI    (N        VY       VZ       MT       MFY      MFZ      )
    EEFFONO  = SIEF_R   ELNO__ IDEN__  (FX       FY       FZ       MX       MY       MZ       )
    EEFGENO  = SIEF_R   ELNO__ IDEN__  (N        VY       VZ       MT       MFY      MFZ      )
    EEFGENC  = SIEF_C   ELNO__ IDEN__  (N        VY       VZ       MT       MFY      MFZ      )
    CSTADYN  = STAOUDYN ELEM__         (STAOUDYN ALFNMK   DELNMK   )
    ZVARIPG  = VARI_R   ELGA__ RIGI    (VARI     )
    ZVARINO  = VARI_R   ELNO__ IDEN__  (VARI     )
    CVENTCX  = VENTCX_F ELEM__         (FCXP     )
    EREFCO   = PREC     ELEM__         (EFFORT    MOMENT)
    EGNEUT_R = NEUT_R   ELGA__ RIGI    (X[30])
    ENONLIN  = NEUT_I   ELEM__         (X1)
    EGNEUT_F = NEUT_F   ELGA__ RIGI    (X[30])

VECTEUR__
    MVECTUR = VDEP_R DDL_MECA

MATRICE__
    MMATUUR = MDEP_R DDL_MECA DDL_MECA
    MMATUNS = MDNS_R DDL_MECA DDL_MECA

OPTION__
    ADD_SIGM         581  IN__  EEFGEGA  PEPCON1  EEFGEGA  PEPCON2
                          OUT__ EEFGEGA  PEPCON3
    AMOR_MECA         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    CARA_GEOM         -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_LIMITE       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_EPSI_R  -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_FC1D1D  -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_FF1D1D 161  IN__  CFORCEF  PFF1D1D  NGEOMER  PGEOMER  CTEMPSR  PTEMPSR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_FR1D1D 161  IN__  CFORCER  PFR1D1D  NGEOMER  PGEOMER
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_FRELEC  -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_FRLAPL 163  IN__  CFLAPLA  PFLAPLA  NGEOMER  PGEOMER  CLISTMA  PLISTMA
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_PESA_R 161  IN__  CCAGNPO  PCAGNPO  NGEOMER  PGEOMER  CMATERC  PMATERC  CPESANR  PPESANR
                                ZVARCPG  PVARCPR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_ROTA_R  -1  IN__
                          OUT__ XXXXXX   XXXXXX
    CHAR_MECA_SF1D1D 161  IN__  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  CFORCEF  PFF1D1D  NGEOMER  PGEOMER
                                CTEMPSR  PTEMPSR
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_SR1D1D 161  IN__  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CVENTCX  PVENTCX
                                NVITER   PVITER
                          OUT__ MVECTUR  PVECTUR
    CHAR_MECA_TEMP_R 396  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                ZVARCPG  PVARCPR  ZVARCPG  PVARCRR
                          OUT__ MVECTUR  PVECTUR
    COOR_ELGA        478  IN__  NGEOMER  PGEOMER
                          OUT__ EGGEOP_R PCOORPG
    DEGE_ELNO         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    ECIN_ELEM         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    EFGE_ELGA        546  IN__  EEFGEGA  PSIEFR
                          OUT__ EEFGEGC  PEFGEC   EEFGEGA  PEFGER
    EFGE_ELNO        185  IN__  EEFGEGA  PCONTRR  ENONLIN  PNONLIN  ZVARCPG  PVARCPR
                          OUT__ EEFGENC  PEFFORC  EEFGENO  PEFFORR
    ENEL_ELEM         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    ENER_TOTALE       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    EPOT_ELEM         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    FORC_NODA        393  IN__  CCAORIE  PCAORIE  EEFGEGA  PCONTMR  DDL_MECA PDEPLMR  DDL_MECA PDEPLPR
                                NGEOMER  PGEOMER  ZVARCPG  PVARCPR
                          OUT__ MVECTUR  PVECTUR
    FULL_MECA        390  IN__  DDL_MECA PACCKM1  DDL_MECA PACCPLU  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE
                                CCOMPOR  PCOMPOR  DDL_MECA PDDEPLA  DDL_MECA PDEPKM1  DDL_MECA PDEPLMR
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CINSTMR  PINSTMR  CINSTPR  PINSTPR
                                CMATERC  PMATERC  DDL_MECA PROMK    DDL_MECA PROMKM1  CSTADYN  PSTADYN
                                ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARIPG  PVARIMP
                                DDL_MECA PVITKM1  DDL_MECA PVITPLU
                          OUT__ ECODRET  PCODRET  EEFGEGA  PCONTPR  MMATUNS  PMATUNS  ZVARIPG  PVARIPR
                                MVECTUR  PVECTUR
    INDIC_ENER        -1  IN__
                          OUT__ XXXXXX   XXXXXX
    INDIC_SEUIL       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    INIT_VARC         99  IN__
                          OUT__ ZVARCPG  PVARCPR
    MASS_FLUI_STRU    -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    MASS_INER         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    MASS_MECA        391  IN__  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER  CMATERC  PMATERC
                                ZVARCPG  PVARCPR
                          OUT__ MMATUNS  PMATUNS
    MASS_MECA_DIAG    -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    MASS_MECA_EXPLI   -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    M_GAMMA          391  IN__  DDL_MECA PACCELR  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE  NGEOMER  PGEOMER
                                CMATERC  PMATERC  ZVARCPG  PVARCPR
                          OUT__ MVECTUR  PVECTUR
    NSPG_NBVA        496  IN__  CCOMPO2  PCOMPOR
                          OUT__ EDCEL_I  PDCEL_I
    PAS_COURANT      404  IN__  NGEOMER  PGEOMER  CMATERC  PMATERC
                          OUT__ ECOURAN  PCOURAN
    RAPH_MECA        390  IN__  DDL_MECA PACCKM1  DDL_MECA PACCPLU  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE
                                CCOMPOR  PCOMPOR  DDL_MECA PDDEPLA  DDL_MECA PDEPKM1  DDL_MECA PDEPLMR
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CINSTMR  PINSTMR  CINSTPR  PINSTPR
                                CMATERC  PMATERC  DDL_MECA PROMK    DDL_MECA PROMKM1  CSTADYN  PSTADYN
                                ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARIPG  PVARIMP
                                DDL_MECA PVITKM1  DDL_MECA PVITPLU
                          OUT__ ECODRET  PCODRET  EEFGEGA  PCONTPR  ZVARIPG  PVARIPR  MVECTUR  PVECTUR
    REFE_FORC_NODA   393  IN__  EREFCO   PREFCO
                          OUT__ MVECTUR  PVECTUR
    REPERE_LOCAL     135  IN__  CCAORIE  PCAORIE
                          OUT__ CGEOMER  PREPLO1  CGEOMER  PREPLO2  CGEOMER  PREPLO3
    RICE_TRACEY       -1  IN__
                          OUT__ XXXXXX   XXXXXX
    RIGI_FLUI_STRU    -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA_ELAS    -1  IN__  ZVARCPG  PVARCMR  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA_GE      -1  IN__
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA_HYST    -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA_RO      -1  IN__
                          OUT__ XXXXXX   XXXXXX
    RIGI_MECA_TANG   390  IN__  DDL_MECA PACCKM1  DDL_MECA PACCPLU  CCAGNPO  PCAGNPO  CCAORIE  PCAORIE
                                CCOMPOR  PCOMPOR  DDL_MECA PDDEPLA  DDL_MECA PDEPKM1  DDL_MECA PDEPLMR
                                DDL_MECA PDEPLPR  NGEOMER  PGEOMER  CINSTMR  PINSTMR  CINSTPR  PINSTPR
                                CMATERC  PMATERC  DDL_MECA PROMK    DDL_MECA PROMKM1  CSTADYN  PSTADYN
                                ZVARCPG  PVARCMR  ZVARCPG  PVARCPR  ZVARCPG  PVARCRR  ZVARIPG  PVARIMP
                                DDL_MECA PVITKM1  DDL_MECA PVITPLU
                          OUT__ MMATUNS  PMATUNS
    SIEF_ELGA         -1  IN__  ZVARCPG  PVARCPR
                          OUT__ XXXXXX   XXXXXX
    SIEF_ELNO          4  IN__  EEFGEGA  PCONTRR  ZVARCPG  PVARCPR
                          OUT__ EEFGENC  PSIEFNOC EEFGENO  PSIEFNOR
    SIPO_ELNO         -1  IN__
                          OUT__ XXXXXX   XXXXXX
    SYME_MDNS_R      222  IN__  MMATUNS  PNOSYM
                          OUT__ MMATUUR  PSYM
    TOU_INI_ELEM      99  IN__
                          OUT__ CGEOMER  PGEOM_R
    TOU_INI_ELGA      99  IN__
                          OUT__ EGNEUT_F PNEUT_F  EGNEUT_R PNEUT_R  EEFGEGA  PSIEF_R  ZVARIPG  PVARI_R
    VARI_ELNO          4  IN__  ZVARIPG  PVARIGR
                          OUT__ ZVARINO  PVARINR
    WEIBULL           -1  IN__
                          OUT__ XXXXXX   XXXXXX
